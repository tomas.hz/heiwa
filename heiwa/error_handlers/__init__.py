"""Error handler functions. Unless something is very wrong, their respones will
always contain JSONified dictionaries.
"""

import http.client
import typing

import flask
import werkzeug.exceptions

from .. import exceptions

__all__ = [
	"handle_api_exception",
	"handle_http_exception"
]
__version__ = "1.5.0"


def _log_exception(exception: Exception) -> None:
	"""If this function is running inside a Flask app context, logs the
	``exception`` being handled in its logger.

	:param exception: The exception to log.
	"""

	if flask.has_app_context():
		flask.current_app.logger.info(
			"Handling exception %s: %s",
			exception.__class__.__name__,
			(
				exception
				if hasattr(exception, "__str__")
				else "no details"
			)
		)


def handle_api_exception(
	exception: exceptions.APIException
) -> typing.Tuple[flask.Response, int]:
	"""Turns an :class:`APIException <heiwa.exceptions.APIException>` object into
	a dictionary of its type (class name) and
	:attr:`details <heiwa.exceptions.APIException.details>`.

	:param exception: The exception to convert.

	:returns: A tuple of the dictionary contained within a :class:`flask.Response`
		and its status code.
	"""

	_log_exception(exception)

	return flask.jsonify({
		"type": exception.__class__.__name__,
		"details": exception.details
	}), exception.code


def handle_http_exception(
	exception: werkzeug.exceptions.HTTPException
) -> typing.Tuple[flask.Response, int]:
	"""Turns an :class:`HTTPException <werkzeug.exceptions.HTTPException>`
	object into a dictionary of its type (class name) and
	:attr:`description <werkzeug.exceptions.HTTPException.description>`.

	If the exception's :attr:`code <werkzeug.exceptions.HTTPException.code>
	attribute is :data:`None`, returns
	:const:`INTERNAL_SERVER_ERROR <http.client.INTERNAL_SERVER_ERROR>` as the
	code.

	:param exception: The exception to convert.

	:returns: A tuple of the dictionary contained within a :class:`flask.Response`
		and its status code.

	.. note::
		Flask's default error handler knows how to handle this type of exception
		already, but since it uses HTML documents to do so, it would be inconsistent
		with the rest of the API.
	"""

	_log_exception(exception)

	return flask.jsonify({
		"type": exception.__class__.__name__,
		"details": exception.description
	}), (
		exception.code
		if exception.code is not None
		else http.client.INTERNAL_SERVER_ERROR
	)
