"""Utilities for API views."""

from .constants import (
	BASE_PERMISSION_CREATE_EDIT_SCHEMA,
	PERMISSION_KEY_ATTR_SCHEMA,
	PERMISSION_KEY_CREATE_EDIT_SCHEMA
)
from .edit import handle_edit
from .get_and_validate import (
	get_category,
	get_forum,
	get_group,
	get_session_and_user_defaults,
	get_thread,
	get_user,
	validate_category_exists,
	validate_forum_exists,
	validate_group_exists,
	validate_thread_exists,
	validate_user_exists
)
from .hash import get_identifier_hash
from .jwt import get_user_jwt
from .permissions import requires_permission, validate_permission
from .schema import (
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry
)
from .search import get_order_by_expression, parse_search

__all__ = [
	"BASE_PERMISSION_CREATE_EDIT_SCHEMA",
	"PERMISSION_KEY_ATTR_SCHEMA",
	"PERMISSION_KEY_CREATE_EDIT_SCHEMA",
	"get_category",
	"get_forum",
	"get_group",
	"get_user_jwt",
	"get_order_by_expression",
	"get_identifier_hash",
	"get_search_schema",
	"get_search_schema_filter_in_max_list_length",
	"get_search_schema_filter_registry",
	"get_session_and_user_defaults",
	"get_thread",
	"get_user",
	"handle_edit",
	"parse_search",
	"requires_permission",
	"validate_permission",
	"validate_category_exists",
	"validate_forum_exists",
	"validate_group_exists",
	"validate_thread_exists",
	"validate_user_exists"
]
__version__ = "1.36.1"
