"""Utilities for ``edit`` endpoints."""

import datetime
import typing

from heiwa import database

__all__ = ["handle_edit"]


def handle_edit(
	object_: database.utils.PermissionControlMixin,
	values: typing.Dict[str, typing.Any],
	exception: Exception,
	current_utc_time: typing.Union[None, datetime.datetime] = None
) -> None:
	"""Updates the attributes of ``object_`` as ``values`` describes and checks
	if any of them are different than the previous ones. If not, ``exception``
	is raised. Otherwise, the object's
	:meth:`edited <heiwa.database.utils.PermissionControlMixin.edited>` method is
	called.

	This function is only meant to be used in ``edit`` endpoints, where repeating
	the same code is unnecessary.

	:param object_: The object to edit. Since the fact it's been edited is also
		going to be handled, this must be a subclass of
		:class:`heiwa.database.utils.PermissionControlMixin`.
	:param values: The values to update the object with.
	:param exception: The exception to be called when there aren't any changes.
	:param current_utc_time: The current date and time, in the UTC timezone. This
		is to be used when the
		:meth:`edited <heiwa.database.utils.PermissionControlMixin.edited>` method
		is called, but can be left as :data:`None`.
	"""

	unchanged = True

	for key, value in values.items():
		if getattr(object_, key) != value:
			unchanged = False
			setattr(object_, key, value)

	if unchanged:
		raise exception

	object_.edited(current_utc_time=current_utc_time)
