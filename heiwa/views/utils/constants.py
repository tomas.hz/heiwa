"""API endpoint constants."""

from heiwa import database

__all__ = [
	"BASE_PERMISSION_CREATE_EDIT_SCHEMA",
	"PERMISSION_KEY_ATTR_SCHEMA",
	"PERMISSION_KEY_CREATE_EDIT_SCHEMA"
]

PERMISSION_KEY_ATTR_SCHEMA = {
	"type": "boolean"
}

PERMISSION_KEY_CREATE_EDIT_SCHEMA = {
	**PERMISSION_KEY_ATTR_SCHEMA,
	"nullable": True,
	"required": True
}
"""A Cerberus schema for permission keys used in API endpoints for creating and
editing objects. Generally not to be used on its own.

.. seealso::
	:class:`BasePermissionMixin <heiwa.database.utils.BasePermissionMixin>`
"""

BASE_PERMISSION_CREATE_EDIT_SCHEMA = {
	key: PERMISSION_KEY_CREATE_EDIT_SCHEMA
	for key in database.utils.BasePermissionMixin.DEFAULT_PERMISSIONS
}
"""A Cerberus schema used for the validation of all default permission
values in API endpoints used for creating or editing objects.
"""
