"""Utilities for handling JWTs, the preferred (and currently only) way to
authenticate with most API endpoints.
"""

import typing
import uuid

import authlib.jose
import flask

from heiwa import database

__all__ = ["get_user_jwt"]


def get_user_jwt(
	user_id: uuid.UUID,
	name: typing.Union[None, str] = None,
	expires_after: typing.Union[None, int] = None
) -> str:
	"""Creates a JWT for the :class:`User <heiwa.database.User>` with the given
	``user_id``.

	It's assumed that the user exists, since there's no need to look for data
	about them stored in the database. An invalid JWT will be generated if the
	input is invalid, no exceptions will be raised.

	To avoid (admittedly very unlikely) collisions with other views using
	JWTs, the ``sub`` claim is prepended with the
	:class:`User <heiwa.database.User>` class name and ``:``. This string is
	referred to as the **resource type**.

	:param user_id: The user's :attr:`id <heiwa.database.User.id>`.
	:param name: The name of the service which generated the JWT. If left as
		:data:`None`, this will be the ``META_NAME`` key in the current app's
		config.
	:param expires_after: How much time until the JWT expires. If left as
		:data:`None`, this will be the ``USER_JWT_EXPIRES_AFTER`` key in the
		current app's config. To comply with the JWT specification, this is not
		an ISO datetime, but a UNIX timestamp.

	:returns: An encoded version of the JWT, signed using the current app's
		``USER_JWT_SECRET_KEY``.

	.. seealso::
		`RFC 7519 - JSON web token <https://datatracker.ietf.org/doc/html/rfc7519>`_
	"""

	current_timestamp = round(flask.g.current_utc_time.timestamp())

	return authlib.jose.jwt.encode(
		{
			"alg": "HS256",
			"typ": "JWT"
		},
		{
			"iss": (
				flask.current_app.config["META_NAME"]
				if name is None
				else name
			),
			"iat": current_timestamp,
			"exp": (
				current_timestamp
				+ (
					flask.current_app.config["USER_JWT_EXPIRES_AFTER"]
					if expires_after is None
					else expires_after
				)
			),
			"sub": f"{database.User.__class__.__name__}:{user_id}"
		},
		flask.current_app.config["USER_JWT_SECRET_KEY"]
	).decode("utf-8")
