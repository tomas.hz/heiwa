"""Blueprints and utilities for different parts of the API."""

from . import utils
from .category import category_blueprint
from .file import file_blueprint
from .forum import (
	forum_blueprint,
	forum_parsed_permissions_blueprint,
	forum_permissions_group_blueprint,
	forum_permissions_user_blueprint,
	forum_subscription_blueprint
)
from .group import group_blueprint, group_permissions_blueprint
from .guest import guest_blueprint
from .local_account import local_account_blueprint
from .message import message_blueprint, message_directory_blueprint
from .meta import meta_blueprint
from .notification import notification_blueprint
from .oidc import oidc_blueprint
from .post import post_blueprint, post_vote_blueprint
from .statistic import statistic_blueprint
from .thread import (
	thread_blueprint,
	thread_subscription_blueprint,
	thread_view_record_blueprint,
	thread_vote_blueprint
)
from .user import (
	user_avatar_blueprint,
	user_ban_blueprint,
	user_block_blueprint,
	user_blueprint,
	user_follow_blueprint,
	user_groups_blueprint,
	user_permissions_blueprint,
	user_preferences_blueprint,
	user_reputation_blueprint
)

__all__ = [
	"category_blueprint",
	"file_blueprint",
	"forum_blueprint",
	"forum_parsed_permissions_blueprint",
	"forum_permissions_group_blueprint",
	"forum_permissions_user_blueprint",
	"forum_subscription_blueprint",
	"group_blueprint",
	"group_permissions_blueprint",
	"guest_blueprint",
	"local_account_blueprint",
	"message_blueprint",
	"message_directory_blueprint",
	"meta_blueprint",
	"notification_blueprint",
	"oidc_blueprint",
	"post_blueprint",
	"post_vote_blueprint",
	"statistic_blueprint",
	"thread_blueprint",
	"thread_subscription_blueprint",
	"thread_view_record_blueprint",
	"thread_vote_blueprint",
	"user_avatar_blueprint",
	"user_ban_blueprint",
	"user_block_blueprint",
	"user_blueprint",
	"user_follow_blueprint",
	"user_groups_blueprint",
	"user_permissions_blueprint",
	"user_preferences_blueprint",
	"user_reputation_blueprint",
	"utils"
]
__version__ = "0.64.2"
