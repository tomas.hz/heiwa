r"""API views for
:obj:`forum_subscribers <heiwa.database.forum_subscribers>`, belonging under
the base set of views for :class:`Forum <heiwa.database.Forum>`\ s.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter
)

from ..utils import get_forum, requires_permission, validate_permission

__all__ = [
	"create",
	"delete",
	"forum_subscription_blueprint",
	"view"
]

forum_subscription_blueprint = flask.Blueprint(
	"subscription",
	__name__,
	url_prefix="/<uuid:id_>/subscription"
)


@forum_subscription_blueprint.route("", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_subscription", database.Forum)
def create(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Creates a subscription from the current :attr:`flask.g.user` to the
	:class:`Forum <heiwa.database.Forum>` with the requested ``id_``.

	:param id_: The `id <heiwa.database.Forum.id>` of the forum.

	:raises heiwa.exceptions.APIForumSubscriptionAlreadyExists: Raised when the
		user has already subscribed to the forum in the past.

	:returns: :data:`None`, with the ``204`` HTTP status code.

	.. seealso::
		:obj:`heiwa.database.forum_subscribers`
	"""

	forum = get_forum(id_)

	validate_permission(
		flask.g.user,
		"edit_subscription",
		forum
	)

	if forum.get_is_subscribed(flask.g.user.id):
		raise exceptions.APIForumSubscriptionAlreadyExists

	flask.g.sa_session.execute(
		sqlalchemy.insert(database.forum_subscribers).
		values(
			forum_id=forum.id,
			user_id=flask.g.user.id
		)
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@forum_subscription_blueprint.route("", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view_subscription", database.Forum)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets whether or not the current :attr:`flask.g.user` is subscribed to the
	class:`Forum <heiwa.database.Forum>` with the requested ``id_``.

	:param id_: The `id <heiwa.database.Forum.id>` of the forum.

	:returns: The boolean result, with the ``200`` HTTP status code.

	.. seealso::
		:obj:`heiwa.database.forum_subscribers`
	"""

	forum = get_forum(id_)

	validate_permission(
		flask.g.user,
		"view_subscription",
		forum
	)

	return flask.jsonify(
		forum.get_is_subscribed(flask.g.user.id)
	), http.client.OK


@forum_subscription_blueprint.route("", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_subscription", database.Forum)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Removes :attr:`flask.g.user`'s subscription to the
	:class:`Forum <heiwa.database.Forum>` with the requested ``id_``.

	:param id_: The `id <heiwa.database.Forum.id>` of the forum.

	:raises heiwa.exceptions.APIForumSubscriptionNotFound: Raised when the user
		has never subscribed to the forum, thus there is no subscription to
		delete.

	:returns: :data:`None`, with the ``204`` HTTP status code.

	.. seealso::
		:obj:`heiwa.database.forum_subscribers`
	"""

	forum = get_forum(id_)

	validate_permission(
		flask.g.user,
		"edit_subscription",
		forum
	)

	if not forum.get_is_subscribed(flask.g.user.id):
		raise exceptions.APIForumSubscriptionNotFound

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.forum_subscribers).
		where(
			sqlalchemy.and_(
				database.forum_subscribers.c.forum_id == forum.id,
				database.forum_subscribers.c.user_id == flask.g.user.id
			)
		)
	)

	return flask.jsonify(None), http.client.NO_CONTENT
