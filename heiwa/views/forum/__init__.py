r"""API views for :class:`Forum <heiwa.database.Forum>`\ s."""

import http.client
import typing
import uuid

import flask
import sqlalchemy

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_category,
	get_forum,
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	handle_edit,
	parse_search,
	requires_permission,
	validate_category_exists,
	validate_permission
)
from .parsed_permissions import forum_parsed_permissions_blueprint
from .permissions_group import forum_permissions_group_blueprint
from .permissions_user import forum_permissions_user_blueprint
from .subscription import forum_subscription_blueprint

__all__ = [
	"ATTR_SCHEMAS",
	"CREATE_EDIT_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"create",
	"delete",
	"edit",
	"forum_blueprint",
	"forum_parsed_permissions_blueprint",
	"forum_permissions_group_blueprint",
	"forum_permissions_user_blueprint",
	"forum_subscription_blueprint",
	"list_",
	"mass_delete",
	"mass_edit",
	"merge",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.1.7"

forum_blueprint = flask.Blueprint(
	"forum",
	__name__,
	url_prefix="/forums"
)

for blueprint in (
	forum_parsed_permissions_blueprint,
	forum_permissions_group_blueprint,
	forum_permissions_user_blueprint,
	forum_subscription_blueprint
):
	forum_blueprint.register_blueprint(blueprint)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"category_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"parent_forum_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"name": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Forum.name.property.columns[0].type.length
	},
	"description": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Forum.description.property.columns[0].type.length
	},
	"order": {
		"type": "integer",
		"min": -2147483647,
		"max": 2147483647
	},
	"subscriber_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"thread_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"last_thread_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"post_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	}
}

CREATE_EDIT_SCHEMA = {
	"category_id": {
		**ATTR_SCHEMAS["category_id"],
		"nullable": True,
		"required": True
	},
	"parent_forum_id": {
		**ATTR_SCHEMAS["parent_forum_id"],
		"nullable": True,
		"required": True
	},
	"name": {
		**ATTR_SCHEMAS["name"],
		"required": True
	},
	"description": {
		**ATTR_SCHEMAS["description"],
		"nullable": True,
		"required": True
	},
	"order": {
		**ATTR_SCHEMAS["order"],
		"required": True
	}
}
search_schema = get_search_schema(
	(
		"order",
		"creation_timestamp",
		"edit_timestamp",
		"edit_count",
		"subscriber_count",
		"thread_count",
		"post_count"
	),
	default_order_by="order",
	default_order_asc=False,
	blueprint_name=forum_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(forum_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"order": ATTR_SCHEMAS["order"],
	"subscriber_count": ATTR_SCHEMAS["subscriber_count"],
	"thread_count": ATTR_SCHEMAS["thread_count"],
	"last_thread_timestamp": ATTR_SCHEMAS["last_thread_timestamp"],
	"post_count": ATTR_SCHEMAS["post_count"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"category_id": {
				**ATTR_SCHEMAS["category_id"],
				"nullable": True
			},
			"parent_forum_id": {
				**ATTR_SCHEMAS["parent_forum_id"],
				"nullable": True
			},
			"name": ATTR_SCHEMAS["name"],
			"description": {
				**ATTR_SCHEMAS["description"],
				"nullable": True
			},
			"order": ATTR_SCHEMAS["order"],
			"subscriber_count": ATTR_SCHEMAS["subscriber_count"],
			"thread_count": ATTR_SCHEMAS["thread_count"],
			"last_thread_timestamp": {
				**ATTR_SCHEMAS["last_thread_timestamp"],
				"nullable": True
			},
			"post_count": ATTR_SCHEMAS["post_count"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"category_id": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["category_id"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"parent_forum_id": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["parent_forum_id"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"name": {
				"type": "list",
				"schema": ATTR_SCHEMAS["name"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"description": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["description"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"order": {
				"type": "list",
				"schema": ATTR_SCHEMAS["order"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"subscriber_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["subscriber_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"thread_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["thread_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"last_thread_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["last_thread_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"post_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["post_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"name": {
				**ATTR_SCHEMAS["name"],
				"check_with": "is_valid_regex"
			},
			"description": {
				**ATTR_SCHEMAS["description"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})


@forum_blueprint.route("", methods=["POST"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.Forum)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a :class:`Forum <heiwa.database.Forum>` with the given values.

	:raises heiwa.exceptions.APIForumCategoryOutsideParent: Raised when a
		``category_id`` is specified, but the
		:class:`Category <heiwa.database.Category>` it corresponds to does not
		belong under the same forum as this one should.

	:returns: The newly created forum, with a ``201`` HTTP status code.
	"""

	if flask.g.json["category_id"] is not None:
		with flask.g.sa_session.no_autoflush:
			if flask.g.json["parent_forum_id"] is not None:
				category = get_category(flask.g.json["category_id"])

				if category.forum_id != flask.g.json["parent_forum_id"]:
					raise exceptions.APIForumCategoryOutsideParent
			else:
				validate_category_exists(flask.g.json["category_id"])

	if flask.g.json["parent_forum_id"] is not None:
		with flask.g.sa_session.no_autoflush:
			parent_forum = get_forum(flask.g.json["parent_forum_id"])

			validate_permission(
				flask.g.user,
				"create",
				database.Forum,
				parent=parent_forum
			)

			if (
				parent_forum.get_child_level() + 1
				> flask.current_app.config["FORUM_MAX_CHILD_LEVEL"]
			):
				raise exceptions.APIForumChildLevelLimitReached(
					flask.current_app.config["FORUM_MAX_CHILD_LEVEL"]
				)

	forum = database.Forum.create(
		flask.g.sa_session,
		**flask.g.json
	)
	forum.parse_permissions(flask.g.user)

	flask.g.sa_session.commit()

	return flask.jsonify(forum), http.client.CREATED


@forum_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Forum)
def list_() -> typing.Tuple[flask.Response, int]:
	r"""Gets all the :class:`Forum <heiwa.database.Forum>`\ s that the current
	:attr:`flask.g.user` has permission to view and match the filter, if there
	is one requested.

	If their parsed permissions for them haven't been calculated yet, it's done
	automatically.

	:returns: The list of forums, with a ``200`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Forum
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.Forum.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.Forum),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@forum_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"parent_forum_id": {
					**ATTR_SCHEMAS["parent_forum_id"],
					"nullable": True
				},
				"category_id": {
					**ATTR_SCHEMAS["category_id"],
					"nullable": True
				},
				"name": ATTR_SCHEMAS["name"],
				"description": {
					**ATTR_SCHEMAS["description"],
					"nullable": True
				},
				"order": ATTR_SCHEMAS["order"]
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Thread)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	r"""Updates all :class:`Forum <heiwa.database.Forum>`\ s that the current
	:attr:`flask.g.user` has the permission to.

	If their permissions haven't been parsed yet, it's done so automatically.

	:raises heiwa.exceptions.APIForumCategoryOutsideParent: Raised when both
		a ``category_id`` and ``parent_forum_id`` are given, but the
		respective :class:`Category <heiwa.database.Category>`'s
		:attr:`forum_id <heiwa.database.Category.forum_id>` does not match
		the ``parent_forum_id``.
	:raises heiwa.exceptions.APIForumChildLevelLimitReached: Raised when the
		forum is too many levels deep. The maximum for this is defined in
		the ``FORUM_MAX_CHILD_LEVEL`` config key.

	:returns: :data:`None`, with the ``204`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	conditions = True
	action_conditions = database.Forum.action_queries["edit"](flask.g.user)

	if "category_id" in flask.g.json["values"]:
		category = get_category(flask.g.json["category_id"])

		if "parent_forum_id" in flask.g.json["values"]:
			if category.forum_id != flask.g.json["values"]["parent_forum_id"]:
				raise exceptions.APIForumCategoryOutsideParent
		else:
			conditions = sqlalchemy.and_(
				conditions,
				database.Forum.parent_forum_id == category.forum_id
			)

	if "parent_forum_id" in flask.g.json["values"]:
		parent_forum = get_forum(flask.g.json["values"]["parent_forum_id"])

		action_conditions = sqlalchemy.and_(
			action_conditions,
			database.Forum.action_queries["move"](
				flask.g.user,
				future_parent=(
					flask.g.user,
					"move",
					parent_forum
				)
			)
		)

		if (
			parent_forum.get_child_level() + 1
			> flask.current_app.config["FORUM_MAX_CHILD_LEVEL"]
		):
			raise exceptions.APIForumChildLevelLimitReached(
				flask.current_app.config["FORUM_MAX_CHILD_LEVEL"]
			)

		conditions = sqlalchemy.and_(
			conditions,
			database.Forum.id != parent_forum.id
		)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Forum
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.Forum).
		where(
			database.Forum.id.in_(
				database.Forum.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=action_conditions,
					conditions=conditions,
					order_by=get_order_by_expression(database.Forum),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"]
				)
			)
		).
		values(**flask.g.json["values"])
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@forum_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Forum)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	r"""Deletes all the :class:`Forum <heiwa.database.Forum>`\ s that the current
	:attr:`flask.g.user` has permission to. If there is a filter requested, they
	must also match it. The associated :clasS:`Thread <heiwa.database.Thread>`\ s,
	:class:`Post <heiwa.database.Post>` and their
	:class:`Notification <heiwa.database.Notification>`\ s are also deleted.

	If permissions haven't been parsed for the respective forums yet, it will be
	done so automatically.

	:returns: :data:`None`, with the ``204`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Forum
			)
		)

	forum_ids = flask.g.sa_session.execute(
		database.Forum.get(
			flask.g.sa_session,
			flask.g.user,
			action_conditions=database.Forum.action_queries["delete"](flask.g.user),
			conditions=conditions,
			order_by=get_order_by_expression(database.Forum),
			limit=flask.g.json["limit"],
			offset=flask.g.json["offset"],
			pkeys_only=True
		)
	).scalars().all()

	if len(forum_ids) != 0:
		thread_ids = flask.g.sa_session.execute(
			sqlalchemy.select(database.Thread.id).
			where(database.Thread.forum_id.in_(forum_ids))
		).scalars().all()

		flask.g.sa_session.execute(
			sqlalchemy.delete(database.Notification).
			where(
				sqlalchemy.or_(
					sqlalchemy.and_(
						database.Notification.type.in_(database.Thread.NOTIFICATION_TYPES),
						database.Notification.identifier.in_(thread_ids)
					),
					sqlalchemy.and_(
						database.Notification.type.in_(database.Post.NOTIFICATION_TYPES),
						database.Notification.identifier.in_(
							sqlalchemy.select(database.Post.id).
							where(database.Post.thread_id.in_(thread_ids))
						)
					)
				)
			).
			execution_options(synchronize_session="fetch")
		)

		flask.g.sa_session.execute(
			sqlalchemy.delete(database.Forum).
			where(database.Forum.id.in_(forum_ids))
		)

	return flask.jsonify(None), http.client.NO_CONTENT


@forum_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Forum)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the :class:`Forum <heiwa.database.Forum>` with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Forum.id>` of the forum.

	:returns: The forum, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_forum(id_)
	), http.client.OK


@forum_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Forum)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the :class:`Forum <heiwa.database.Forum>` with the requested
	``id_``.

	:param id_: The :attr:`id <heiwa.database.Forum.id>` of the forum to update.

	:raises heiwa.exceptions.APIForumCategoryOutsideParent: Raised when the given
		``category_id`` is different from the previous one, and the
		forum corresponding to them ``parent_forum_id`` is not the same as the
		:class:`Category <heiwa.database.Category>`'s
		:attr:`forum_id <heiwa.database.Category.forum_id>`.
	:raises heiwa.exceptions.APIForumParentIsChild: Raised when the
		``parent_forum_id`` is the exact same as the current forum's ID.
	:raises heiwa.exceptions.APIForumChildLevelLimitReached: Raised when the
		forum is too many levels deep. The maximum value for this is defined in
		the ``FORUM_MAX_CHILD_LEVEL`` config key.
	:raises heiwa.exceptions.APIForumUnchanged: Raised when no attributes of the
		forum have been changed.

	:returns: The updated forum, with the ``200`` HTTP status code.
	"""

	forum = get_forum(id_)

	validate_permission(
		flask.g.user,
		"edit",
		forum
	)

	if flask.g.json["category_id"] != forum.category_id:
		category = get_category(flask.g.json["category_id"])

		if (
			category.forum_id != (
				flask.g.json["parent_forum_id"]
				if flask.g.json["parent_forum_id"] != forum.parent_forum_id
				else forum.parent_forum_id
			)
		):
			raise exceptions.APIForumCategoryOutsideParent

	if flask.g.json["parent_forum_id"] != forum.parent_forum_id:
		if forum.id == flask.g.json["parent_forum_id"]:
			raise exceptions.APIForumParentIsChild

		future_parent = get_forum(flask.g.json["parent_forum_id"])

		validate_permission(
			flask.g.user,
			"move",
			forum,
			future_parent=future_parent
		)

		if (
			future_parent.get_child_level() + 1
			> flask.current_app.config["FORUM_MAX_CHILD_LEVEL"]
		):
			raise exceptions.APIForumChildLevelLimitReached(
				flask.current_app.config["FORUM_MAX_CHILD_LEVEL"]
			)

		forum.parent_forum = future_parent

		forum.delete_all_parsed_permissions()

	handle_edit(
		forum,
		flask.g.json,
		exceptions.APIForumUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	flask.g.sa_session.commit()

	return flask.jsonify(forum), http.client.OK


@forum_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Forum)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the :class:`Forum <heiwa.database.Forum>` with the requested
	``id_``.

	:param id_: The :attr:`id <heiwa.database.Forum.id>` of the forum to delete.

	:returns: :data:`None`, with the ``204`` HTTP status code.
	"""

	forum = get_forum(id_)

	validate_permission(
		flask.g.user,
		"delete",
		forum
	)

	forum.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@forum_blueprint.route("/<uuid:id_>/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Forum)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all the actions that the current :attr:`flask.g.user` is allowed to
	perform on the :class:`Forum <heiwa.database.Forum>` with the requested
	``id_``.

	:param id_: The :attr:`id <heiwa.database.Forum.id>` of the forum.

	:returns: A list of the allowed actions, with the ``200`` HTTP status code.

	.. seealso::
		:attr:`heiwa.database.Forum.instance_actions`
	"""

	return flask.jsonify(
		get_forum(id_)
	), http.client.OK


@forum_blueprint.route("/<uuid:id_old>/merge/<uuid:id_new>", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("merge", database.Forum)
def merge(
	id_old: uuid.UUID,
	id_new: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	r"""Merges the :class:`Forum <heiwa.database.Forum>` with the given ``id_old``
	to the one with the ``id_new``.

	This means all :class:`Thread <heiwa.database.Thread>`\ s'
	:attr:`forum_id <heiwa.database.Thread.forum_id>` \s change to the new
	forum's. The same applies to :class:`Post <heiwa.database.Post>`\ s and
	:obj:`forum_subscribers <heiwa.database.forum_subscribers>`. The new forum
	is then marked as edited and the old one is deleted.

	:param id_old: The :attr:`id <heiwa.database.Forum.id>` of the old forum.
	:param id_new: The :attr:`id <heiwa.database.Forum.id>` of the new forum.

	:returns: The updated forum, with the ``200`` HTTP status code.
	"""

	old_forum = get_forum(id_old)
	new_forum = get_forum(id_new)

	validate_permission(
		flask.g.user,
		"merge",
		old_forum,
		future_parent=new_forum
	)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.Thread).
		where(database.Thread.id == old_forum.id).
		values(id=new_forum.id)
	)
	flask.g.sa_session.execute(
		sqlalchemy.update(database.Forum).
		where(database.Forum.parent_forum_id == old_forum.id).
		values(parent_forum_id=new_forum.id)
	)
	flask.g.sa_session.execute(
		sqlalchemy.update(database.forum_subscribers).
		where(database.forum_subscribers.c.forum_id == old_forum.id).
		values(parent_forum_id=new_forum.id)
	)

	new_forum.delete_all_parsed_permissions()

	old_forum.delete()
	new_forum.edited(current_utc_time=flask.g.current_utc_time)

	return flask.jsonify(new_forum), http.client.OK


@forum_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	r"""Gets all the actions that the current :attr:`flask.g.user` is allowed to
	perform on all :class:`Forum <heiwa.database.Forum>`\ s, irrespective of
	which one it is.

	:returns: A list of the allowed actions, with the ``200`` HTTP status code.

	.. seealso::
		:attr:`heiwa.database.Forum.static_actions`
	"""

	return flask.jsonify(
		database.Forum.get_allowed_static_actions(flask.g.user)
	), http.client.OK
