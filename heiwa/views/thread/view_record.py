r"""API views for :class:`Thread <heiwa.database.ThreadViewRecord>`\ s,
belonging under the default :class:`Thread <heiwa.database.Thread>` set of
views.
"""

import datetime
import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter
)

from ..utils import requires_permission, validate_thread_exists

__all__ = [
	"create",
	"thread_view_record_blueprint"
]

thread_view_record_blueprint = flask.Blueprint(
	"view",
	__name__,
	url_prefix="/<uuid:id_>/view"
)


@thread_view_record_blueprint.route("", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Thread)  # See model notes
def create(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Adds a recorded view to the thread with the requested ``id_`` and deletes
	all expired ones. This is not limited to a single view per user, information
	about who has viewed which thread is deleted after the cooldown runs out and
	only the counter remains.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:raises heiwa.exceptions.APIThreadViewRecordExceededCooldown: Raised when
		it's been too short of an amount of time since the current
		:attr:`flask.g.user` last added a view to this thread. Includes the time
		it expires in its :attr:`details <heiwa.exceptions.APIThreadViewRecord\
		ExceededCooldown.details>`.
	"""

	validate_thread_exists(id_)

	cooldown_timedelta = datetime.timedelta(
		seconds=flask.current_app.config["THREAD_VIEW_COOLDOWN"]
	)

	# Delete all expired views
	flask.g.sa_session.execute(
		sqlalchemy.delete(database.ThreadViewRecord).
		where(
			database.ThreadViewRecord.creation_timestamp <= (
				flask.g.current_utc_time - cooldown_timedelta
			)
		)
	)

	last_view_timestamp = flask.g.sa_session.execute(
		sqlalchemy.select(database.ThreadViewRecord.creation_timestamp).
		where(
			sqlalchemy.and_(
				database.ThreadViewRecord.thread_id == id_,
				database.ThreadViewRecord.user_id == flask.g.user.id
			)
		)
	).scalars().one_or_none()

	if last_view_timestamp is not None:
		raise exceptions.APIThreadViewRecordExceededCooldown(
			last_view_timestamp
			+ cooldown_timedelta
		)

	database.ThreadViewRecord.create(
		flask.g.sa_session,
		thread_id=id_,
		user_id=flask.g.user.id
	)

	return flask.jsonify(None), http.client.NO_CONTENT
