r"""API views for :class:`ThreadVote <heiwa.database.ThreadVote>`\ s, belonging
under the default :class:`Thread <heiwa.database.Thread>` set of views.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_session_and_user_defaults,
	get_thread,
	requires_permission,
	validate_permission,
	validate_thread_exists
)

__all__ = [
	"delete",
	"edit",
	"get_vote",
	"thread_vote_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]

thread_vote_blueprint = flask.Blueprint(
	"vote",
	__name__
)


def get_vote(
	thread_id: uuid.UUID,
	user_id: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None,
	validate_existence: bool = True
) -> database.ThreadVote:
	"""Gets the thread vote with the given ``user_id`` and ``thread_id``.

	:param thread_id: The :attr:`thread_id <heiwa.database.ThreadVote.thread_id>`
		of the vote to find. Defaults to :attr:`flask.g.sa_session` if
		:data:`None`.
	:param user_id: The :attr:`user_id <heiwa.database.ThreadVote.user_id>` of the
		vote to find. Defaults to :attr:`flask.g.user` if :data:`None`.
	:param session: The session to find the vote with.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the vote.
	:param validate_existence: Whether or not to check for the vote's existence.
		Defaults to :data:`True`.

	:raises heiwa.exceptions.APIThreadVoteNotFound: Raised when the vote doesn't
		exist, or the ``user`` does not have permission to view it. Depends on
		``validate_existence`` being :data:`True`.

	:returns: The vote.
	"""

	session, user = get_session_and_user_defaults(session, user)

	vote = flask.g.sa_session.execute(
		database.ThreadVote.get(
			session,
			user,
			conditions=sqlalchemy.and_(
				database.ThreadVote.thread_id == thread_id,
				database.ThreadVote.user_id == user_id
			)
		)
	).scalars().one_or_none()

	if validate_existence and vote is None:
		raise exceptions.APIThreadVoteNotFound

	return vote


@thread_vote_blueprint.route("/<uuid:id_>/vote", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.ThreadVote)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets :attr:`flask.g.user`'s vote for the thread with the requested
	``id_``.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:returns: The vote, with a ``200`` status code. :data:`None` if there's no
		vote.
	"""

	validate_thread_exists(id_)

	return flask.jsonify(
		get_vote(
			id_,
			flask.g.user.id,
			validate_existence=False
		)
	), http.client.OK


@thread_vote_blueprint.route("/<uuid:id_>/vote", methods=["PUT"])
@validators.validate_json({
	"is_upvote": {
		"type": "boolean",
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.ThreadVote)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Either updates :attr:`flask.g.user`'s vote for the thread with the
	requested ``id_`` if there is one already, or creates a new one. The only
	modifiable attribute is ``is_upvote``.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:raises heiwa.exceptions.APIThreadVoteUnchanged: Raised when there was a
		vote already, and its
		:attr:`is_upvote <heiwa.database.ThreadVote.is_upvote>` attribute does
		not differ from the one provided by the user.
	:raises heiwa.exceptions.APIThreadLocked: Raised when the thread is locked.

	:returns: Either the new vote with the HTTP ``204`` status code, or the
		updated vote with the ``200`` status code.
	"""

	thread = get_thread(id_)

	vote = get_vote(
		id_,
		flask.g.user.id,
		validate_existence=False
	)

	if vote is not None:
		validate_permission(
			flask.g.user,
			"edit",
			vote
		)

		if vote.is_upvote is flask.g.json["is_upvote"]:
			raise exceptions.APIThreadVoteUnchanged
	else:
		validate_permission(
			flask.g.user,
			"create",
			database.ThreadVote,
			thread=thread
		)

	if thread.is_locked:
		raise exceptions.APIThreadLocked

	if vote is not None:
		vote.is_upvote = flask.g.json["is_upvote"]

		vote.edited()

		status = http.client.OK
	else:
		vote = database.ThreadVote.create(
			flask.g.sa_session,
			thread_id=thread.id,
			user_id=flask.g.user.id,
			is_upvote=flask.g.json["is_upvote"]
		)

		status = http.client.CREATED

	flask.g.sa_session.commit()

	return flask.jsonify(vote), status


@thread_vote_blueprint.route("/<uuid:id_>/vote", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.ThreadVote)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes :attr:`flask.g.user`'s vote for the thread with the requested
	``id_``.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:raises heiwa.exceptions.APIThreadVoteNotFound: Raised when there is no vote.
	:raises heiwa.exceptions.APIThreadLocked: Raised when the thread is locked.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	thread = get_thread(id_)

	vote = get_vote(
		id_,
		flask.g.user.id
	)

	validate_permission(
		flask.g.user,
		"delete",
		vote
	)

	if thread.is_locked:
		raise exceptions.APIThreadLocked

	vote.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@thread_vote_blueprint.route(
	"/<uuid:id_>/vote/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.ThreadVote)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on their
	vote for the thread with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:raises heiwa.exceptions.APIThreadNotFound: Raised when the ``id_`` does not
		correspond to any thread, or the current user does not have the permission
		to view it.
	:raises heiwa.exceptions.APIThreadVoteNotFound: Raised when the thread does
		not have any vote from the current user, thus no permissions can be found.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	validate_thread_exists(id_)

	return flask.jsonify(
		get_vote(
			id_,
			flask.g.user.id
		)
	), http.client.OK


@thread_vote_blueprint.route("/votes/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on all
	thread votes, irrespective of which one it is.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		database.ThreadVote.get_allowed_static_actions(flask.g.user)
	), http.client.OK
