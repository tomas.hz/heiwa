r"""API views for :class:`Thread <heiwa.database.Thread>`\ s."""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_forum,
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_thread,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)
from .subscription import thread_subscription_blueprint
from .view_record import thread_view_record_blueprint
from .vote import thread_vote_blueprint

__all__ = [
	"ATTR_SCHEMAS",
	"CREATE_EDIT_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"create",
	"delete",
	"edit",
	"list_",
	"mass_delete",
	"mass_edit",
	"merge",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"thread_blueprint",
	"thread_subscription_blueprint",
	"thread_view_record_blueprint",
	"thread_vote_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.1.0"

thread_blueprint = flask.Blueprint(
	"thread",
	__name__,
	url_prefix="/threads"
)

for blueprint in (
	thread_subscription_blueprint,
	thread_view_record_blueprint,
	thread_vote_blueprint
):
	thread_blueprint.register_blueprint(blueprint)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"forum_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"user_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"is_approved": {
		"type": "boolean"
	},
	"is_locked": {
		"type": "boolean"
	},
	"is_pinned": {
		"type": "boolean"
	},
	"tags": {
		"type": "list",
		"check_with": "has_no_duplicates",
		"minlength": 0,
		"maxlength": 64,
		"schema": {
			"type": "string",
			"minlength": 1,
			"maxlength": database.Thread.tags.property.columns[0].type.item_type.length
		}
	},
	"name": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Thread.name.property.columns[0].type.length
	},
	"content": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Thread.content.property.columns[0].type.length
	},
	"vote_value": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"post_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"subscriber_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"last_post_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	}
}

CREATE_EDIT_SCHEMA = {
	"forum_id": {
		**ATTR_SCHEMAS["forum_id"],
		"required": True
	},
	"is_approved": {
		**ATTR_SCHEMAS["is_approved"],
		"required": True
	},
	"is_locked": {
		**ATTR_SCHEMAS["is_locked"],
		"required": True
	},
	"is_pinned": {
		**ATTR_SCHEMAS["is_pinned"],
		"required": True
	},
	"tags": {
		**ATTR_SCHEMAS["tags"],
		"required": True
	},
	"name": {
		**ATTR_SCHEMAS["name"],
		"required": True
	},
	"content": {
		**ATTR_SCHEMAS["content"],
		"required": True
	}
}
search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count",
		"vote_value",
		"post_count",
		"subscriber_count",
		"last_post_timestamp"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=thread_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(thread_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"vote_value": ATTR_SCHEMAS["vote_value"],
	"post_count": ATTR_SCHEMAS["post_count"],
	"subscriber_count": ATTR_SCHEMAS["subscriber_count"],
	"last_post_timestamp": ATTR_SCHEMAS["last_post_timestamp"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"forum_id": ATTR_SCHEMAS["forum_id"],
			"user_id": ATTR_SCHEMAS["user_id"],
			"is_approved": ATTR_SCHEMAS["is_approved"],
			"is_locked": ATTR_SCHEMAS["is_locked"],
			"is_pinned": ATTR_SCHEMAS["is_pinned"],
			"tags": ATTR_SCHEMAS["tags"],
			"name": ATTR_SCHEMAS["name"],
			"content": ATTR_SCHEMAS["content"],
			"vote_value": ATTR_SCHEMAS["vote_value"],
			"post_count": ATTR_SCHEMAS["post_count"],
			"subscriber_count": ATTR_SCHEMAS["subscriber_count"],
			"last_post_timestamp": {
				**ATTR_SCHEMAS["last_post_timestamp"],
				"nullable": True
			}
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"forum_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["forum_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"user_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["user_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"tags": {
				"type": "list",
				"schema": ATTR_SCHEMAS["tags"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"name": {
				"type": "list",
				"schema": ATTR_SCHEMAS["name"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"content": {
				"type": "list",
				"schema": ATTR_SCHEMAS["content"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"vote_value": {
				"type": "list",
				"schema": ATTR_SCHEMAS["vote_value"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"post_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["post_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"subscriber_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["subscriber_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"last_post_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["last_post_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$contains": {
		"type": "dict",
		"schema": {
			"tags": ATTR_SCHEMAS["tags"]["schema"]
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"name": {
				**ATTR_SCHEMAS["name"],
				"check_with": "is_valid_regex"
			},
			"content": {
				**ATTR_SCHEMAS["content"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})


@thread_blueprint.route("", methods=["POST"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.Thread)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a thread with the requested ``forum_id``, locked status
	(``is_locked``), pinned status (``is_pinned``), ``name`` and ``content``.

	:returns: The newly created thread, with a ``201`` HTTP status code.
	"""

	forum = get_forum(flask.g.json["forum_id"])

	validate_permission(
		flask.g.user,
		"create",
		database.Thread,
		forum=forum,
		is_approved=flask.g.json["is_approved"],
		is_locked=flask.g.json["is_locked"],
		is_pinned=flask.g.json["is_locked"]
	)

	thread = database.Thread.create(
		flask.g.sa_session,
		user_id=flask.g.user.id,
		**flask.g.json
	)

	flask.g.sa_session.commit()

	return flask.jsonify(thread), http.client.CREATED


@thread_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Thread)
def list_() -> typing.Tuple[flask.Response, int]:
	r"""Gets all threads that the current :attr:`flask.g.user` has the permission
	to view. If there is a filter requested, they must also match it.

	The respective :class:`Forum <heiwa.database.Forum>`\ s' parsed permisisons
	are immediately calculated, if it hasn't been done so already.

	:returns: The list of threads, with a ``200`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Thread
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.Thread.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.Thread),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@thread_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"forum_id": ATTR_SCHEMAS["forum_id"],
				"is_approved": ATTR_SCHEMAS["is_approved"],
				"is_locked": ATTR_SCHEMAS["is_locked"],
				"is_pinned": ATTR_SCHEMAS["is_pinned"],
				"tags": ATTR_SCHEMAS["tags"],
				"name": ATTR_SCHEMAS["name"],
				"content": ATTR_SCHEMAS["content"]
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Thread)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	r"""Updates all threads that the current :attr:`flask.g.user` is allowed to
	with the given ``values``.  If there is a filter requested, they must also
	match it.

	If it hasn't happened already, the respective
	:class:`Forum <heiwa.database.Forum>` \s' permissions are automatically
	parsed.

	:returns: :data:`None`, with the ``204`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	action_conditions = database.Thread.action_queries["edit"](
		flask.g.user,
		is_approved=(
			flask.g.json["values"]["is_approved"]
			if "is_approved" in flask.g.json["values"]
			else None
		),
		is_locked=(
			flask.g.json["values"]["is_locked"]
			if "is_locked" in flask.g.json["values"]
			else None
		),
		is_pinned=(
			flask.g.json["values"]["is_pinned"]
			if "is_pinned" in flask.g.json["values"]
			else None
		)
	)

	if "forum_id" in flask.g.json["values"]:
		action_conditions = sqlalchemy.and_(
			action_conditions,
			database.Thread.action_queries["move"](
				flask.g.user,
				future_forum=get_forum(flask.g.json["values"]["forum_id"])
			)
		)

	conditions = True

	if "is_locked" not in flask.g.json["values"]:
		conditions = sqlalchemy.and_(
			conditions,
			database.Thread.is_locked.is_(False)
		)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Thread
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.Thread).
		where(
			database.Thread.id.in_(
				database.Thread.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=action_conditions,
					conditions=conditions,
					order_by=get_order_by_expression(database.Thread),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"]
				)
			)
		).
		values(**flask.g.json["values"]).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@thread_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Thread)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	r"""Deletes all threads that the current :attr:`flask.g.user` is allowed to.
	If there is a filter requested, they must also match it.

	If it hasn't happened already, the respective
	:class:`Forum <heiwa.database.Forum>` \s' permissions are automatically
	parsed.

	:returns: :data:`None`, with the ``204`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Thread
			)
		)

	ids = flask.g.sa_session.execute(
		database.Thread.get(
			flask.g.sa_session,
			flask.g.user,
			action_conditions=database.Thread.action_queries["delete"](flask.g.user),
			conditions=conditions,
			order_by=get_order_by_expression(database.Thread),
			limit=flask.g.json["limit"],
			offset=flask.g.json["offset"],
			pkeys_only=True
		)
	).scalars().all()

	if len(ids) != 0:
		flask.g.sa_session.execute(
			sqlalchemy.delete(database.Notification).
			where(
				sqlalchemy.or_(
					sqlalchemy.and_(
						database.Notification.type.in_(database.Thread.NOTIFICATION_TYPES),
						database.Notification.identifier.in_(ids)
					),
					sqlalchemy.and_(
						database.Notification.type.in_(database.Post.NOTIFICATION_TYPES),
						database.Notification.identifier.in_(
							sqlalchemy.select(database.Post.id).
							where(database.Post.thread_id.in_(ids))
						)
					)
				)
			).
			execution_options(synchronize_session="fetch")
		)

		flask.g.sa_session.execute(
			sqlalchemy.delete(database.Thread).
			where(database.Thread.id.in_(ids))
		)

	return flask.jsonify(None), http.client.NO_CONTENT


@thread_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Thread)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the thread with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:returns: The thread, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_thread(id_)
	), http.client.OK


@thread_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Thread)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the thread with the requested ``id_`` with the given values.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:returns: The updated thread, with the ``200`` HTTP status code.
	"""

	thread = get_thread(id_)

	validate_permission(
		flask.g.user,
		"edit",
		thread,
		is_approved=flask.g.json["is_approved"],
		is_locked=flask.g.json["is_locked"],
		is_pinned=flask.g.json["is_pinned"]
	)

	if (
		thread.is_locked
		and flask.g.json["is_locked"]
	):
		raise exceptions.APIThreadLocked

	if thread.forum_id != flask.g.json["forum_id"]:
		future_forum = get_forum(flask.g.json["forum_id"])

		validate_permission(
			flask.g.user,
			"move",
			thread,
			future_forum=future_forum
		)

		thread.forum = future_forum

	handle_edit(
		thread,
		flask.g.json,
		exceptions.APIThreadUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	flask.g.sa_session.commit()

	return flask.jsonify(thread), http.client.OK


@thread_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Thread)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the thread with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	thread = get_thread(id_)

	validate_permission(
		flask.g.user,
		"delete",
		thread
	)

	thread.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@thread_blueprint.route("/<uuid:id_>/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Thread)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on the thread with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.Thread.id>` of the thread.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(
		get_thread(id_)
	), http.client.OK


@thread_blueprint.route("/<uuid:id_old>/merge/<uuid:id_new>", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("merge", database.Thread)
def merge(
	id_old: uuid.UUID,
	id_new: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	r"""Moves all votes, :class:`Post <heiwa.database.Post>`\ s and
	:class:`Notification <heiwa.database.Notification>`
	:attr:`identifier <heiwa.database.Notification.identifier>`\ s to the thread
	with the ``id_new``.

	:param id_old: The :attr:`id <heiwa.database.Thread.id>` of the thread to
		merge.
	:param id_new: The :attr:`id <heiwa.database.Thread.id>` of the thread to
		merge with.

	:raises heiwa.exceptions.APIThreadLocked: Raised when either of the threads
		is locked.

	:returns: The updated thread, with the ``200`` HTTP status code.
	"""

	old_thread = get_thread(id_old)

	new_thread = get_thread(id_new)

	for thread in (old_thread, new_thread):
		if thread.is_locked:
			raise exceptions.APIThreadLocked(thread.id)

	validate_permission(
		flask.g.user,
		"merge",
		old_thread,
		future_thread=new_thread
	)

	# Don't transfer over subscriptions
	flask.g.sa_session.execute(
		sqlalchemy.update(database.Post).
		where(database.Post.id == old_thread.id).
		values(id=new_thread.id)
	)
	flask.g.sa_session.execute(
		sqlalchemy.update(database.ThreadVote).
		where(database.ThreadVote.thread_id == old_thread.id).
		values(thread_id=new_thread.id)
	)
	flask.g.sa_session.execute(
		sqlalchemy.update(database.Notification).
		where(
			sqlalchemy.and_(
				database.Notification.type.in_(database.Thread.NOTIFICATION_TYPES),
				database.Notification.identifier == old_thread.id
			)
		).
		values(identifier=new_thread.id)
	)

	old_thread.delete()
	new_thread.edited(current_utc_time=flask.g.current_utc_time)

	return flask.jsonify(new_thread), http.client.OK


@thread_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on any thread, irrespective of which one it is.

	:returns: The list of allowed actions, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		database.Thread.get_allowed_static_actions(flask.g.user)
	), http.client.OK
