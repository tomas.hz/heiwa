r"""API views for OpenID Connect authentication of
:class:`User <heiwa.database.User>`\ s.

.. seealso::
	`Official OpenID Connect specification <https://openid.net/specs/openid-co\
	nect-core-1_0.html>`_
"""

import datetime
import http.client
import io
import mimetypes
import typing
import uuid

import authlib.common.security
import authlib.integrations.requests_client
import authlib.jose
import authlib.oidc.core
import flask
import PIL
import requests
import sqlalchemy

from heiwa import (
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import get_user_jwt, get_identifier_hash

__all__ = [
	"authenticate",
	"list_",
	"login",
	"oidc_blueprint"
]
__version__ = "1.0.9"

oidc_blueprint = flask.Blueprint(
	"oidc",
	__name__,
	url_prefix="/oidc"
)


@oidc_blueprint.route("", methods=["GET"])
@limiter.rate_limited
def list_() -> typing.Tuple[flask.Response, int]:
	"""Gets all OIDC services registered in the ``OIDC_SERVICES`` config key.
	The sensitive data stored within isn't included, only their names are.

	:returns: The list of OIDC services, with a ``200`` HTTP status code.
	"""

	result = {}

	for identifier, values in flask.current_app.config["OIDC_SERVICES"].items():
		result[identifier] = values["name"]

	return flask.jsonify(result), http.client.OK


@oidc_blueprint.route("/<string:client_name>/login", methods=["GET"])
@validators.validate_json({
	"redirect_url": {
		"type": "string",
		"minlength": 8,
		"maxlength": 2048,
		"check_with": "is_public_url",
		"required": True
	}
})
@limiter.rate_limited
def login(client_name: str) -> typing.Tuple[flask.Response, int]:
	"""Creates a URL for a user to log in through, and user the
	:func:`.authenticate` endpoint. The login attempt is stored using the
	:class:`OIDCAuthentication <heiwa.database.OIDCAuthentication>` model,
	to secure against potential attacks. It is deleted shortly thereafter.

	:param client_name: The OIDC service to use.

	:raises heiwa.exceptions.APIOIDCServiceNotFound: Raised when the requested
		OIDC service was not found in the ``OIDC_SERVICES`` config key.

	:returns: The URL to log in through, a state and a nonce. ``200`` HTTP status
		code.
	"""

	if client_name not in flask.current_app.config["OIDC_SERVICES"]:
		raise exceptions.APIOIDCServiceNotFound

	session_kwargs = flask.current_app.config["OIDC_SERVICES"][client_name]
	session_kwargs.pop("name")

	with authlib.integrations.requests_client.OAuth2Session(
		**session_kwargs
	) as oa2_session:
		nonce = authlib.common.security.generate_token()

		url, state = oa2_session.create_authorization_url(
			oa2_session.metadata["authorization_endpoint"],
			redirect_uri=flask.g.json["redirect_url"],
			nonce=nonce
		)

		hashed_identifier = get_identifier_hash()

		existing_authentication = flask.g.sa_session.get(
			database.OIDCAuthentication,
			hashed_identifier
		)

		if existing_authentication is not None:
			existing_authentication.delete()

		database.OIDCAuthentication.create(
			flask.g.sa_session,
			identifier=hashed_identifier,
			nonce=nonce,
			state=state
		)

		return flask.jsonify({
			"url": url,
			"state": state,
			"nonce": nonce
		}), http.client.OK


@oidc_blueprint.route("/<string:client_name>/authenticate", methods=["POST"])
@validators.validate_json({
	"code": {
		"type": "string",
		"required": True
	},
	"state": {
		"type": "string",
		"required": True
	},
	"redirect_url": {
		"type": "string",
		"minlength": 8,
		"maxlength": 2048,
		"check_with": "is_public_url",
		"required": True
	}
})
@limiter.rate_limited
def authenticate(client_name: str) -> typing.Tuple[flask.Response, int]:
	"""Creates a JWT for the user who the ``client_name`` OIDC service
	points to. If there is no user with its external identifier, a new one is
	automatically created.

	If the ``userinfo`` claim also contains an avatar, it's automatically given
	to the user.

	If the ``scope`` key in the OIDC service's config entry does not contain
	``openid``, a warning is logged.

	If user names are required to be unique by the current app's config, a
	random UUID one is automatically chosen. Collisions are handled, should the
	extraordinarily small chance of one happening come true.

	:param client_name: The OIDC service's name.

	:raises heiwa.exceptions.APIOIDCServiceNotFound: Raised when the OIDC
		service is not present in the current app's config.
	:raises heiwa.exceptions.APIOIDCStateInvalid: Raised when the OIDC state
		is not valid. This is validated using the
		:class:`OIDCAuthentication <heiwa.database.OIDCAuthentication>` model.
	:raises heiwa.exceptions.APIOIDCAuthenticationFailed: Raised when the
		authentication failed on the server's end for any reason. If available,
		more information is given in its details.
	:raises heiwa.exceptions.APIOIDCNonceInvalid: Raised when the presented
		``nonce`` does not match the recorded one. This can signal a potential
		replay attack.

	:returns: A JWT to access the user's account, with either the ``201`` or
		``200`` status code, depending on whether the user already exists or
		they've been newly created.

	.. seealso::
		:class:`heiwa.database.User`

		:func:`heiwa.views.utils.get_user_jwt`

		``OIDC_SERVICES`` config key
	"""

	if client_name not in flask.current_app.config["OIDC_SERVICES"]:
		raise exceptions.APIOIDCServiceNotFound

	# Delete all expired entries
	flask.g.sa_session.execute(
		sqlalchemy.delete(database.OIDCAuthentication).
		where(
			database.OIDCAuthentication.creation_timestamp
			<= (
				flask.g.current_utc_time
				- datetime.timedelta(
					seconds=flask.current_app.config["OIDC_AUTHENTICATION_EXPIRES_AFTER"]
				)
			)
		)
	)

	authentication = flask.g.sa_session.execute(
		sqlalchemy.select(database.OIDCAuthentication).
		where(
			sqlalchemy.and_(
				database.OIDCAuthentication.identifier == get_identifier_hash(),
				database.OIDCAuthentication.state == flask.g.json["state"]
			)
		)
	).scalars().one_or_none()

	if authentication is None:
		raise exceptions.APIOIDCStateInvalid

	if (
		"openid" not in
		flask.current_app.config["OIDC_SERVICES"][client_name].get("scope", [])
	):
		flask.current_app.logger.warning(
			"OIDC service %s has no `openid` scope",
			client_name
		)

	session_kwargs = flask.current_app.config["OIDC_SERVICES"][client_name]
	session_kwargs.pop("name")

	with authlib.integrations.requests_client.OAuth2Session(
		**session_kwargs
	) as oa2_session:
		try:
			token = oa2_session.fetch_token(
				oa2_session.metadata["token_endpoint"],
				**flask.g.json
			)
		except authlib.integrations.requests_client.OAuthError as exception:
			raise exceptions.APIOIDCAuthenticationFailed(
				exception.error
			) from exception
		except requests.HTTPError as exception:
			raise exceptions.APIOIDCAuthenticationFailed from exception

		jwks = oa2_session.get(oa2_session.metadata["jwks_uri"]).json()

		claims = authlib.jose.jwt.decode(
			token["id_token"],
			jwks,
			claims_cls=authlib.oidc.core.CodeIDToken
		)
		claims.validate()

		if claims["nonce"] != authentication.nonce:
			raise exceptions.APIOIDCNonceInvalid

		authentication.delete()

		userinfo = oa2_session.get(oa2_session.metadata["userinfo_endpoint"]).json()

		user = flask.g.sa_session.execute(
			sqlalchemy.select(database.User).
			where(
				sqlalchemy.and_(
					database.User.registered_by == "oidc",
					database.User.external_id == userinfo["sub"]
				)
			)
		).scalars().one_or_none()

		has_avatar = False

		if userinfo.get("picture") is not None:
			try:
				avatar = PIL.Image.open(
					io.BytesIO(
						requests.get(userinfo["picture"]).content
					)
				)

				avatar.verify()
			except OSError as exception:
				flask.current_app.logger.warning(
					"The %s OIDC service user ID %s has a set avatar, but it is invalid. "
					"Exception raised: %s",
					client_name,
					userinfo["sub"],
					exception
				)
			else:
				has_avatar = True

				guessed_type = mimetypes.guess_type(avatar.format.lower())

				if guessed_type not in flask.current_app.config["USER_AVATAR_TYPES"]:
					flask.current_app.logger.warning(
						"The %s OIDC service user ID %s has a set avatar, but its media "
						"type of %s is not allowed",
						client_name,
						userinfo["sub"],
						guessed_type
					)

		if user is None:
			user_name = userinfo.get("preferred_username")

			if flask.current_app.config["USER_NAME_REQUIRE_UNIQUE"]:
				while (
					flask.g.sa_session.execute(
						sqlalchemy.select(database.User.id).
						where(
							sqlalchemy.and_(
								~database.User.name.is_(None),
								database.User.name == user_name
							)
						).
						exists().
						select()
					).scalars().one()
				):
					old_user_name = user_name.copy()
					user_name = uuid.uuid4()

					flask.current_app.logger.info(
						f"User {old_user_name} would cause a name collision; "
						f"renaming to {user_name}"
					)

			user = database.User.create(
				flask.g.sa_session,
				registered_by="oidc",
				external_id=userinfo["sub"],
				name=userinfo.get("preferred_username"),
				write_kwargs={
					"current_app": flask.current_app
				}
			)

			if has_avatar:
				user.set_avatar(avatar)

				avatar.close()

			status = http.client.CREATED
		else:
			status = http.client.OK

		oa2_session.revoke_token(
			oa2_session.metadata["token_endpoint"],
			token=token["access_token"]
		)

		return flask.jsonify(get_user_jwt(user.id)), status
