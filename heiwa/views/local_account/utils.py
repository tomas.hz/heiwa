"""Utilities specific to the
:class:`LocalAccount <heiwa.database.LocalAccount>` views, but not only a
single module.
"""

import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import database, exceptions

__all__ = ["get_local_account"]


def get_local_account(
	user_id: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	validate_existence: bool = True
) -> database.LocalAccount:
	"""Gets the local account with the given ``user_id``.

	:param user_id: The :attr:`user_id <heiwa.database.LocalAccount.user_id>` of
		the account to find.
	:param session: The session to find the account with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param validate_existence: Whether or not to raise an exception when the
		account was not found. Defaults to :data:`True`.

	:raises heiwa.exceptions.APILocalAccountNotFound: Raised when the account
		does not exist.

	:returns: The account.
	"""

	if session is None:
		session = flask.g.sa_session

	account = session.execute(
		sqlalchemy.select(database.LocalAccount).
		where(database.LocalAccount.user_id == user_id)
	).scalars().one_or_none()

	if validate_existence and account is None:
		raise exceptions.APILocalAccountNotFound

	return account
