r"""API views for :class:`LocalAccount <heiwa.database.LocalAccount>`\ s."""

import datetime
import email.mime.multipart
import email.mime.text
import http.client
import smtplib
import typing

import authlib.jose
import flask
import flask_babel
import sqlalchemy

from heiwa import (
	authentication,
	database,
	exceptions,
	get_config,
	limiter,
	validators
)

from ..utils import get_user_jwt
from .totp import local_account_totp_blueprint
from .utils import get_local_account

__all__ = [
	"ATTR_SCHEMAS",
	"authenticate",
	"edit_meta",
	"edit_password",
	"get_local_account",
	"local_account_blueprint",
	"send_email",
	"send_email_password_reset",
	"send_email_verification",
	"register",
	"verify_email"
]
__version__ = "1.2.0"

local_account_blueprint = flask.Blueprint(
	"local_account",
	__name__,
	url_prefix="/local-account"
)

local_account_blueprint.register_blueprint(local_account_totp_blueprint)

config = get_config()

ATTR_SCHEMAS = {
	"email": {
		"type": "string",
		"minlength": 6,  # No internal addresses, ex. ``c@i.to``
		"maxlength": database.LocalAccount.email.property.columns[0].type.length,
		"check_with": "is_email"
	},
	"password": {
		"type": "string",
		"minlength": config["LOCAL_ACCOUNT_PASSWORD_MIN_LENGTH"],
		"maxlength": config["LOCAL_ACCOUNT_PASSWORD_MAX_LENGTH"]
	}
}


def send_email(
	sender: str,
	receiver: str,
	message: str
) -> None:
	"""Sends the ``message`` to the given ``receiver``.

	Any exceptions raised during the sending process are not caught and must be
	handled elsewhere.

	:param sender: The email address to send the message from.
	:param receiver: The email address to send the message to.
	:param message: The message, formatted as a string.
	"""

	smtp_class = (
		smtplib.SMTP_SSL
		if flask.current_app.config["LOCAL_ACCOUNT_EMAIL_SMTP_TLS"]
		else smtplib.SMTP
	)

	connection = smtp_class(
		flask.current_app.config["LOCAL_ACCOUNT_EMAIL_SMTP_SERVER"]
	)

	connection.login(
		flask.current_app.config["LOCAL_ACCOUNT_EMAIL_SMTP_USERNAME"],
		flask.current_app.config["LOCAL_ACCOUNT_EMAIL_SMTP_PASSWORD"]
	)

	try:
		connection.sendmail(
			sender,
			receiver,
			message
		)
	finally:  # Always close the connection
		connection.quit()


def send_email_verification(account: database.LocalAccount) -> None:
	"""Sends an email verification message to the given ``account``'s
	:attr:`email <heiwa.database.LocalAccount.email>`.

	The message will be sent in both plaintext and HTML, with HTML being the
	preferred and nicer looking version. Both are just as functional, however.

	If the ``LOCAL_ACCOUNT_EMAIL_SMTP_TLS`` config value is :data:`True`
	(which it usually should be), an encrypted SMTP connection is used.

	To generate a frontend URL the user will use to verify, the
	``LOCAL_ACCOUNT_VERIFICATION_URL`` config key is used. It must contain
	``{jwt}`` somewhere, which will be replaced using the string
	:meth:`format <str.format>` method.

	To avoid (admittedly very unlikely) collisions with other views using
	JWTs, the ``sub`` claim is prepended with the
	`LocalAccount <heiwa.database.LocalAccount>` class name and ``:``.

	``:email_verification`` is also added at the end, since JWTs are also used for
	local accounts' password resets. This is referred to as the **scope**.

	:param account: The local account to send the verification to.
	"""

	message = email.mime.multipart.MIMEMultipart("alternative")

	message["Subject"] = (
		flask_babel.gettext("Verify your email for {meta_name}").
		format(meta_name=flask.current_app.config["META_NAME"])
	)
	message["From"] = (
		flask.current_app.config["LOCAL_ACCOUNT_VERIFICATION_SMTP_ADDRESS"]
	)
	message["To"] = account.email

	current_timestamp = round(flask.g.current_utc_time.timestamp())

	verification_jwt = authlib.jose.jwt.encode(
		{
			"alg": "HS256",
			"typ": "JWT"
		},
		{
			"iss": flask.current_app.config["META_NAME"],
			"iat": current_timestamp,
			"exp": (
				current_timestamp
				+ flask.current_app.config["LOCAL_ACCOUNT_VERIFICATION_JWT_EXPIRES_AFTER"]
			),
			"sub": (
				f"{database.LocalAccount.__class__.__name__}:{account.id}"
				":email_verification"
			)
		},
		flask.current_app.config["LOCAL_ACCOUNT_VERIFICATION_JWT_SECRET_KEY"]
	).decode("utf-8")

	confirmation_url = (
		flask.current_app.config["LOCAL_ACCOUNT_VERIFICATION_URL"].
		format(jwt=verification_jwt)
	)

	message.attach(
		email.mime.text.MIMEText(
			flask_babel.gettext(
				"Hello! Someone has used your email address to register for "
				"{meta_name}. If this was you, please use this link to confirm it: "
				"{confirmation_url}. If not, you can ignore this email or contact "
				"us at: {meta_contact}."
			).
			format(
				meta_name=flask.current_app.config["META_NAME"],
				confirmation_url=confirmation_url,
				meta_contact=flask.current_app.config["META_CONTACT"]
			)
		)
	)
	message.attach(
		email.mime.text.MIMEText(
			flask.render_template(
				"local_account_email_verification.html",
				confirmation_url=confirmation_url
			),
			_subtype="html"
		)
	)

	flask.current_app.logger.info(
		"Sending email verification message from %s to %s",
		message["From"],
		message["To"]
	)

	send_email(
		flask.current_app.config["LOCAL_ACCOUNT_VERIFICATION_SMTP_ADDRESS"],
		account.email,
		message.as_string()
	)


def send_email_password_reset(account: database.LocalAccount) -> None:
	"""Sends a password reset message to the given ``account``'s
	:attr:`email <heiwa.database.LocalAccount.email>`.

	The message will be sent in both plaintext and HTML, with HTML being the
	preferred and nicer looking version. Both are just as functional, however.

	If the ``LOCAL_ACCOUNT_EMAIL_SMTP_TLS`` config value is :data:`True`
	(which it usually should be), an encrypted SMTP connection is used.

	To generate a frontend URL the user will use to verify, the
	``LOCAL_ACCOUNT_PASSWORD_RESET_URL`` config key is used. It must contain
	``{jwt}`` somewhere, which will be replaced using the string
	:meth:`format <str.format>` method.

	The JWT secret is the user's current
	:attr:`_hashed_password <heiwa.database.LocalAccount._hashed_password>`,
	combined with the ISO-8601 string of their
	:attr:`creation_timestamp <heiwa.database.LocalAccount.creation_timestamp>`
	and a ``-`` inbetween. This makes the JWT unique for each password reset.
	Inspired by `Jamie Munro <https://www.smashingmagazine.com/2017/11/safe-pa\
	ssword-resets-with-json-web-tokens/>`_, to whom I am very thankful. :)

	To avoid (admittedly very unlikely) collisions with other views using
	JWTs, the ``sub`` claim is prepended with the
	`LocalAccount <heiwa.database.LocalAccount>` class name and ``:``.

	``:password_reset`` is also added at the end, since JWTs are also used for
	local accounts' email verifications. This is referred to as the **scope**.

	:param account: The local account to send the verification to.
	"""

	message = email.mime.multipart.MIMEMultipart("alternative")

	message["Subject"] = (
		flask_babel.gettext("Reset your password for {meta_name}").
		format(meta_name=flask.current_app.config["META_NAME"])
	)
	message["From"] = (
		flask.current_app.config["LOCAL_ACCOUNT_PASSWORD_RESET_SMTP_ADDRESS"]
	)
	message["To"] = account.email

	current_timestamp = round(flask.g.current_utc_time.timestamp())

	password_reset_jwt = authlib.jose.jwt.encode(
		{
			"alg": "HS256",
			"typ": "JWT"
		},
		{
			"iss": flask.current_app.config["META_NAME"],
			"iat": current_timestamp,
			"exp": (
				current_timestamp
				+ flask.current_app.config[
					"LOCAL_ACCOUNT_PASSWORD_RESET_JWT_EXPIRES_AFTER"
				]
			),
			"sub": (
				f"{database.LocalAccount.__class__.__name__}:{account.id}:password_reset"
			)
		},
		(
			account._hashed_password
			+ "-"
			+ account.creation_timestamp.isoformat()
		)
	).decode("utf-8")

	password_reset_url = (
		flask.current_app.config["LOCAL_ACCOUNT_PASSWORD_RESET_URL"].
		format(jwt=password_reset_jwt)
	)

	message.attach(
		email.mime.text.MIMEText(
			flask_babel.gettext(
				"Hello! Someone asked for your account on {meta_name}'s password to "
				"be reset. If this was you, please use this link to continue: "
				"{password_reset_url}. If not, you can ignore this email or contact "
				"us at: {meta_contact}."
			).
			format(
				meta_name=flask.current_app.config["META_NAME"],
				password_reset_url=password_reset_url,
				meta_contact=flask.current_app.config["META_CONTACT"]
			)
		)
	)
	message.attach(
		email.mime.text.MIMEText(
			flask.render_template(
				"local_account_password_reset.html",
				password_reset_url=password_reset_url
			),
			_subtype="html"
		)
	)

	flask.current_app.logger.info(
		"Sending password reset message from %s to %s",
		message["From"],
		message["To"]
	)

	send_email(
		flask.current_app.config["LOCAL_ACCOUNT_PASSWORD_RESET_SMTP_ADDRESS"],
		account.email,
		message.as_string()
	)


@local_account_blueprint.route("/register", methods=["POST"])
@validators.validate_json({
	"email": {
		**ATTR_SCHEMAS["email"],
		"required": True
	},
	"password": {
		**ATTR_SCHEMAS["password"],
		"required": True
	}
})
@limiter.rate_limited
def register() -> typing.Union[flask.Response, int]:
	"""Creates a local account with the given ``email`` and password.

	Uses the :func:`.send_email_verification` function to send a verification
	email before the actual corresponding :class:`User <heiwa.database.User>`
	is created.

	:raises heiwa.exceptions.APILocalAccountEmailAlreadyUsed: Raised when another
		account is already using the given ``email``.
	:raises heiwa.exceptions.APILocalAccountVerificationEmailSendingFailed: Raised
		when the given ``email``, for any reason, wasn't able to receive the
		message. **This assumes our config is valid and the SMTP server always
		works.**

	:returns: :data:`None` with the HTTP ``204`` status code.
	"""

	if (
		flask.g.sa_session.execute(
			sqlalchemy.select(database.LocalAccount.email).
			where(database.LocalAccount.email == flask.g.json["email"]).
			exists().
			select()
		).scalars().one()
	):
		raise exceptions.APILocalAccountEmailAlreadyUsed

	account = database.LocalAccount.create(
		flask.g.sa_session,
		email=flask.g.json["email"],
		write_kwargs={
			"password": flask.g.json["password"].encode("utf-8")
		}
	)
	flask.g.sa_session.flush()

	try:
		send_email_verification(account)
	except smtplib.SMTPException as exception:
		account.delete()

		raise exceptions.APILocalAccountVerificationEmailSendingFailed from exception

	return flask.jsonify(None), http.client.NO_CONTENT


@local_account_blueprint.route("/verify-email", methods=["POST"])
@validators.validate_json({
	"token": {
		"type": "string",
		# Don't validate base64, JWT uses dots to separate values
		"minlength": 4,
		# 2048 is entirely arbitrary, but should be more than enough for 256
		# 4-byte UTF-8 characters in the ``iss`` field and 2^64 in both timestamp
		# values.
		"maxlength": 2048,
		"required": True
	}
})
@limiter.rate_limited
def verify_email() -> typing.Union[flask.Response, int]:
	r"""Verifies the :attr:`email <heiwa.database.LocalAccount.email>` addres
	for the account presented in the JWT (``token``).

	When this view is used, all unverieid accounts older than the amount of
	seconds in the ``OIDC_AUTHENTICATION_EXPIRES_AFTER`` config key are
	automatically deleted.

	Once the verification is complete, a :class:`User <heiwa.database.User>` is
	created - if one isn't associated with the account already. Note that it has
	no set name or avatar - those must be set through the
	:mod:`user <heiwa.views.user>` set of views.

	:raises heiwa.exceptions.APILocalAccountVerificationJWTInvalid: Raised when
		the JWT is, for any reason, invalid. This can, for example, happen when
		the presented base64 data is valid, but does not represent a JWT.
	:raises heiwa.exceptions.APILocalAccountVerificationJWTInvalidClaims: Raised
		when the presented JWT is valid, but its claims are not.
	:raises heiwa.exceptions.APILocalAccountVerificationJWTSubjectNotFound: Raised
		when the JWT's ``sub`` did not correspond to any account. This shouldn't
		happen in general either, but it could under certain circumstances - for
		example, a user attempting to verify their email on a service which has
		recently been reset, but kept the old secret key.
	:raises heiwa.exceptions.APILocalAccountVerificationJWTInvalidSubjectResour\
	ceType: Raised when the resource type identifier (the string before the first
		``:`` in the ``sub`` claim) does not match the
		:class:`LocalAccount <heiwa.database.LocalAccount>` class name.
	:raises heiwa.exceptions.APILocalAccountVerificationJWTInvalidScope: Raised
		when the scope (the string after the second ``:`` in the ``sub`` claim)
		is not ``verification``. This can mean the token is valid, but supposed
		to be used for a different purpose.

	:returns: If there was no :class:`User <heiwa.database.User>` before, a JWT
		for them, with the HTTP ``201`` status code. Otherwise, :data:`None`
		with the ``204`` code.
	"""

	# Delete old unverified accounts
	flask.g.sa_session.execute(
		sqlalchemy.delete(database.LocalAccount).
		where(
			sqlalchemy.and_(
				database.LocalAccount.email_is_verified.is_(False),
				(
					database.LocalAccount.creation_timestamp
					<= (
						flask.g.current_utc_time
						- datetime.timedelta(
							seconds=(
								flask.current_app.config[
									"LOCAL_ACCOUNT_VERIFICATION_JWT_EXPIRES_AFTER"
								]
							)
						)
					)
				)
			)
		)
	)

	try:
		claims = authlib.jose.jwt.decode(
			flask.g.json["token"],
			flask.current_app.config["LOCAL_ACCOUNT_VERIFICATION_JWT_SECRET_KEY"]
		)
	except authlib.jose.JoseError as exception:
		raise exceptions.APILocalAccountVerificationJWTInvalid from exception

	try:
		claims.validate()
	except authlib.jose.JoseError as exception:
		raise exceptions.APILocalAccountVerificationJWTInvalidClaims from exception

	resource_type, id_, scope = claims["sub"].split(":")

	if resource_type != database.LocalAccount.__class__.__name__:
		raise exceptions.APILocalAccountVerificationJWTInvalidSubjectResourceType

	if scope != "email_verification":
		raise exceptions.APILocalAccountVerificationJWTInvalidScope

	account = flask.g.sa_session.get(
		database.LocalAccount,
		id_
	)

	if account is None:
		raise exceptions.APILocalAccountVerificationJWTSubjectNotFound(id_)

	if account.email_is_verified:
		raise exceptions.APILocalAccountVerificationAlreadyDone

	account.email_is_verified = True

	if not (
		flask.g.sa_session.execute(
			sqlalchemy.select(database.User.external_id).
			where(
				sqlalchemy.and_(
					database.User.registered_by == "local_account",
					database.User.external_id == str(account.id)
				)
			).
			exists().
			select()
		).scalars().one()
	):
		user = database.User.create(
			flask.g.sa_session,
			registered_by="local_account",
			external_id=str(account.id)
		)

		account.user_id = user.id

		response = (
			flask.jsonify(get_user_jwt(user.id)),
			http.client.CREATED
		)
	else:
		response = (
			flask.jsonify(None),
			http.client.NO_CONTENT
		)

	flask.g.sa_session.commit()

	return response


@local_account_blueprint.route("", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view() -> typing.Tuple[flask.Response, int]:
	"""Gets the current :attr:`flask.g.user`'s local account.

	:returns: The local account, if there is one. If there is none, :data:`None`
		is returned with the same HTTP status code - ``200``.
	"""

	account = get_local_account(
		flask.g.user.id,
		validate_existence=False
	)

	return flask.jsonify(account), http.client.OK


@local_account_blueprint.route("/meta", methods=["PUT"])
@validators.validate_json({
	"email": {
		**ATTR_SCHEMAS["email"],
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
def edit_meta() -> typing.Tuple[flask.Response, int]:
	"""Updates :attr:`flask.g.user`'s local account with the given values. This
	view is only for meta.

	If the email is being changed, the account automatically becomes unverified
	and a confirmation link is sent to the provided new address.

	:raises heiwa.exceptions.APILocalAccountNotFound: Raised when the user is
		not registered through a local account and thus has none.
	:raises heiwa.exceptions.APILocalAccountUnchanged: Raised when none of the
		existing attributes (currently only ``email``) have been changed.
	:raises heiwa.exceptions.APILocalAccountEmailAlreadyUsed: Raised when another
		account is already using the given ``email``.
	:raises heiwa.exceptions.APILocalAccountVerificationEmailSendingFailed: Raised
		when the given ``email``, for any reason, wasn't able to receive the
		message. **This assumes our config is valid and the SMTP server always
		works.**

	:returns: The updated account, with the HTTP ``200`` status code.

	.. seealso::
		To change the password, use :func:`.edit_password`.
	"""

	account = get_local_account(flask.g.user.id)

	if flask.g.json["email"] == account.email:
		raise exceptions.APILocalAccountUnchanged

	if (
		flask.g.sa_session.execute(
			sqlalchemy.select(database.LocalAccount.email).
			where(database.LocalAccount.email == flask.g.json["email"]).
			exists().
			select()
		).scalars().one()
	):
		raise exceptions.APILocalAccountEmailAlreadyUsed

	account.email = flask.g.json["email"]

	try:
		send_email_verification(account)
	except smtplib.SMTPException as exception:
		raise exceptions.APILocalAccountVerificationEmailSendingFailed from exception

	account.email_is_verified = False

	return flask.jsonify(account), http.client.OK


@local_account_blueprint.route("/password", methods=["PUT"])
@validators.validate_json({
	"old_password": {
		**ATTR_SCHEMAS["password"],
		"required": True
	},
	"new_password": {
		**ATTR_SCHEMAS["password"],
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
def edit_password() -> typing.Tuple[flask.Response, int]:
	"""Updates :attr:`flask.g.user`'s local account's password.

	:raises heiwa.exceptions.APILocalAccountNotFound: Raised when the user is
		not registered through a local account and thus has none.
	:raises heiwa.exceptions.APILocalAccountUnchanged: Raised when the password
		has not been changed.
	:raises heiwa.exceptions.APILocalAccountPasswordInvalid: Raised when the
		``old_password`` does not match the recorded hashed one.

	:returns: The updated account, with the HTTP ``200`` status code.

	.. seealso::
		To change account meta, use :func:`.edit_meta`.
	"""

	account = get_local_account(flask.g.user.id)

	if flask.g.json["old_password"] == flask.g.json["new_password"]:
		raise exceptions.APILocalAccountUnchanged

	if not account.get_hashed_password_match(flask.g.json["old_password"]):
		raise exceptions.APILocalAccountPasswordInvalid

	account.set_hashed_password(flask.g.json["new_password"])

	return flask.jsonify(account), http.client.OK


@local_account_blueprint.route("/authenticate", methods=["POST"])
@validators.validate_json({
	"email": {
		**ATTR_SCHEMAS["email"],
		"required": True
	},
	"password": {
		**ATTR_SCHEMAS["password"],
		"required": True
	},
	"totp_code": {
		"type": "string",
		"minlength": 6,
		"maxlength": 6,
		"excludes": "totp_backup_code",
		"nullable": True,
		"required": True
	},
	"totp_backup_code": {
		"type": "string",
		"minlength": 32,
		"maxlength": 32,
		"excludes": "totp_code",
		"nullable": True,
		"required": True
	}
})
@limiter.rate_limited
def authenticate() -> typing.Tuple[flask.Response, int]:
	"""Authenticates a local account as per the given details.

	:raises heiwa.exceptions.APILocalAccountNotFound: Raised when there is no
		account with the given ``email`` to be found.
	:raises heiwa.exceptions.APILocalAccountEmailNeverVerified: Raised when the
		account exists, but has never been verified. Unless the user can verify
		it, the account is scheduled to be deleted.
	:raises heiwa.exceptions.APILocalAccountPasswordInvalid: Raised when the
		given ``password`` does not match.
	:raises heiwa.exceptions.APILocalAccountTOTPCodeInvalid: Raised when the given
		``totp_code`` does not match the secret, or has been used before.
	:raises heiwa.exceptions.APILocalAccountTOTPBackupCodeInvalid: Raised when
		the given ``totp_backup_code`` does not match any of the known ones.

	:returns: A JWT for the corresponding :class:`User <heiwa.database.User>`,
		with the HTTP ``200`` status code.
	"""

	account = flask.g.sa_session.execute(
		sqlalchemy.select(database.LocalAccount).
		where(database.LocalAccount.email == flask.g.json["email"])
	).scalars().one_or_none()

	if account is None:
		raise exceptions.APILocalAccountNotFound

	if (
		not account.email_is_verified
		and account.user_id is None
	):
		raise exceptions.APILocalAccountEmailNeverVerified

	if not account.get_hashed_password_match(flask.g.json["password"]):
		raise exceptions.APILocalAccountPasswordInvalid

	if account.has_totp:
		if (
			"totp_code" in flask.g.json
			and not account.totp.get_code_valid(flask.g.json["totp_code"])
		):
			raise exceptions.APILocalAccountTOTPCodeInvalid

		if flask.g.json["totp_backup_code"] not in account.backup_codes:
			raise exceptions.APILocalAccountTOTPBackupCodeInvalid

		account.backup_codes.remove(flask.g.json["totp_backup_code"])

	return flask.jsonify(get_user_jwt(account.user_id)), http.client.OK


@local_account_blueprint.route("/password-reset/request", methods=["POST"])
@validators.validate_json({
	"email": {
		**ATTR_SCHEMAS["email"],
		"required": True
	}
})
@limiter.rate_limited
def password_reset_request() -> typing.Union[flask.Response, int]:
	"""Creates a request for the account with the given ``email``'s password to
	be reset.

	Uses the :func:`.send_email_password_reset` function to send the email.

	:raises heiwa.exceptions.APILocalAccountNotFound: Raised when the given
		``email`` does not correspond to any account.
	:raises heiwa.exceptions.APILocalAccountPasswordResetEmailSendingFailed:
		Raised when, for any reason, the email was unable to be sent. More
		information is available in this exception's documentation.

	:returns: :data:`None` with the HTTP ``204`` status code.
	"""

	account = flask.g.sa_session.execute(
		sqlalchemy.select(database.LocalAccount).
		where(database.LocalAccount.email == flask.g.json["email"])
	).scalars().one_or_none()

	if account is None:
		raise exceptions.APILocalAccountNotFound

	try:
		send_email_password_reset(account)
	except smtplib.SMTPException as exception:
		raise (
			exceptions.APILocalAccountPasswordResetEmailSendingFailed
		) from exception

	return flask.jsonify(None), http.client.NO_CONTENT


@local_account_blueprint.route("/password-reset/verify", methods=["POST"])
@validators.validate_json({
	"email": {
		**ATTR_SCHEMAS["email"],
		"required": True
	},
	"token": {
		"type": "string",
		# Don't validate base64, JWT uses dots to separate values
		"minlength": 4,
		# 2048 is entirely arbitrary, but should be more than enough for 256
		# 4-byte UTF-8 characters in the ``iss`` field and 2^64 in both timestamp
		# values.
		"maxlength": 2048,
		"required": True
	},
	"password": {
		**ATTR_SCHEMAS["password"],
		"required": True
	}
})
@limiter.rate_limited
def password_reset_verify() -> typing.Union[flask.Response, int]:
	r"""Verifies the password reset based on the given JWT ``token``, decoded
	with the account corresponding to ``email``'s old password and creation
	timestamp. :func:`.send_email_password_reset` describes this in more detail.

	The new password is set to the ``password`` key, as long as it does not match
	the old password.

	:raises heiwa.exceptions.APILocalAccountNotFound: Raised when an account
		corresponding to the given ``email`` could not be found.
	:raises heiwa.exceptions.APILocalAccountPasswordResetJWTInvalid: Raised when
		the provided JWT is invalid and could not be decoded at all.
	:raises heiwa.exceptions.APILocalAccountPasswordResetJWTInvalidClaims: Raised
		when the provided JWT was able to be decoded, but its claims are invalid.
		This usually means an expired token, or in rare cases, a forged one.
	:raises heiwa.exceptions.APILocalAccountPasswordResetJWTInvalidSubjectReso\
		urceType: Raised when the resource type (the string before the first
		``:`` in the ``sub`` claim) is not the local account class name.
	:raises heiwa.exceptions.APILocalAccountPasswordResetJWTInvalidScope: Raised
		when the scope (the string after the second ``:`` in the ``sub`` claim)
		is not ``password_reset``.
	:raises heiwa.exceptions.APILocalAccountPasswordResetJWTSubjectMismatch:
		Raised when the account with the given ``email`` is not the one the JWT
		refers to.
	:raises heiwa.exceptions.APILocalAccountUnchanged: Raised when the given
		``password`` matches the account's current one.

	:returns: A JWT for the account's corresponding user, with the HTTP ``200``
		status code.
	"""

	account = flask.g.sa_session.execute(
		sqlalchemy.select(database.LocalAccount).
		where(database.LocalAccount.email == flask.g.json["email"])
	).scalars().one_or_none()

	if account is None:
		raise exceptions.APILocalAccountNotFound

	try:
		claims = authlib.jose.jwt.decode(
			flask.g.json["token"],
			(
				account._hashed_password
				+ "-"
				+ account.creation_timestamp.isoformat()
			)
		)
	except authlib.jose.JoseError as exception:
		raise exceptions.APILocalAccountPasswordResetJWTInvalid from exception

	try:
		claims.validate()
	except authlib.jose.JoseError as exception:
		raise exceptions.APILocalAccountPasswordResetJWTInvalidClaims from exception

	resource_type, id_, scope = claims["sub"].split(":")

	if resource_type != database.LocalAccount.__class__.__name__:
		raise exceptions.APILocalAccountPasswordResetJWTInvalidSubjectResourceType

	if scope != "password_reset":
		raise exceptions.APILocalAccountPasswordResetJWTInvalidScope

	if account.id != id_:
		raise exceptions.APILocalAccountPasswordResetJWTSubjectMismatch({
			"account_id": account.id,
			"token_id": id_
		})

	if account.get_hashed_password_match(flask.g.json["password"]):
		raise exceptions.APILocalAccountUnchanged

	account.set_hashed_password(flask.g.json["password"])

	return (
		flask.jsonify(get_user_jwt(account.user_id)),
		http.client.OK
	)
