r"""API views for
:class:`UserPreferences <heiwa.database.UserPreferences>`, belonging under
the base set of views for :class:`User <heiwa.database.User>`\ s.
"""

import http.client
import typing

import flask

from heiwa import (
	authentication,
	exceptions,
	limiter,
	validators
)

from ..utils import handle_edit

__all__ = [
	"edit",
	"user_preferences_blueprint"
]

user_preferences_blueprint = flask.Blueprint(
	"preferences",
	__name__,
	url_prefix="/self/preferences"
)


@user_preferences_blueprint.route("", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view() -> typing.Tuple[flask.Response, int]:
	"""Gets the current :attr:`flask.g.user`'s preferences.

	No raised exceptions can occur here, since preferences will always
	exist and be visible only to their owner.

	:returns: The preferences, with the ``200`` HTTP status code.
	"""

	return flask.jsonify(flask.g.user.preferences), http.client.OK


@user_preferences_blueprint.route("", methods=["PUT"])
@validators.validate_json({
	"should_record_activity": {
		"type": "boolean",
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
def edit() -> typing.Tuple[flask.Response, int]:
	"""Updates the current :attr:`flask.g.user`'s preferences with the given
	values.

	:raises heiwa.exceptions.APIUserPreferencesUnchanged: Raised when all values
		are the same as the previous ones.

	:returns: The updated preferences, with the ``200`` HTTP status code.
	"""

	handle_edit(
		flask.g.user.preferences,
		flask.g.json,
		exceptions.APIUserPreferencesUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	return flask.jsonify(flask.g.user.preferences), http.client.OK
