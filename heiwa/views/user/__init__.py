r"""API views for :class:`User <heiwa.database.User>`\ s."""

import http.client
import typing
import uuid

import Crypto.PublicKey.RSA
import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)
from .avatar import user_avatar_blueprint
from .ban import user_ban_blueprint
from .block import user_block_blueprint
from .follow import user_follow_blueprint
from .groups import user_groups_blueprint
from .utils import get_user_self_or_id
from .permissions import user_permissions_blueprint
from .preferences import user_preferences_blueprint
from .reputation import user_reputation_blueprint

__all__ = [
	"ATTR_SCHEMAS",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"delete",
	"edit",
	"list_",
	"mass_delete",
	"mass_edit",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"user_avatar_blueprint",
	"user_ban_blueprint",
	"user_block_blueprint",
	"user_blueprint",
	"user_follow_blueprint",
	"user_groups_blueprint",
	"user_permissions_blueprint",
	"user_preferences_blueprint",
	"user_reputation_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.3.10"

user_blueprint = flask.Blueprint(
	"user",
	__name__
)

for blueprint in (
	user_avatar_blueprint,
	user_ban_blueprint,
	user_block_blueprint,
	user_follow_blueprint,
	user_groups_blueprint,
	user_permissions_blueprint,
	user_preferences_blueprint,
	user_reputation_blueprint
):
	user_blueprint.register_blueprint(blueprint)


def check_with_rsa_public_key_valid(
	field: str,
	value: typing.Union[None, bytes],
	error: typing.Callable[[str, str], None]
) -> None:
	"""Checks whether or not ``value`` is a valid RSA public key. If not, ``error``
	is called. This function should be used within a Cerberus validator schema.

	:param field: The current field.
	:param value: The field's value, in bytes. It can also be :data:`None`,
		meaning the current field is nullable. Nothing happens in this case
		and ``return`` is called early.
	:param error: A callback to be called when the validation failed.

	.. note::
		This function is stored in this module instead of
		:class:`Validator <heiwa.validators.Validator>`, since it's only ever
		going to be used here. If that's not the case in the future, it will be
		moved there.
	"""

	if value is None:
		return

	try:
		Crypto.PublicKey.RSA.import_key(value)
	except ValueError:
		error(field, "must be a valid RSA public key")


ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"is_banned": {
		"type": "boolean"
	},
	"encrypted_private_key": {
		"type": "binary",
		"coerce": "decode_base64",
		"minlength": 624,  # 1024-bit RSA private key, AES-CBC, block size 16
		"maxlength": 2352,  # 4096-bit private key, AES-CBC, block size 16
		"not_null_dependencies": "encrypted_private_key_iv"
	},
	"encrypted_private_key_iv": {
		"type": "binary",
		"coerce": "decode_base64",
		"minlength": 16,
		"maxlength": 16,
		"not_null_dependencies": "encrypted_private_key"
	},
	"public_key": {
		"type": "binary",
		"check_with": check_with_rsa_public_key_valid,
		"coerce": "decode_base64",
		"minlength": 161,  # 1024-bit RSA public key
		"maxlength": 550  # 4096-bit RSA public key
	},
	"avatar_type": {
		"type": "string",
		"minlength": 3,
		"maxlength": database.User.avatar_type.property.columns[0].type.length
	},
	"name": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.User.name.property.columns[0].type.length
	},
	"status": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.User.status.property.columns[0].type.length
	},
	"follower_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"post_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"thread_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	}
}

search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count",
		"follower_count",
		"post_count",
		"thread_count"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=user_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(user_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"follower_count": ATTR_SCHEMAS["follower_count"],
	"post_count": ATTR_SCHEMAS["post_count"],
	"thread_count": ATTR_SCHEMAS["thread_count"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"is_banned": ATTR_SCHEMAS["is_banned"],
			"public_key": ATTR_SCHEMAS["public_key"],
			"avatar_type": {
				**ATTR_SCHEMAS["avatar_type"],
				"nullable": True
			},
			"name": {
				**ATTR_SCHEMAS["name"],
				"nullable": True
			},
			"status": {
				**ATTR_SCHEMAS["status"],
				"nullable": True
			},
			"post_count": ATTR_SCHEMAS["post_count"],
			"thread_count": ATTR_SCHEMAS["thread_count"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"public_key": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["public_key"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"avatar_type": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["avatar_type"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"name": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["name"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"status": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["status"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"follower_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["follower_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"post_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["post_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"thread_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["thread_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"avatar_type": {
				**ATTR_SCHEMAS["avatar_type"],
				"check_with": "is_valid_regex"
			},
			"name": {
				**ATTR_SCHEMAS["name"],
				"check_with": "is_valid_regex"
			},
			"status": {
				**ATTR_SCHEMAS["status"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})


@user_blueprint.route("/users", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.User)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Gets all users that the current :attr:`flask.g.user` is allowed to view.
	If there is a filter requested, they must also match it.

	:returns: The list of users, with the HTTP ``200`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.User
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.User.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.User),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@user_blueprint.route("/users", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"name": {
					**ATTR_SCHEMAS["name"],
					"nullable": True
				},
				"status": {
					**ATTR_SCHEMAS["status"],
					"nullable": True
				},
				"encrypted_private_key": {
					**ATTR_SCHEMAS["encrypted_private_key"],
					"nullable": True
				},
				"encrypted_private_key_iv": {
					**ATTR_SCHEMAS["encrypted_private_key_iv"],
					"nullable": True
				},
				"public_key": {
					**ATTR_SCHEMAS["public_key"],
					"nullable": True
				}
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.User)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all users that the current :attr:`flask.g.user` is allowed to with
	the given values. If there is a filter requested, they must also match it.

	:raises heiwa.exceptions.APIUserNameNotUnique: Raised when names are required
		to be unique, the set name is not :data:`None` and 2 or more users would
		be edited, or a single user's name edit would cause a collision.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.User
			)
		)

	ids = flask.g.sa_session.execute(
		database.User.get(
			flask.g.sa_session,
			flask.g.user,
			action_conditions=database.User.action_queries["edit"](flask.g.user),
			conditions=conditions,
			order_by=get_order_by_expression(database.User),
			limit=flask.g.json["limit"],
			offset=flask.g.json["offset"]
		)
	).scalars().all()

	if (
		flask.g.json.get("name") is not None
		and flask.current_app.config["USER_NAME_REQUIRE_UNIQUE"]
		and (
			len(ids) > 1
			or flask.g.sa_session.execute(
				sqlalchemy.select(database.User.id).
				where(database.User.name == flask.g.json["name"]).
				exists().
				select()
			).scalars().one()
		)
	):
		raise exceptions.APIUserNameNotUnique

	flask.g.sa_session.execute(
		sqlalchemy.update(database.User).
		where(
			database.User.id.in_(ids)
		).
		values(**flask.g.json["values"])
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_blueprint.route("/users", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.User)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	"""Deletes all users that the current :attr:`flask.g.user` is allowed to. If
	there is a filter requested, they must also match it.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.User
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.User).
		where(
			database.User.id.in_(
				database.User.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=database.User.action_queries["delete"](flask.g.user),
					conditions=conditions,
					order_by=get_order_by_expression(database.User),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"]
				)
			)
		)
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_blueprint.route("/users/<uuid:id_>", methods=["GET"])
@user_blueprint.route(
	"/self",
	defaults={"id_": None},
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.User)
def view(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Gets the user with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, it's assumed to be that of the current :attr:`flask.g.user`.

	:returns: The user, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		get_user_self_or_id(
			id_,
			flask.g.sa_session
		)
	), http.client.OK


@user_blueprint.route("/users/<uuid:id_>", methods=["PUT"])
@user_blueprint.route(
	"/self",
	defaults={"id_": None},
	methods=["PUT"]
)
@validators.validate_json({
	"name": {
		**ATTR_SCHEMAS["name"],
		"nullable": True,
		"required": True
	},
	"status": {
		**ATTR_SCHEMAS["status"],
		"nullable": True,
		"required": True
	},
	"encrypted_private_key": {
		**ATTR_SCHEMAS["encrypted_private_key"],
		"nullable": True,
		"required": True
	},
	"encrypted_private_key_iv": {
		**ATTR_SCHEMAS["encrypted_private_key_iv"],
		"nullable": True,
		"required": True
	},
	"public_key": {
		**ATTR_SCHEMAS["public_key"],
		"nullable": True,
		"required": True
	}
})
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.User)
def edit(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Updates the user with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, it's assumed to be that of the current :attr:`flask.g.user`.

	:raises heiwa.exceptions.APIUserUnchanged: Raised when none of the user's
		attributes have been changed.
	:raises heiwa.exceptions.APIUserNameNotUnique: Raised when the name would
		change to one that would cause a name collision.

	:returns: The updated user, with the HTTP ``200`` status code.
	"""

	user = get_user_self_or_id(
		id_,
		flask.g.sa_session
	)

	validate_permission(
		flask.g.user,
		"edit",
		user
	)

	if (
		user.name != flask.g.json["name"]
		and flask.current_app.config["USER_NAME_REQUIRE_UNIQUE"]
		and flask.g.sa_session.execute(
			sqlalchemy.select(database.User.id).
			where(database.User.name == flask.g.json["name"]).
			exists().
			select()
		).scalars().one()
	):
		raise exceptions.APIUserNameNotUnique

	handle_edit(
		user,
		flask.g.json,
		exceptions.APIUserUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	flask.g.sa_session.commit()

	return flask.jsonify(user), http.client.OK


@user_blueprint.route(
	"/users/<uuid:id_>",
	methods=["DELETE"]
)
@user_blueprint.route(
	"/self",
	defaults={"id_": None},
	methods=["DELETE"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.User)
def delete(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Deletes the user with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, it's assumed to be that of the current :attr:`flask.g.user`.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	user = get_user_self_or_id(
		id_,
		flask.g.sa_session
	)

	validate_permission(
		flask.g.user,
		"delete",
		user
	)

	user.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@user_blueprint.route("/users/<uuid:id_>/allowed-actions", methods=["GET"])
@user_blueprint.route(
	"/self/allowed-actions",
	defaults={"id_": None},
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.User)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on the user with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, it's assumed to be that of the current :attr:`flask.g.user`.

	:returns: The list of allowed actions, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		get_user_self_or_id(
			id_,
			flask.g.sa_session
		).get_allowed_instance_actions(flask.g.user)
	), http.client.OK


@user_blueprint.route("/users/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on any user, irrespective of their identity.

	:returns: The list of allowed actions, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		database.User.get_allowed_static_actions(flask.g.user)
	), http.client.OK
