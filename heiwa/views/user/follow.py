r"""API views for
:obj:`user_follows <heiwa.database.user_follows>`, belonging under
the base set of views for :class:`User <heiwa.database.User>`\ s.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter
)

from ..utils import get_user, requires_permission, validate_permission

__all__ = [
	"create",
	"delete",
	"user_follow_blueprint",
	"view"
]

user_follow_blueprint = flask.Blueprint(
	"follow",
	__name__
)


@user_follow_blueprint.route("/users/<uuid:id_>/follow", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_follow", database.User)
def create(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Creates a follow from the current :attr:`flask.g.user` for the user with
	the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user.

	:raises heiwa.exceptions.APIUserFollowAlreadyExists: Raised when the current
		user has already followed this user before.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	user = get_user(id_)

	validate_permission(
		flask.g.user,
		"edit_follow",
		user
	)

	if flask.g.user.get_is_followee(user.id):
		raise exceptions.APIUserFollowAlreadyExists

	flask.g.sa_session.execute(
		sqlalchemy.insert(database.user_follows).
		values(
			follower_id=flask.g.user.id,
			followee_id=user.id
		)
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@user_follow_blueprint.route("/users/<uuid:id_>/follow", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view_follow", database.User)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets whether or not the current :attr:`flask.g.user` is following the user
	with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user.

	:returns: The boolean result, with the ``200`` HTTP status code.
	"""

	user = get_user(id_)

	validate_permission(
		flask.g.user,
		"view_follow",
		user
	)

	return flask.jsonify(
		flask.g.user.get_is_followee(user.id)
	), http.client.OK


@user_follow_blueprint.route("/users/<uuid:id_>/follow", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_follow", database.User)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Removes the current :attr:`flask.g.user`'s follow for the user with the
	given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user.

	:raises heiwa.exceptions.APIUserFollowNotFound: Raised when the current user
		has not followed this user, thus there is nothing to remove.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	user = get_user(id_)

	validate_permission(
		flask.g.user,
		"edit_follow",
		user
	)

	existing_follow = flask.g.sa_session.execute(
		sqlalchemy.select(database.user_follows).
		where(
			sqlalchemy.and_(
				database.user_follows.c.follower_id == flask.g.user.id,
				database.user_follows.c.followee_id == user.id
			)
		)
	).scalars().one_or_none()

	if existing_follow is None:
		raise exceptions.APIUserFollowNotFound

	flask.g.sa_session.delete(existing_follow)

	return flask.jsonify(None), http.client.NO_CONTENT
