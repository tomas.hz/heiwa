r"""API views for
:obj:`user_groups <heiwa.database.user_groups>`, belonging under
the base set of views for :class:`User <heiwa.database.User>`\ s.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter
)

from ..utils import get_group, requires_permission, validate_permission
from .utils import get_user_self_or_id

__all__ = [
	"add",
	"delete",
	"user_groups_blueprint",
	"list_"
]

user_groups_blueprint = flask.Blueprint(
	"groups",
	__name__
)


@user_groups_blueprint.route(
	"/users/<uuid:user_id>/groups/<uuid:group_id>",
	methods=["POST"]
)
@user_groups_blueprint.route(
	"/self/groups/<uuid:group_id>",
	defaults={"user_id": None},
	methods=["POST"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_group", database.User)
def add(
	user_id: typing.Union[None, uuid.UUID],
	group_id: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Adds the group with the requested ``group_id`` to the user with the given
	``user_id``.

	:param user_id: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, this is assumed to be that of the current
		:attr:`flask.g.user`.
	:param group_id: The :attr:`id <heiwa.database.Group.id>` of the group.
		Unlike ``user_id``, this must be set.

	:raises heiwa.exceptions.APIUserGroupAlreadyAdded: Raised when the group
		has already been added to this user before.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	user = get_user_self_or_id(
		user_id,
		flask.g.sa_session
	)

	group = get_group(group_id)

	validate_permission(
		flask.g.user,
		"edit_group",
		user,
		edited_group=group
	)

	if not user.get_in_group(group.id):
		raise exceptions.APIUserGroupAlreadyAdded

	flask.g.sa_session.execute(
		sqlalchemy.insert(database.user_groups).
		values(
			user_id=user.id,
			group_id=group.id
		)
	)

	user.parse_permissions()

	return flask.jsonify(None), http.client.NO_CONTENT


@user_groups_blueprint.route("/users/<uuid:id_>/groups", methods=["GET"])
@user_groups_blueprint.route(
	"/self/groups",
	defaults={"id_": None},
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view_groups", database.User)
def list_(
	id_: typing.Union[None, uuid.UUID]
) -> typing.Tuple[flask.Response, int]:
	"""Gets the assigned group IDs for the user with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, it's assumed to be that of the current
		:attr:`flask.g.user`.

	:returns: The list of group IDs, with the HTTP ``200`` status code.
	"""

	user = get_user_self_or_id(
		id_,
		flask.g.sa_session
	)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.Group.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=(
					database.Group.id.in_(
						sqlalchemy.select(database.user_groups.c.group_id).
						where(database.user_groups.c.user_id == user.id)
					)
				),
				order_by=sqlalchemy.asc(database.Group.level),
				pkeys_only=True
			)
		).scalars().all()
	), http.client.OK


@user_groups_blueprint.route(
	"/users/<uuid:user_id>/groups/<uuid:group_id>",
	methods=["DELETE"]
)
@user_groups_blueprint.route(
	"/self/groups/<uuid:group_id>",
	defaults={"user_id": None},
	methods=["DELETE"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit_group", database.User)
def delete(
	user_id: typing.Union[None, uuid.UUID],
	group_id: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Removes the group with the requested ``group_id`` from the user with the
	given ``user_id``.

	:param user_id: The :attr:`id <heiwa.database.User.id>` of the user. If
		:data:`None`, this is assumed to be that of the current
		:attr:`flask.g.user`.
	:param group_id: The :attr:`id <heiwa.database.Group.id>` of the group.
		Unlike ``user_id``, this must be set.

	:raises heiwa.exceptions.APIUserGroupNotAdded: Raised when the group is not
		assigned to the user in the first place.
	:raises heiwa.exceptions.APIUserGroupIsLastDefault: Raised when the group is
		the last one among those assigned containing ``*`` in the
		:attr:`default_for <heiwa.database.Group.default_for>` attribute.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	user = get_user_self_or_id(
		user_id,
		flask.g.sa_session
	)

	group = get_group(group_id)

	validate_permission(
		flask.g.user,
		"edit_group",
		user,
		edited_group=group
	)

	existing_group_association = flask.g.sa_session.execute(
		sqlalchemy.select(database.user_groups.c.user_id).
		where(
			sqlalchemy.and_(
				database.user_groups.c.user_id == user.id,
				database.user_groups.c.group_id == group.id
			)
		)
	).scalars().one_or_none()

	if existing_group_association is None:
		raise exceptions.APIUserGroupNotAdded

	if "*" in group.default_for:
		found_default_group = False

		for user_group in user.groups:
			if (
				user_group.id != group.id and
				"*" in user_group.default_for
			):
				found_default_group = True

				break

		if not found_default_group:
			raise exceptions.APIUserGroupIsLastDefault

	flask.g.sa_session.delete(existing_group_association)

	user.parse_permissions()

	return flask.jsonify(None), http.client.NO_CONTENT
