r"""API views for :class:`Statistic <heiwa.database.Statistic>`\ s."""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_session_and_user_defaults,
	parse_search,
	requires_permission
)

__all__ = [
	"ATTR_SCHEMAS",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"get_statistic",
	"list_",
	"search_schema",
	"search_schema_filter_max_in_length",
	"search_schema_registry",
	"statistic_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.0.7"

statistic_blueprint = flask.Blueprint(
	"statistic",
	__name__,
	url_prefix="/statistics"
)


def get_statistic(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.Statistic:
	"""Gets the statistic with the given ``id_``.

	:param id_: The :attr:`id_ <heiwa.database.Statistic.id_>` of the statistic
		to find.
	:param session: The session to find the statistic with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the statistic. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIStatisticNotFound: Raised when the statistic
		doesn't exist, or the ``user`` does not have permission to view it.

	:returns: The statistic.
	"""

	session, user = get_session_and_user_defaults(session, user)

	statistic = flask.g.sa_session.execute(
		database.Statistic.get(
			session,
			user,
			conditions=(database.Statistic.id == id_)
		)
	).scalars().one_or_none()

	if statistic is None:
		raise exceptions.APIStatisticNotFound

	return statistic


ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"thread_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"post_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"user_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	}
}

search_schema = get_search_schema(
	(
		"creation_timestamp",
		"thread_count",
		"post_count",
		"user_count"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=statistic_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(statistic_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"thread_count": ATTR_SCHEMAS["thread_count"],
	"post_count": ATTR_SCHEMAS["post_count"],
	"user_count": ATTR_SCHEMAS["user_count"]
}

search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"thread_count": ATTR_SCHEMAS["thread_count"],
			"post_count": ATTR_SCHEMAS["post_count"],
			"user_count": ATTR_SCHEMAS["user_count"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"thread_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["thread_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"post_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["post_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"user_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["user_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	}
})


@statistic_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Statistic)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Gets all statistic that the current :attr:`flask.g.user` has access to
	view. If there is a filter requested, they must also match it.

	:returns: The list of statistics, with the ``200`` HTTP status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Statistic
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.Statistic.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.Statistic),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@statistic_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Statistic)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the statistics with the given ``id_``, as long as the current
	:attr:`flask.g.user` has the permission to view it.

	:param id_: The :attr:`id <heiwa.database.Statistic.id>` of the statistic.

	:returns: The statistic, with a HTTP ``200`` status code.
	"""

	return flask.jsonify(
		get_statistic(id_)
	), http.client.OK


@statistic_blueprint.route("/<uuid:id_>/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Statistic)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on the statistic with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Statistic.id>` of the statistic.

	:raises heiwa.exceptions.APIStatisticNotFound: Raised when the statistic was
		not found.

	:returns: A list of the allowed actions, with a ``200`` HTTP status code.

	.. seealso::
		:attr:`heiwa.database.Statistic.instance_actions`
	"""

	return flask.jsonify(
		get_statistic(id_)
	), http.client.OK


@statistic_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform of any statistic, without any knowledge on which one it will be.

	:returns: The list of allowed actions, with a ``200`` HTTP status code.

	.. seealso::
		:attr:`heiwa.database.Statistic.static_actions`
	"""

	return flask.jsonify(
		database.Statistic.get_allowed_static_actions(flask.g.user)
	), http.client.OK
