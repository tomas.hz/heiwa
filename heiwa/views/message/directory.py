"""API views for :class:`MessageDirectory <heiwa.database.MessageDirectory>`
objects, belonging under the base message set of views.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_session_and_user_defaults,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)

__all__ = [
	"ATTR_SCHEMAS",
	"CREATE_EDIT_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"create",
	"delete",
	"edit",
	"get_message_directory",
	"list_",
	"mass_delete",
	"mass_edit",
	"message_directory_blueprint",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]

message_directory_blueprint = flask.Blueprint(
	"directory",
	__name__,
	url_prefix="/directories"
)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"user_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"name": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.MessageDirectory.name.property.columns[0].type.length
	},
	"order": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"message_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	}
}

CREATE_EDIT_SCHEMA = {
	"name": {
		**ATTR_SCHEMAS["name"],
		"required": True
	},
	"order": {
		**ATTR_SCHEMAS["order"],
		"required": True
	}
}
search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count",
		"order"
	),
	default_order_by="order",
	default_order_asc=False,
	blueprint_name=message_directory_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(message_directory_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"order": ATTR_SCHEMAS["order"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"user_id": ATTR_SCHEMAS["user_id"],
			"name": ATTR_SCHEMAS["name"],
			"order": ATTR_SCHEMAS["order"],
			"message_count": ATTR_SCHEMAS["message_count"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"user_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["user_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"name": {
				"type": "list",
				"schema": ATTR_SCHEMAS["name"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"order": {
				"type": "list",
				"schema": ATTR_SCHEMAS["order"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"message_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["message_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"name": {
				**ATTR_SCHEMAS["name"],
				"check_with": "is_valid_regex"
			}
		}
	}
})


def get_message_directory(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.MessageDirectory:
	"""Gets the message directory with the given ``id_``. If it doesn't exist or
	the ``user`` does not have permission to view it, an exception is raised
	instead.

	:param id_: The :attr:`id <heiwa.database.MessageDirectory.id>` of the
		directory to find.
	:param session: The SQLAlchemy session to execute the selection query with.
		Defaults to :attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who should have
		permission to view the directory. Defaults to :attr:`flask.g.user` if
		:data:`None`.

	:raises heiwa.exceptions.APIMessageDirectoryNotFound: Raised when the
		directory does simply not exist, or the given user does not have
		permission to view it.

	:returns: The directory.
	"""

	session, user = get_session_and_user_defaults(session, user)

	directory = session.execute(
		database.MessageDirectory.get(
			session,
			user,
			conditions=(database.MessageDirectory.id == id_)
		)
	).scalars().one_or_none()

	if directory is None:
		raise exceptions.APIMessageDirectoryNotFound

	return directory


@message_directory_blueprint.route("", methods=["POST"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.MessageDirectory)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a message directory with the given ``name`` and ``order``.

	:returns: The newly created message directory, with the HTTP ``201`` status
		code.
	"""

	directory = database.MessageDirectory.create(
		flask.g.sa_session,
		user_id=flask.g.user.id,
		**flask.g.json
	)

	flask.g.sa_session.commit()

	return flask.jsonify(directory), http.client.CREATED


@message_directory_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.MessageDirectory)
def list_() -> typing.Tuple[flask.Response, int]:
	"""Gets all message directories that the current :attr:`flask.g.user` is
	allowed to view. If there is a filter requested, they must also match it.

	:returns: The list of directories, with the HTTP ``200`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.MessageDirectory
			)
		)

	directories = flask.g.sa_session.execute(
		sqlalchemy.select(database.MessageDirectory).
		where(conditions).
		order_by(get_order_by_expression(database.MessageDirectory)).
		limit(flask.g.json["limit"]).
		offset(flask.g.json["offset"])
	).scalars().all()

	return flask.jsonify(directories), http.client.OK


@message_directory_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"name": ATTR_SCHEMAS["name"],
				"order": ATTR_SCHEMAS["order"]
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.MessageDirectory)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all message directories :attr:`flask.g.user` is allowed to. If
	there is a filter requested, the messages must also match it.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.MessageDirectory
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.MessageDirectory).
		where(
			database.MessageDirectory.id.in_(
				database.MessageDirectory.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=database.MessageDirectory.action_queries["edit"](
						flask.g.user
					),
					conditions=conditions,
					order_by=get_order_by_expression(database.MessageDirectory),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"],
					pkeys_only=True
				)
			)
		).
		values(**flask.g.json["values"]).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@message_directory_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.MessageDirectory)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	"""Deletes all message directories :attr:`flask.g.user` is allowed to. If
	there is a filter requested, the messages must also match it.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.MessageDirectory
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.MessageDirectory).
		where(
			database.MessageDirectory.id.in_(
				database.MessageDirectory.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=database.MessageDirectory.action_queries["delete"](
						flask.g.user
					),
					conditions=conditions,
					order_by=get_order_by_expression(database.MessageDirectory),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"],
					pkeys_only=True
				)
			)
		).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@message_directory_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.MessageDirectory)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the message directory with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.MessageDirectory.id>` of the
		directory.

	:returns: The directory, with the HTTP ``200`` status code.
	"""

	return flask.jsonify(
		get_message_directory(id_)
	), http.client.OK


@message_directory_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.MessageDirectory)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the message directory with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.MessageDirectory.id>` of the
		directory.

	:returns: The updated directory, with the HTTP ``200`` status code.
	"""

	directory = get_message_directory(id_)

	validate_permission(
		flask.g.user,
		"edit",
		directory
	)

	handle_edit(
		directory,
		flask.g.json,
		exceptions.APIMessageDirectoryUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	flask.g.sa_session.commit()

	return flask.jsonify(directory), http.client.OK


@message_directory_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.MessageDirectory)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the message directory with the requested ``id_``.

	:param id_: The :attr:`id <heiwa.database.MessageDirectory.id>` of the
		directory.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	directory = get_message_directory(id_)

	validate_permission(
		flask.g.user,
		"delete",
		directory
	)

	directory.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@message_directory_blueprint.route(
	"/<uuid:id_>/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.MessageDirectory)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on the message directory with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.MessageDirectory.id>` of the
		directory.

	:raises heiwa.exceptions.APIMessageDirectoryNotFound: Raised when the
		directory was not found, or the current user is not allowed to view it.

	:returns: A list of the allowed actions, with a 200 status code.

	.. seealso::
		:attr:`heiwa.database.MessageDirectory.instance_actions`
	"""

	return flask.jsonify(
		get_message_directory(id_)
	), http.client.OK


@message_directory_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on message directories, without any knowledge of which one it will
	be.

	:returns: The list of allowed actions, with a 200 status code.

	.. seealso::
		:attr:`heiwa.database.MessageDirectory.static_actions`
	"""

	return flask.jsonify(
		database.MessageDirectory.get_allowed_static_actions(flask.g.user)
	), http.client.OK
