r"""API views for :class:`Post <heiwa.database.Post>`\ s."""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	constants,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	get_thread,
	handle_edit,
	parse_search,
	requires_permission,
	validate_permission
)
from .utils import get_post
from .vote import post_vote_blueprint

__all__ = [
	"ATTR_SCHEMAS",
	"CREATE_EDIT_SCHEMA",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"create",
	"delete",
	"edit",
	"list_",
	"mass_delete",
	"mass_edit",
	"post_blueprint",
	"post_vote_blueprint",
	"search_schema",
	"search_schema_registry",
	"search_schema_filter_max_in_length",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]
__version__ = "1.0.14"

post_blueprint = flask.Blueprint(
	"post",
	__name__,
	url_prefix="/posts"
)

post_blueprint.register_blueprint(post_vote_blueprint)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"edit_count": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	},
	"thread_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"user_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"subject": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Post.subject.property.columns[0].type.length
	},
	"content": {
		"type": "string",
		"minlength": 1,
		"maxlength": database.Post.content.property.columns[0].type.length
	},
	"vote_value": {
		"type": "integer",
		"min": 0,
		"max": constants.BIG_INTEGER_LIMIT
	}
}

CREATE_EDIT_SCHEMA = {
	"thread_id": {
		**ATTR_SCHEMAS["thread_id"],
		"required": True
	},
	"subject": {
		**ATTR_SCHEMAS["subject"],
		"nullable": True,
		"required": True
	},
	"content": {
		**ATTR_SCHEMAS["content"],
		"required": True
	}
}
search_schema = get_search_schema(
	(
		"creation_timestamp",
		"edit_timestamp",
		"edit_count",
		"vote_value"
	),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=post_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(post_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
	"edit_timestamp": ATTR_SCHEMAS["edit_timestamp"],
	"edit_count": ATTR_SCHEMAS["edit_count"],
	"vote_value": ATTR_SCHEMAS["vote_value"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"edit_timestamp": {
				**ATTR_SCHEMAS["edit_timestamp"],
				"nullable": True
			},
			"edit_count": ATTR_SCHEMAS["edit_count"],
			"thread_id": ATTR_SCHEMAS["thread_id"],
			"user_id": ATTR_SCHEMAS["user_id"],
			"subject": {
				**ATTR_SCHEMAS["subject"],
				"nullable": True
			},
			"content": ATTR_SCHEMAS["content"],
			"vote_value": ATTR_SCHEMAS["vote_value"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_timestamp": {
				"type": "list",
				"schema": {
					**ATTR_SCHEMAS["edit_timestamp"],
					"nullable": True
				},
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"edit_count": {
				"type": "list",
				"schema": ATTR_SCHEMAS["edit_count"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"thread_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["thread_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"user_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["user_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"subject": {
				"type": "list",
				"schema": ATTR_SCHEMAS["subject"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"content": {
				"type": "list",
				"schema": ATTR_SCHEMAS["content"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"vote_value": {
				"type": "list",
				"schema": ATTR_SCHEMAS["vote_value"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	},
	"$regex": {
		"type": "dict",
		"schema": {
			"subject": {
				**ATTR_SCHEMAS["subject"],
				"check_with": "is_valid_regex"
			},
			"content": {
				**ATTR_SCHEMAS["content"],
				"check_with": "is_valid_regex"
			}
		},
		"maxlength": 1
	}
})


@post_blueprint.route("", methods=["POST"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("create", database.Post)
def create() -> typing.Tuple[flask.Response, int]:
	"""Creates a post with the provided ``thread_id``, ``subject`` and
	``content``.

	:raises heiwa.exceptions.APIThreadLocked: Raised when the
		:class:`Thread <heiwa.database.Thread>` referred to by the
		``thread_id`` is locked.

	:returns: The newly created post, with the ``201`` HTTP status code.
	"""

	thread = get_thread(flask.g.json["thread_id"])

	validate_permission(
		flask.g.user,
		"create",
		database.Post,
		thread=thread
	)

	if thread.is_locked:
		raise exceptions.APIThreadLocked

	post = database.Post.create(
		flask.g.sa_session,
		user_id=flask.g.user.id,
		**flask.g.json
	)

	flask.g.sa_session.commit()

	return flask.jsonify(post), http.client.CREATED


@post_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Post)
def list_() -> typing.Tuple[flask.Response, int]:
	r"""Gets all posts that the current :attr:`flask.g.user` is allowed to view.
	If there is a filter requested, the posts must also match it.

	When the respective :class:`Forum <heiwa.database.Forum>` permissions haven'ŧ
	yet been parsed at the time of the initial query, it's done so automatically.

	:returns: The list of posts, with a ``200`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Post
			)
		)

	return flask.jsonify(
		flask.g.sa_session.execute(
			database.Post.get(
				flask.g.sa_session,
				flask.g.user,
				conditions=conditions,
				order_by=get_order_by_expression(database.Post),
				limit=flask.g.json["limit"],
				offset=flask.g.json["offset"]
			)
		).scalars().all()
	), http.client.OK


@post_blueprint.route("", methods=["PUT"])
@validators.validate_json(
	{
		**search_schema,
		"values": {
			"type": "dict",
			"minlength": 1,
			"schema": {
				"thread_id": {
					**ATTR_SCHEMAS["thread_id"]
				},
				"subject": {
					**ATTR_SCHEMAS["subject"],
					"nullable": True
				},
				"content": ATTR_SCHEMAS["content"]
			},
			"required": True
		}
	},
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Post)
def mass_edit() -> typing.Tuple[flask.Response, int]:
	"""Updates all posts that the current :attr:`flask.g.user` is allowed to with
	the given ``values``. If there is a filter requested, they must also match it.

	When the respective :class:`Forum <heiwa.database.Forum>` permissions haven'ŧ
	yet been parsed at the time of the initial query, it's done so automatically.

	:returns: :data:`None`, with the HTTP ``204`` status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	action_conditions = database.Post.action_queries["edit"](flask.g.user)

	if "thread_id" in flask.g.json["values"]:
		action_conditions = sqlalchemy.and_(
			action_conditions,
			database.Post.action_queries["move"](
				flask.g.user,
				future_thread=get_thread(flask.g.json["values"]["thread_id"])
			)
		)

	conditions = (
		sqlalchemy.select(database.Thread.id).
		where(
			sqlalchemy.and_(
				database.Thread.id == database.Post.thread_id,
				database.Thread.is_locked.is_(False)
			)
		).
		exists()
	)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Post
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.update(database.Post).
		where(
			database.Post.id.in_(
				database.Post.get(
					flask.g.sa_session,
					flask.g.user,
					action_conditions=action_conditions,
					conditions=conditions,
					order_by=get_order_by_expression(database.Post),
					limit=flask.g.json["limit"],
					offset=flask.g.json["offset"]
				)
			)
		).
		values(**flask.g.json["values"]).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@post_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Post)
def mass_delete() -> typing.Tuple[flask.Response, int]:
	r"""Deletes all posts that the current :attr:`flask.g.user` is allowed to.
	If there is a filter requested, they must also match it.

	Associated :class:`Notification <heiwa.database.Notification>`\ s are also
	deleted.

	When the respective :class:`Forum <heiwa.database.Forum>` permissions haven'ŧ
	yet been parsed at the time of the initial query, it's done so automatically.

	:returns: :data:`None`, with the HTTP ``204`` status code.

	.. seealso::
		:class:`heiwa.database.ForumParsedPermissions`
	"""

	conditions = True

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Post
			)
		)

	ids = flask.g.sa_session.execute(
		database.Post.get(
			flask.g.sa_session,
			flask.g.user,
			action_conditions=database.Post.action_queries["delete"](flask.g.user),
			conditions=conditions,
			order_by=get_order_by_expression(database.Post),
			limit=flask.g.json["limit"],
			offset=flask.g.json["offset"],
			pkeys_only=True
		)
	).scalars().all()

	if len(ids) != 0:
		flask.g.sa_session.execute(
			sqlalchemy.delete(database.Notification).
			where(
				sqlalchemy.and_(
					database.Notification.type.in_(database.Post.NOTIFICATION_TYPES),
					database.Notification.identifier.in_(ids)
				)
			).
			execution_options(synchronize_session="fetch")
		)

		flask.g.sa_session.execute(
			sqlalchemy.delete(database.Post).
			where(database.Post.id.in_(ids))
		)

	return flask.jsonify(None), http.client.NO_CONTENT


@post_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Post)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the post with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Post.id>` of the post.

	:returns: The post, with a HTTP ``200`` status code.
	"""

	return flask.jsonify(
		get_post(id_)
	), http.client.OK


@post_blueprint.route("/<uuid:id_>", methods=["PUT"])
@validators.validate_json(CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.Post)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the post with the given ``id_`` with the requested ``values``.

	:param id_: The :attr:`id <heiwa.database.Post.id>` of the post.

	:raises heiwa.exceptions.APIThreadLocked: Raised when either the current
		or the new :class:`Thread <heiwa.database.Thread>` is locked.

	:returns: The updated post, with a HTTP ``200`` status code.
	"""

	post = get_post(id_)

	validate_permission(
		flask.g.user,
		"edit",
		post
	)

	if flask.g.sa_session.execute(
		sqlalchemy.select(database.Thread.is_locked).
		where(database.Thread.id == post.thread_id)
	).scalars().one():
		raise exceptions.APIThreadLocked(post.thread_id)

	if post.thread_id != flask.g.json["thread_id"]:
		future_thread = get_thread(flask.g.json["thread_id"])

		validate_permission(
			flask.g.user,
			"move",
			post,
			future_thread=future_thread
		)

		if future_thread.is_locked:
			raise exceptions.APIThreadLocked(future_thread.id)

		post.thread = future_thread

	handle_edit(
		post,
		flask.g.json,
		exceptions.APIPostUnchanged,
		current_utc_time=flask.g.current_utc_time
	)

	flask.g.sa_session.commit()

	return flask.jsonify(post), http.client.OK


@post_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("delete", database.Post)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the post with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Post.id>` of the post.

	:raises heiwa.exceptions.APIThreadLocked: Raised when the respective
		:class:`Thread <heiwa.database.Thread>` is locked.

	:returns: :data:`None`, with the HTTP ``204`` status code.
	"""

	post = get_post(id_)

	validate_permission(
		flask.g.user,
		"delete",
		post
	)

	if flask.g.sa_session.execute(
		sqlalchemy.select(database.Thread.is_locked).
		where(database.Thread.id == post.thread_id)
	).scalars().one():
		raise exceptions.APIThreadLocked

	post.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@post_blueprint.route("/<uuid:id_>/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.Post)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on the post with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Post.id>` of the post.

	:returns: The list of allowed actions, with a HTTP ``200`` status code.

	.. seealso::
		:attr:`heiwa.database.Post.get_allowed_instance_actions`
	"""

	return flask.jsonify(
		get_post(id_)
	), http.client.OK


@post_blueprint.route("/allowed-actions", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that the current :attr:`flask.g.user` is allowed to
	perform on any post, irrespective of which one it is.

	:returns: The list of allowed actions, with a HTTP ``200`` status code.

	.. seealso::
		:attr:`heiwa.database.Post.get_allowed_static_actions`
	"""

	return flask.jsonify(
		database.Post.get_allowed_static_actions(flask.g.user)
	), http.client.OK
