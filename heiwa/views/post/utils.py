"""Utilities specific to the :class:`Post <heiwa.database.Post>` views, but not
only a single module.
"""

import typing
import uuid

import sqlalchemy.orm

from heiwa import database, exceptions

from ..utils import get_session_and_user_defaults

__all__ = ["get_post"]


def get_post(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> database.Post:
	"""Gets the post with the given ``id_``.

	:param id_: The :attr:`id <heiwa.database.Post.id>` of the post to find.
	:param session: The session to find the post with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the post. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIPostNotFound: Raised when the post doesn't exist,
		or the user does not have permission to view it.

	:returns: The post.
	"""

	session, user = get_session_and_user_defaults(session, user)

	post = session.execute(
		database.Post.get(
			session,
			user,
			conditions=(database.Post.id == id_)
		)
	).scalars().one_or_none()

	if post is None:
		raise exceptions.APIPostNotFound

	return post


def validate_post_exists(
	id_: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None
) -> None:
	"""Validates whether or not the :class:`Post <heiwa.database.Post>` with the
	given ``id_`` exists.

	:param id_: The :attr:`id <heiwa.database.Post.id>` of the post to find.
	:param session: The session to find the post with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the post. Defaults to :attr:`flask.g.user` if :data:`None`.

	:raises heiwa.exceptions.APIPostNotFound: Raised when the post doesn't exist,
		or the user does not have permission to view it.
	"""

	session, user = get_session_and_user_defaults(session, user)

	if session.execute(
		database.Post.get(
			session,
			user,
			conditions=(database.Post.id == id_),
			pkeys_only=True
		).
		exists().
		select()
	).scalars().one():
		raise exceptions.APIPostNotFound(id_)
