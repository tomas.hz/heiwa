"""API views for guest users, wishing to access the current service without
registering a full :class:`User <heiwa.database.User>` account. In general,
this will be used automatically when frontend services don't recognize a
registered user from information the browser provides.
"""

import datetime
import http.client
import typing

import flask
import sqlalchemy

from heiwa import database, exceptions, limiter

from ..utils import get_user_jwt, get_identifier_hash

__all__ = [
	"guest_blueprint",
	"token"
]
__version__ = "1.0.3"

guest_blueprint = flask.Blueprint(
	"guest",
	__name__,
	url_prefix="/guest"
)


@guest_blueprint.route("/token", methods=["GET"])
@limiter.rate_limited
def token() -> typing.Tuple[flask.Response, int]:
	r"""Gets a JWT for a temporary guest :class:`User <heiwa.database.User>`
	account. The token expires after the amount of seconds given in the
	``GUEST_SESSION_EXPIRES_AFTER`` config key.

	When this endpoint is used, all expired guest user sessions are handled -
	deleted when they have no public content, anonymized when they do. The
	scheduler should also do this every so often, as long as it's enabled.

	:raises heiwa.exceptions.APIGuestSessionLimitReached: Raised when the current
		user (based on an IP address hash) has requested too many guest sessions
		in a short amount of time. That amount of time is specified in seconds
		within the ``GUEST_SESSION_EXPIRES_AFTER`` config key.

	:returns: The JWT.

	.. note::
		When :exc:`APIGuestSessionLimitReached <heiwa.exceptions.APIGuestSessi\
		onLimitReached>` is raised, the maximum session limit is not provided in
		the details. In theory, this could be used to obtain information about
		how many guests currently exist with a commonly shared IP - for example,
		a Tor exit node or VPN server.

	.. seealso::
		:func:`heiwa.tasks.get_scheduler`

		:attr:`heiwa.database.User.has_content`
	"""

	max_creation_timestamp = (
		flask.g.current_utc_time
		- datetime.timedelta(
			seconds=flask.current_app.config["GUEST_SESSION_EXPIRES_AFTER"]
		)
	)

	# Delete all expired sessions with no content

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.User).
		where(
			sqlalchemy.and_(
				database.User.creation_timestamp <= max_creation_timestamp,
				database.User.registered_by == "guest",
				~database.User.has_content
			)
		).
		execution_options(synchronize_session="fetch")
	)

	# Remove external identifier for guests with content that would have been
	# deleted otherwise

	flask.g.sa_session.execute(
		sqlalchemy.update(database.User).
		where(
			sqlalchemy.and_(
				database.User.creation_timestamp <= max_creation_timestamp,
				database.User.registered_by == "guest"
			)
		).
		values(external_id=None).
		execution_options(synchronize_session="fetch")
	)

	# Commit here just in case there is something wrong with user input,
	# and an exception is raised

	flask.g.sa_session.commit()

	hashed_identifier = get_identifier_hash()

	existing_session_count = flask.g.sa_session.execute(
		sqlalchemy.select(
			sqlalchemy.func.count(
				database.User.id
			)
		).
		where(
			sqlalchemy.and_(
				database.User.creation_timestamp >= max_creation_timestamp,
				database.User.registered_by == "guest",
				database.User.external_id == hashed_identifier
			)
		)
	).scalars().one()

	if (
		existing_session_count
		>= flask.current_app.config["GUEST_MAX_SESSIONS_PER_IP"]
	):
		raise exceptions.APIGuestSessionLimitReached

	user = database.User.create(
		flask.g.sa_session,
		registered_by="guest",
		external_id=hashed_identifier,
		write_kwargs={
			"bypass_first_user_check": True
		}
	)

	return flask.jsonify(
		get_user_jwt(
			user.id,
			expires_after=flask.current_app.config["GUEST_SESSION_EXPIRES_AFTER"]
		)
	), http.client.CREATED
