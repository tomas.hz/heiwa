r"""API views for meta information about the current service. For example, its
name, allowed avatar types for :class:`User <heiwa.database.User>`\ s.
"""

import http.client
import os
import typing

import flask

from heiwa import limiter

__all__ = [
	"meta_blueprint",
	"view_config",
	"view_icon",
	"view_info"
]
__version__ = "1.0.3"

meta_blueprint = flask.Blueprint(
	"meta",
	__name__,
	url_prefix="/meta"
)


@meta_blueprint.route("/config", methods=["GET"])
@limiter.rate_limited
def view_config() -> typing.Tuple[flask.Response, int]:
	"""Gets basic information about this service's config - specifically, all
	keys specified in its ``PUBLIC_CONFIG_KEYS`` value.

	:returns: The list of keys and their associated values, with the ``200`` HTTP
		status code.
	"""

	return flask.jsonify({
		value.lower(): flask.current_app.config[value]
		for value in flask.current_app.config["PUBLIC_CONFIG_KEYS"]
	}), http.client.OK


@meta_blueprint.route("/icon", methods=["GET"])
@limiter.rate_limited
def view_icon() -> typing.Tuple[flask.Response, int]:
	"""Gets this service's icon, based on the data specified in the
	``ICON_LOCATION`` and ``ICON_MEDIA_TYPE`` environment variables.

	:returns: The icon. If there isn't one, a JSON :data:`None` is returned
		instead. The HTTP status code is ``200`` in both cases.
	"""

	path = os.environ.get(
		"ICON_LOCATION",
		os.path.join(os.getcwd(), "icon.png")
	)
	media_type = os.environ.get(
		"ICON_MEDIA_TYPE",
		"image/png"
	)

	if not os.path.exists(path):
		response = flask.jsonify(None)
	else:
		response = flask.send_file(
			path,
			mimetype=media_type,
			as_attachment=True,
			attachment_filename=os.path.basename(path),
			last_modified=os.path.getmtime(path)
		)

	return response, http.client.OK


@meta_blueprint.route("/info", methods=["GET"])
@limiter.rate_limited
def view_info() -> typing.Tuple[flask.Response, int]:
	"""Gets all keys in this service's config starting with ``META_`` - meta
	information.

	:returns: The keys with their prefix removed, as well as their associated
		values, with a ``200`` HTTP status code.
	"""

	return flask.jsonify({
		key[5:].lower(): value
		for key, value in flask.current_app.config.items()
		if key.startswith("META_")
	}), http.client.OK
