r"""API views for :class:`Notification <heiwa.database.Notification>`\ s."""

import http.client
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	get_order_by_expression,
	get_search_schema,
	get_search_schema_filter_in_max_list_length,
	get_search_schema_filter_registry,
	parse_search
)

__all__ = [
	"ATTR_SCHEMAS",
	"LESS_OR_GREATER_FILTER_SCHEMA",
	"confirm_read",
	"delete",
	"get_notification",
	"list_",
	"mass_confirm_read",
	"mass_delete",
	"notification_blueprint",
	"search_schema",
	"search_schema_filter_max_in_length",
	"search_schema_registry",
	"view"
]
__version__ = "1.0.6"

notification_blueprint = flask.Blueprint(
	"notification",
	__name__,
	url_prefix="/notifications"
)

ATTR_SCHEMAS = {
	"id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"creation_timestamp": {
		"type": "datetime",
		"coerce": "convert_to_datetime"
	},
	"user_id": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	},
	"is_read": {
		"type": "boolean"
	},
	"type": {
		"type": "string",
		"minlength": 1,
		"maxlength": 128
	},
	"identifier": {
		"type": "uuid",
		"coerce": "convert_to_uuid"
	}
}

search_schema = get_search_schema(
	("creation_timestamp",),
	default_order_by="creation_timestamp",
	default_order_asc=False,
	blueprint_name=notification_blueprint.name
)

search_schema_filter_max_in_length = (
	get_search_schema_filter_in_max_list_length(notification_blueprint.name)
)

LESS_OR_GREATER_FILTER_SCHEMA = {
	"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"]
}
search_schema_registry = get_search_schema_filter_registry({
	"$equals": {
		"type": "dict",
		"schema": {
			"id": ATTR_SCHEMAS["id"],
			"creation_timestamp": ATTR_SCHEMAS["creation_timestamp"],
			"user_id": ATTR_SCHEMAS["user_id"],
			"is_read": ATTR_SCHEMAS["is_read"],
			"type": ATTR_SCHEMAS["type"],
			"identifier": ATTR_SCHEMAS["identifier"]
		},
		"maxlength": 1
	},
	"$less_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$less_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$greater_than_or_equal_to": {
		"type": "dict",
		"schema": LESS_OR_GREATER_FILTER_SCHEMA,
		"maxlength": 1
	},
	"$in": {
		"type": "dict",
		"schema": {
			"id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"creation_timestamp": {
				"type": "list",
				"schema": ATTR_SCHEMAS["creation_timestamp"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"user_id": {
				"type": "list",
				"schema": ATTR_SCHEMAS["user_id"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"type": {
				"type": "list",
				"schema": ATTR_SCHEMAS["type"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			},
			"identifier": {
				"type": "list",
				"schema": ATTR_SCHEMAS["identifier"],
				"minlength": 2,
				"maxlength": search_schema_filter_max_in_length
			}
		},
		"maxlength": 1
	}
})


def get_notification(
	notification_id: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user_id: typing.Union[None, uuid.UUID] = None
) -> database.Notification:
	"""Gets the :class:`Notification <heiwa.database.Notification>` with the
	given ``notification_id``
	(:attr:`Notification.id <heiwa.database.Notification.id>`). If the message
	doesn't exist, or the :class:`User <heiwa.database.User>` with the given
	``user_id`` is not its receiver, an exception is raised.

	:param notification_id: The :attr:`id <heiwa.database.Notification.id>` of
		the notification to find.
	:param session: The SQLAlchemy session to execute the selection query with.
		Defaults to :attr:`flask.g.sa_session` if :data:`None`.
	:param user_id: The :attr:`id <heiwa.database.User.id>` of the user who
		should be the notification's receiver. Defaults to
		:attr:`flask.g.user.id` if :data:`None`.

	:raises heiwa.exceptions.APINotificationNotFound: Raised when the notification
		simply doesn't exist, or the given user is not its receiver.

	:returns: The message.
	"""

	if session is None:
		session = flask.g.sa_session

	if user_id is None:
		user_id = flask.g.user.id

	notification = session.execute(
		sqlalchemy.select(database.Notification).
		where(
			sqlalchemy.and_(
				database.Notification.id == notification_id,
				database.Notification.user_id == user_id
			)
		)
	).scalars().one_or_none()

	if notification is None:
		raise exceptions.APINotificationNotFound

	return notification


@notification_blueprint.route("", methods=["QUERY"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
def list_() -> typing.Tuple[flask.Response, int]:
	"""Lists all notifications belonging to :attr:`flask.g.user` that match the
	requested filter, if there is one.
	"""

	conditions = (database.Notification.user_id == flask.g.user.id)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Notification
			)
		)

	notifications = flask.g.sa_session.execute(
		sqlalchemy.select(database.Notification).
		where(conditions).
		order_by(get_order_by_expression(database.Notification)).
		limit(flask.g.json["limit"]).
		offset(flask.g.json["offset"])
	).scalars().all()

	return flask.jsonify(notifications), http.client.OK


@notification_blueprint.route("/confirm-read", methods=["POST"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
def mass_confirm_read() -> typing.Tuple[flask.Response, int]:
	"""Confirms that all notifications belonging to :attr:`flask.g.user` that match
	the requested filter (if there is one) have been read.
	"""

	conditions = sqlalchemy.and_(
		database.Notification.user_id == flask.g.user.id,
		database.Notification.is_read.is_(False)
	)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Notification
			)
		)

	notifications = flask.g.sa_session.execute(
		sqlalchemy.select(database.Notification).
		where(conditions).
		order_by(get_order_by_expression(database.Notification)).
		limit(flask.g.json["limit"]).
		offset(flask.g.json["offset"])
	).scalars().all()

	for notification in notifications:
		notification.is_read = True

	return flask.jsonify(None), http.client.NO_CONTENT


@notification_blueprint.route("", methods=["DELETE"])
@validators.validate_json(
	search_schema,
	schema_registry=search_schema_registry
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
def mass_delete() -> typing.Tuple[flask.Response, int]:
	"""Deletes all notifications belonging to :attr:`flask.g.user` that match the
	requested filter, if there is one.
	"""

	conditions = (database.Notification.user_id == flask.g.user.id)

	if "filter" in flask.g.json:
		conditions = sqlalchemy.and_(
			conditions,
			parse_search(
				flask.g.json["filter"],
				database.Notification
			)
		)

	flask.g.sa_session.execute(
		sqlalchemy.delete(database.Notification).
		where(
			database.Notification.id.in_(
				sqlalchemy.select(database.Notification.id).
				where(conditions).
				order_by(get_order_by_expression(database.Notification)).
				limit(flask.g.json["limit"]).
				offset(flask.g.json["offset"])
			)
		).
		execution_options(synchronize_session="fetch")
	)

	return flask.jsonify(None), http.client.NO_CONTENT


@notification_blueprint.route("/<uuid:id_>", methods=["GET"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the notification with the requested ``id_``."""

	return flask.jsonify(
		get_notification(id_)
	), http.client.OK


@notification_blueprint.route("/<uuid:id_>/confirm-read", methods=["POST"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def confirm_read(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Confirms that the notification with the requested ``id_`` has been read."""

	notification = get_notification(id_)

	notification.is_read = True

	return flask.jsonify(None), http.client.NO_CONTENT


@notification_blueprint.route("/<uuid:id_>", methods=["DELETE"])
@authentication.authenticate_via_jwt
@limiter.rate_limited
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the notification with the requested ``id_``."""

	notification = get_notification(id_)

	notification.delete()

	return flask.jsonify(None), http.client.NO_CONTENT
