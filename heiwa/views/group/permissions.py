r"""API views for :class:`GroupPermissions <heiwa.database.GroupPermissions>`,
belonging under the default set of views for
:class:`Group <heiwa.database.Group>`\ s.
"""

import http.client
import typing
import uuid

import flask
import sqlalchemy.orm

from heiwa import (
	authentication,
	database,
	exceptions,
	limiter,
	validators
)

from ..utils import (
	BASE_PERMISSION_CREATE_EDIT_SCHEMA,
	get_group,
	get_session_and_user_defaults,
	validate_group_exists,
	requires_permission,
	validate_permission
)
from .utils import get_if_last_default_group

__all__ = [
	"delete",
	"edit",
	"get_permissions",
	"group_permissions_blueprint",
	"view",
	"view_allowed_actions_instance",
	"view_allowed_actions_static"
]

group_permissions_blueprint = flask.Blueprint(
	"permissions",
	__name__
)


def get_permissions(
	group_id: uuid.UUID,
	session: typing.Union[None, sqlalchemy.orm.Session] = None,
	user: typing.Union[None, database.User] = None,
	validate_existence: bool = True
) -> database.GroupPermissions:
	"""Gets the group permissions with the given ``group_id``.

	:param group_id: The
		:attr:`group_id <heiwa.database.GroupPermissions.group_id>` of the
		permissions to find.
	:param session: The session to find the permissions with. Defaults to
		:attr:`flask.g.sa_session` if :data:`None`.
	:param user: The :class:`User <heiwa.database.User>` who must have permission
		to view the permissions. Defaults to :attr:`flask.g.user` if :data:`None`.
	:param validate_existence: Whether or not to check that the permissions exist.
		:data:`True` by default.

	:raises heiwa.exceptions.APIGroupPermissionsNotFound: Raised when the
		permissions don't exist, or the ``user`` does not have permission to
		view them. Depends on ``validate_existence`` being :data:`True`.

	:returns: The permissions.
	"""

	session, user = get_session_and_user_defaults(session, user)

	permissions = session.execute(
		database.GroupPermissions.get(
			session,
			user,
			conditions=(database.GroupPermissions.group_id == group_id)
		)
	).scalars().one_or_none()

	if validate_existence and permissions is None:
		raise exceptions.APIGroupPermissionsNotFound

	return permissions


@group_permissions_blueprint.route(
	"/<uuid:id_>/permissions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.GroupPermissions)
def view(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Gets the group with the requested ``id_``'s permissions."""

	validate_group_exists(id_)

	return flask.jsonify(
		get_permissions(
			id_,
			validate_existence=False
		)
	), http.client.OK


@group_permissions_blueprint.route(
	"/<uuid:id_>/permissions",
	methods=["PUT"]
)
@validators.validate_json(BASE_PERMISSION_CREATE_EDIT_SCHEMA)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.GroupPermissions)
def edit(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Updates the group with the requested ``id_``'s permissions.
	Automatically creates them if they don't exist.
	"""

	group = get_group(id_)

	permissions = get_permissions(
		id_,
		validate_existence=False
	)

	if permissions is None:
		validate_permission(
			flask.g.user,
			"edit",
			database.GroupPermissions,
			group=group
		)

		if "*" in group.default_for:
			for key, value in flask.g.json.items():
				if value is None:
					raise exceptions.APIGroupPermissionsForLastDefault

		database.GroupPermissions.create(
			flask.g.sa_session,
			group_id=group.id,
			**flask.g.json
		)

		status = http.client.CREATED
	else:
		validate_permission(
			flask.g.user,
			"edit",
			permissions
		)

		# Don't go through the keys twice needlessly
		check_if_none = "*" in group.default_for

		unchanged = True

		for key, value in flask.g.json.items():
			if check_if_none and value is None:
				raise exceptions.APIGroupPermissionsForLastDefault

			if getattr(permissions, key) is not value:
				unchanged = False
				setattr(permissions, key, value)

		if unchanged:
			raise exceptions.APIGroupPermissionsUnchanged

		permissions.edited(current_utc_time=flask.g.current_utc_time)

		status = http.client.OK

	flask.g.sa_session.commit()

	return flask.jsonify(permissions), status


@group_permissions_blueprint.route(
	"/<uuid:id_>/permissions",
	methods=["DELETE"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("edit", database.GroupPermissions)
def delete(id_: uuid.UUID) -> typing.Tuple[flask.Response, int]:
	"""Deletes the group with the requested ``id_``'s permissions."""

	group = get_group(id_)

	permissions = get_permissions(id_)

	validate_permission(
		flask.g.user,
		"edit",
		permissions
	)

	if "*" in group.default_for and get_if_last_default_group(group.id):
		raise exceptions.APIGroupPermissionsForLastDefault

	permissions.delete()

	return flask.jsonify(None), http.client.NO_CONTENT


@group_permissions_blueprint.route(
	"/<uuid:id_>/permissions/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
@requires_permission("view", database.GroupPermissions)
def view_allowed_actions_instance(
	id_: uuid.UUID
) -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on the
	group with the given ``id_``'s permissions

	:param id_: The ID of the group.

	:raises heiwa.exceptions.APIGroupNotFound: Raised when the ``id_`` does not
		correspond to any group, or the current user does not have the permission
		to view it.
	:raises heiwa.exceptions.APIGroupPermissionsNotFound: Raised when the group
		does not have any set permissions.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.GroupPermissions`
	"""

	validate_group_exists(id_)

	return flask.jsonify(
		get_permissions(id_)
	), http.client.OK


@group_permissions_blueprint.route(
	"/permissions/allowed-actions",
	methods=["GET"]
)
@authentication.authenticate_via_jwt
@limiter.rate_limited
def view_allowed_actions_static() -> typing.Tuple[flask.Response, int]:
	"""Gets all actions that :attr:`flask.g.user` is allowed to perform on all
	group permissions, regardless of which one they belong to.

	:returns: The list of allowed actions, with the ``200`` HTTP status code.

	.. seealso::
		:class:`heiwa.database.GroupPermissions`
	"""

	return flask.jsonify(
		database.GroupPermissions.get_allowed_static_actions(flask.g.user)
	), http.client.OK
