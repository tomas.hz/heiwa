"""Utilities specific to :class:`Group <heiwa.database.Group>` views, but not
only a single module.
"""

import operator
import uuid

import flask
import sqlalchemy

from heiwa import database

__all__ = ["get_if_last_default_group"]


def get_if_last_default_group(group_id: uuid.UUID) -> bool:
	"""Checks whether or not the :class:`Group <heiwa.database.Group>` with the
	given ``group_id`` is the last whose
	:attr:`default_for <heiwa.database.Group.default_for>` column contains ``*`` -
	meaning it's default for all users.

	:param group_id: The group's :attr:`id <heiwa.database.Group.id>`.

	:returns: The result of the check.
	"""

	return flask.g.sa_session.execute(
		sqlalchemy.select(database.Group.id).
		where(
			sqlalchemy.and_(
				database.Group.id != group_id,
				database.Group.default_for.any(
					"*",
					operator=operator.eq
				)
			)
		).
		exists().
		select()
	).scalars().one()
