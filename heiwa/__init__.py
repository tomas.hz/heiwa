"""Heiwa, a forum API."""

from __future__ import annotations

import datetime
import json
import logging
import os
import typing

import apscheduler.schedulers.background
import apscheduler.schedulers.blocking
import flask
import flask_babel
import rpyc
import sqlalchemy
import sqlalchemy.orm
import werkzeug.exceptions

__all__ = [
	"ConfiguredLockFlask",
	"create_app",
	"get_config",
	"run_scheduler"
]
__version__ = "0.24.5"

logging.basicConfig(
	format="[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
	level=getattr(
		logging,
		os.environ.get(
			"LOGGING_LEVEL",
			"DEBUG"
		)
	)
)


class ConfiguredLockFlask(flask.Flask):
	"""A Flask subclass which supports detecting and setting whether or not
	everything has been set up for production use.
	"""

	def __init__(self: ConfiguredLockFlask, *args, **kwargs) -> None:
		"""Sets whether or not this app has been configured and when the last
		update to its config was. This depends on whether or not the file located
		where the ``CONFIGURED_LOCK_LOCATION`` environment variable (or
		``$current_working_directory/configured.lock``, if it's unset) describes
		exists, and its content if it does. If it doesn't exist, the app has not
		been configured. Otherwise, it has, and the last time that happened was
		when the file's content - an
		`ISO-8601 <https://wikiless.org/wiki/ISO_8601>`_ datetime - describes.
		"""

		self.lock_file_location = os.environ.get(
			"CONFIGURED_LOCK_LOCATION",
			os.path.join(os.getcwd(), "configured.lock")
		)

		self._configured = os.path.isfile(self.lock_file_location)

		if self._configured:
			with open(
				self.lock_file_location,
				"r",
				encoding="utf-8"
			) as lock_file:
				self._last_configured = datetime.datetime.fromisoformat(
					lock_file.read()
				)
		else:
			self._last_configured = None

		super().__init__(*args, **kwargs)

	@property
	def configured(self: ConfiguredLockFlask) -> bool:
		"""Gets the value in the
		:attr:`_configured <.ConfiguredLockFlask._configured>` instance variable.
		This describes whther or not the current app has been properly set up for
		production use.

		:returns: The variable.
		"""

		return self._configured

	@configured.setter
	def configured(
		self: ConfiguredLockFlask,
		configured_: bool
	) -> None:
		"""Sets whether or not this app has been configured, and creates / removes
		the lock file.

		:param configured_: Whether or not the app has been configured.
		"""

		if configured_:
			with open(
				self.lock_file_location,
				"w",
				newline="",
				encoding="utf-8"
			) as lock_file:
				if self._last_configured is None:
					self._last_configured = datetime.datetime.now(tz=datetime.timezone.utc)

				lock_file.write(self._last_configured.isoformat())
		elif os.path.exists(self.lock_file_location):
			os.remove(self.lock_file_location)

		self._configured = configured_

	@property
	def last_configured(self: ConfiguredLockFlask) -> typing.Union[
		None,
		datetime.datetime
	]:
		"""Gets the value in the
		:attr:`_last_configured <.ConfiguredLockFlask._last_configured>` instance
		variable. This describes the last time the current app was re/configured.

		:returns: The variable.
		"""

		return self._last_configured

	@last_configured.setter
	def last_configured(
		self: ConfiguredLockFlask,
		last_configured_: datetime.datetime
	) -> None:
		"""Sets the last time this app has been re/configured. If it has not been
		configured before, it's automatically set to :data:`True`.

		:param last_configured_: The last time the app has been re/configured.
		"""

		if not self._configured:
			self._configured = True

		with open(self.lock_file_location, "w", encoding="utf-8") as lock_file:
			lock_file.write(last_configured_.isoformat())

		self._last_configured = datetime.datetime


def get_config(
		skip_context_check: bool = False,
		config_location_envvar: str = "CONFIG_LOCATION",
		fallback_name: str = "config.json"
	) -> typing.Dict[str, typing.Any]:
	"""Gets the current JSON config for this service.

	If this function is run in a Flask context and ``skip_context_check`` is
	:data:`True`, :attr:`flask.current_app.config` is returned. Otherwise, an
	attempt is made to use the ``config_location_envvar`` environment variable
	for the config file. If it exists, the location it points to will be used.
	Otherwise, the current working directory's ``fallback_name`` is used.

	:param config_location_envvar: The environment variable to use in order to
		find the custom config file. ``CONFIG_LOCATION`` by default.
	:param fallback_name: The filename to use when the environment variable is
		not set. Defaults to ``config.json``.

	.. warning::
		It's assumed that if the environment variable is set, the location is
		valid and the file exists. If it is not set, ``config.json`` must exist.

	:returns: The config, in a Python dictionary.
	"""

	if not skip_context_check and flask.has_app_context():
		return flask.current_app.config

	with open(
		os.environ.get(
			config_location_envvar,
			os.path.join(os.getcwd(), fallback_name)
		),
		"r",
		encoding="utf-8"
	) as config_file:
		return json.load(config_file)


def create_app() -> ConfiguredLockFlask:
	r"""Creates a :class:`.ConfiguredLockFlask` app with settings already applied.

	If the ``FLASK_ENV`` environment variable is ``development``, debug mode is
	enabled. If unset, it's assumed to be ``production``.

	Loads the config from the file located where the ``CONFIG_LOCATION``
	environment variable describes, or ``$current_working_directory/config.json``
	if it's not set.

	Creates a SQLAlchemy engine from the ``DATABASE_URI`` config value and a
	scoped session to use it at ``app.sa_session_class``.

	Sets the app's ``app.json_encoder`` to
	:class:`JSONLDEncoder <json_ld.JSONLDEncoder>`, a custom encoder which
	supports ISO-8601 datetimes, SQLAlchemy models and such.

	Instantiates a :class:`Limiter <limiter.Limiter>` to rate limit requests.
	Limits set in the ``RATELIMIT_DEFAULT`` and ``RATELIMIT_SPECIFIC`` config keys
	are considered.

	Creates a ``before_request`` function, which sets the global current user
	identifier (:attr:`flask.g.identifier`) to the remote IP address and creates
	a basic SQLAlchemy session derived from
	:attr:`flask.current_app.sa_session_class` at :attr:`flask.g.sa_session`.
	Also creates a :attr:`flask.g.current_utc_time` attribute with the current
	datetime in the UTC timezone.

	Creates a ``teardown_request`` function, which attempts to commit
	:attr:`flask.g.sa_session` and rolls it back if any exception is raised during
	the process. The exception is then re-raised. Otherwise, the session is
	removed and the original response is returned.

	Registers both error handlers in :mod:`.error_handlers`.

	Registers all blueprints in :mod:`.views`.

	Creates a ``reflect`` Flask CLI command, which reflects all models to the
	app's database and creates default groups. If there are any
	:class:`Group <.database.Group>`\ s specified in the ``GROUP_DEFAULTS``
	config key, they are also created, as long as they don't exist already.

	:returns: The finished app.
	"""

	app = ConfiguredLockFlask(__name__)

	with app.app_context():
		app.logger.info("Setting up app")

		if os.environ.get("FLASK_ENV", "production") == "development":
			app.logger.info("Enabling development mode")

			app.debug = True

		app.logger.debug("Loading config file")

		app.config.update(get_config(skip_context_check=True))

		app.logger.debug("Creating engine")

		sa_engine = sqlalchemy.create_engine(app.config["DATABASE_URI"])

		app.sa_session_class = sqlalchemy.orm.scoped_session(
			sqlalchemy.orm.sessionmaker(
				bind=sa_engine
			)
		)

		from .json_ld import JSONLDEncoder
		from .limiter import init_limiter

		app.json_encoder = JSONLDEncoder
		init_limiter(app, lambda: flask.g.identifier)

		app.babel = flask_babel.Babel(app)

		from .exceptions import APIException

		@app.before_request
		def before_request() -> None:
			"""Sets the global current user identifier (:attr:`flask.g.identifier`)
			to the remote IP address and creates a basic SQLAlchemy session
			derived from :attr:`flask.current_app.sa_session_class` at
			:attr:`flask.g.sa_session`. Also creates a :attr:`flask.g.current_utc_time`
			attribute with the current datetime in the UTC timezone.
			"""

			flask.g.identifier = flask.request.remote_addr
			flask.g.current_utc_time = datetime.datetime.now(tz=datetime.timezone.utc)
			flask.g.sa_session = flask.current_app.sa_session_class()

		from .error_handlers import (
			handle_api_exception,
			handle_http_exception
		)

		@app.teardown_request
		def teardown_request(
			exception: typing.Union[None, Exception]
		) -> None:
			"""Attempts to commit :attr:`flask.g.sa_session` and rolls it back if
			any exception is raised during the process. The exception is then
			logged.

			:param exception: The exception that occurred in the prior request,
				if there was any.
			"""

			if "sa_session" in flask.g:
				# "Clean" the session
				try:
					flask.g.sa_session.commit()
				except Exception as commit_exception:
					flask.g.sa_session.rollback()

					flask.current_app.logger.error(
						"Exception %s raised during the request teardown session commit: %s",
						commit_exception.__class__.__name__,
						(
							commit_exception
							if hasattr(commit_exception, "__str__")
							else "no details"
						)
					)

				flask.current_app.sa_session_class.remove()

		for handler in (
			(
				APIException,
				handle_api_exception
			),
			(
				werkzeug.exceptions.HTTPException,
				handle_http_exception
			)
		):
			app.logger.debug("Registering error handler: %s", handler)

			app.register_error_handler(*handler)

		from .views import (
			category_blueprint,
			file_blueprint,
			forum_blueprint,
			group_blueprint,
			guest_blueprint,
			local_account_blueprint,
			message_blueprint,
			meta_blueprint,
			notification_blueprint,
			oidc_blueprint,
			post_blueprint,
			statistic_blueprint,
			thread_blueprint,
			user_blueprint
		)

		for blueprint in (
			category_blueprint,
			file_blueprint,
			forum_blueprint,
			group_blueprint,
			guest_blueprint,
			local_account_blueprint,
			message_blueprint,
			meta_blueprint,
			notification_blueprint,
			oidc_blueprint,
			post_blueprint,
			statistic_blueprint,
			thread_blueprint,
			user_blueprint
		):
			app.logger.debug("Registering blueprint: %s", blueprint)

			app.register_blueprint(blueprint)

		@app.cli.command("reflect")
		def reflect() -> None:
			r"""Reflects database models and creates default groups.

			If there are any :class:`Group <.database.Group>`\ s specified in the
			``GROUP_DEFAULTS`` config key, they are also created, as long as they
			don't exist already.
			"""

			from .database import Base, Group, GroupPermissions

			with app.sa_session_class() as sa_session:
				Base.metadata.create_all(bind=sa_engine)

				for group_name, group_attrs in app.config["GROUP_DEFAULTS"].items():
					if sa_session.execute(
						sqlalchemy.select(Group.id).
						where(Group.name == group_name).
						exists().
						select()
					).scalars().one():
						continue

					group = Group.create(
						sa_session,
						default_for=group_attrs["default_for"],
						level=group_attrs["level"],
						name=group_name,
						description=group_attrs["description"]
					)

					# Need to get the group ID here
					sa_session.flush()

					if "permissions" in group_attrs:
						GroupPermissions.create(
							sa_session,
							group_id=group.id,
							**group_attrs["permissions"]
						)

					sa_session.commit()

	app.cli.add_command(reflect)

	app.logger.info("App setup finished")

	return app


def run_scheduler() -> None:
	r"""Creates a :class:`SchedulerService <.tasks.SchedulerService>` and run a
	RPyC server in order for other programs to be able to interface with it. If
	the ``RPYC_PORT`` config value is ``0``, only the scheduler will run and no
	external communication is possible.

	.. note::
		Adapted from an `example <https://github.com/agronholm/apscheduler/com\
		mit/19fa2fa27a7e168fbcec883817408a21a8b7339e>`_ by
		`Alex Grönholm <https://github.com/agronholm>`_.
	"""

	from .tasks import SchedulerService, get_scheduler

	logger = logging.getLogger(__name__)

	logger.debug("Loading config file")

	config = get_config(skip_context_check=True)

	logger.debug("Loading scheduler")

	if config["RPYC_PORT"] == 0:
		scheduler = get_scheduler(
			config,
			scheduler_class=apscheduler.schedulers.blocking.BlockingScheduler
		)

		try:
			logger.info("Running scheduler without RPyC server")
			scheduler.start()
		except (KeyboardInterrupt, SystemExit):
			pass
		finally:
			scheduler.shutdown()

		return

	scheduler = get_scheduler(
		config,
		scheduler_class=apscheduler.schedulers.background.BackgroundScheduler
	)

	logger.debug("Creating RPyC server")

	server = rpyc.ThreadedServer(
		SchedulerService,
		port=config["RPYC_PORT"],
		protocol_config={
			"allow_public_attrs": True
		}
	)

	try:
		logger.info("Running scheduler with RPyC server")

		scheduler.start()
		server.start()
	except (KeyboardInterrupt, SystemExit):
		pass
	finally:
		scheduler.shutdown()
