"""Rate limiting."""

from __future__ import annotations

import datetime
import functools
import logging
import typing

import flask
import limits
import limits.storage
import limits.strategies

from heiwa import exceptions

__all__ = [
	"Limiter",
	"init_limiter",
	"rate_limited"
]
__version__ = "2.3.3"


class Limiter:
	"""Rate limiter, primarily compatible with Flask applications."""

	def __init__(
		self: Limiter,
		default_limits: typing.Union[
			None,
			typing.Iterable[str]
		] = None,
		endpoint_limits: typing.Union[
			None,
			typing.Dict[
				str,
				typing.Iterable[str]
			]
		] = None,
		storage_class: limits.storage.Storage = limits.storage.MemoryStorage,
		strategy_class: limits.strategies.RateLimiter = (
			limits.strategies.MovingWindowRateLimiter
		),
		key_func: typing.Callable[
			[],
			typing.Hashable
		] = lambda: flask.request.remote_addr,
		endpoint_func: typing.Callable[
			[],
			str
		] = lambda: flask.request.endpoint
	) -> None:
		"""Sets this instance's attributes to the given values.

		:param default_limits: An iterable of default rate limits. For example,
			``4/1second`` and ``2000/1hour``. If this value is :data:`None` and
			this method is running within a Flask app context, it's set to the
			app's ``RATELIMIT_DEFAULT`` config key, if there is one.
		:param endpoint_limits: The same as ``default_limits``, but for specific
			endpoints, where the key corresponds to the endpoint's name.
		:param storage_class: The storage backend this rate limiter uses to store
			requests.
		:param strategy_class: The strategy for this rate limiter.
		:param key_func: A function that returns the unique identifier for the
			current user with each request. By default, this will be remote IP
			addresses.
		:param endpoint_func: A function which returns the current endpoint with
			each request. By default, this will be :attr:`flask.request.endpoint`.
		"""

		has_app_context = flask.has_app_context()

		self.logger = (
			logging.getLogger(__name__)
			if not has_app_context
			else flask.current_app.logger
		)

		self.logger.debug("Setting up default rate limits")

		if default_limits is None:
			self.default_limits = [
				limits.parse(limit)
				for limit in (
					default_limits
					if default_limits is not None
					else (
						flask.current_app.config["RATELIMIT_DEFAULT"]
						if has_app_context
						else default_limits
					)
				)
			]

			self.logger.debug("Default rate limits: %s", self.default_limits)
		else:
			self.logger.warning("No default rate limits have been set during init")

			self.default_limits = []

		self.logger.debug("Setting up endpoint-specific rate limits")

		if endpoint_limits is None:
			self.endpoint_limits = {
				endpoint: [
					limits.parse(limit)
					for limit in limit_set
				]
				for endpoint, limit_set in (
					endpoint_limits.items()
					if endpoint_limits is not None
					else (
						flask.current_app.config["RATELIMIT_SPECIFIC"].items()
						if has_app_context
						else endpoint_limits
					)
				)
			}
		else:
			self.endpoint_limits = {}

		self.logger.debug("Specific rate limits: %s", self.endpoint_limits)

		self.storage = storage_class()
		self.strategy = strategy_class(self.storage)

		self.key_func = key_func
		self.endpoint_func = endpoint_func

	def check(
		self: Limiter,
		identifier: typing.Union[
			None,
			typing.Hashable
		] = None,
		endpoint: typing.Union[
			None,
			str
		] = None,
		add_expires: bool = False
	) -> typing.Union[
		bool,
		typing.Tuple[
			bool,
			typing.Union[
				None,
				datetime.datetime
			]
		]
	]:
		"""Gets whether or not the current user has exceeded the rate limit for
		the given endpoint.

		:param identifier: The current user's identifier. If :data:`None`, the
			output of the :attr:`key_func <.Limiter.key_func>` is used. This
			is generally expected behavior.
		:param endpoint: The current endpoint.
		:param add_expires: Whether or not the function should return the time the
			user will be allowed to access the endpoint again. If :data:`True`,
			this will always be returned, even if they have not exceeded the rate
			limit.

		:returns: If ``add_expires`` is :data:`False`, whether or not the user
			has exceeded the rate limit, stored in a boolean value. If it's
			:data:`True`, that value and also the time they can access it again.

		.. note::
			If there are no rate limits specified for the current endpoint, it's
			assumed that it has none. For example, when a rate limit specific to
			the endpoint is an empty iterable, the default limit is overriden
			and the endpoint has no rate limit at all.
		"""

		identifier = self.key_func() if identifier is None else identifier
		endpoint = self.endpoint_func() if endpoint is None else endpoint

		limit_set = (
			self.endpoint_limits[endpoint]
			if endpoint in self.endpoint_limits
			else self.default_limits
		)

		passed_limit = True
		soonest_expiration_limit = None

		for limit in limit_set:
			if (
				soonest_expiration_limit is None or
				soonest_expiration_limit.get_expiry() > limit.get_expiry()
			):
				soonest_expiration_limit = limit

			if not self.strategy.hit(
				limit,
				identifier,
				endpoint
			):
				soonest_expiration_limit = limit
				passed_limit = False

		if add_expires:
			return (
				passed_limit,
				datetime.datetime.fromtimestamp(
					self.strategy.get_window_stats(
						soonest_expiration_limit,
						identifier,
						endpoint
					)[0],
					tz=datetime.timezone.utc
				)
			)

		return passed_limit


def init_limiter(
	app: flask.Flask,
	key_func: typing.Callable[[], typing.Hashable]
) -> None:
	"""Instantiates a :class:`.Limiter` at ``app.limiter``. The ``key_func``
	argument is taken from this function's.

	:param app: The :class:`Flask <flask.Flask>` app to add the rate limiter to.
	:param key_func: A callable object that returns the current user's identifier.
		This should always be unique for every user.
	"""

	app.limiter = Limiter(key_func=key_func)


def rate_limited(
	func: typing.Callable
) -> typing.Callable:
	"""Rate limits the decorated function, using the current Flask app's
	``limiter`` attribute.

	:raises heiwa.exceptions.APIRateLimitExceeded: Raised when the user with the
		current identifier has exceeded the rate limit for the current endpoint.

	.. seealso::
		:class:`.Limiter`
	"""

	@functools.wraps(func)
	def decorator(*args, **kwargs) -> typing.Any:
		is_rate_limit_exceeded, rate_limit_expires = (
			flask.current_app.limiter.check(add_expires=True)
		)

		if not is_rate_limit_exceeded:
			raise exceptions.APIRateLimitExceeded(details=rate_limit_expires)

		return func(*args, **kwargs)
	return decorator
