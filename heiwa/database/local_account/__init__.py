"""Models for local :class:`.User` accounts, not using any external source of
registration.
"""

from __future__ import annotations

import typing

import argon2
import sqlalchemy

from .. import Base, utils
from .totp import LocalAccountTOTP

__all__ = ["LocalAccount", "LocalAccountTOTP"]
__version__ = "1.2.1"


class LocalAccount(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Local user account model."""

	__tablename__ = "local_accounts"

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who a local account
	belongs to. Their :attr:`registered_by <.User.registered_by>` column should
	be ``local_account``.
	"""

	email_is_verified = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False,
		default=False
	)
	"""Whether or not a local user account's email has been verified. Generally,
	a :class:`.User` will only be created once it has.
	"""

	email = sqlalchemy.Column(
		sqlalchemy.String(254),
		nullable=False,
		index=True
	)
	r"""The email address a user used to register their local account.

	Since this column will almost always be used to find accounts, it is
	indexed to improve performance.

	.. seealso::
		`The reasoning behind the 254 character limit <https://blog.moonmail.i\
		o/what-is-the-maximum-length-of-a-valid-email-address-f712c6c4bc93>`_.
	"""

	_hashed_password = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)
	"""The password used to access a local account, hashed using Argon2id.

	An example of this value is ``$argon2id$v=19$m=65536,t=3,p=4$MskBVyhHxb2vO\
	pMUZ/CcQQ$8EG5E+uFtYajUyuPuX/Hlk8RY8UsdXTOU2W6YvzB5UM``. To allow for larger
	numbers in the preceding parameters, the maximum number of characters is 128.
	"""

	has_totp = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False,
		default=False
	)
	"""Whether or not a local account also uses TOTP for authentication. If yes,
	there should be a :class:`.LocalAccountTOTP` corresponding to it.
	"""

	totp = sqlalchemy.orm.relationship(
		LocalAccountTOTP,
		uselist=False,
		backref=sqlalchemy.orm.backref(
			"account",
			uselist=False
		),
		passive_deletes="all",
		lazy=True
	)
	"""A :class:`.LocalAccountTOTP` instance corresponding to an account. If this
	is anything but :data:`None`, the :attr:`has_totp <.LocalAccount.has_totp>`
	attribute must be :data:`True`.
	"""

	def write(
		self: LocalAccount,
		session: sqlalchemy.orm.Session,
		password: str,
		hasher_kwargs: typing.Dict[str, typing.Any] = {}
	) -> None:
		"""Sets this new instance's
		:attr:`_hashed_password <.LocalAccount._hashed_password>` to the hashed
		version of the given ``password``.

		:param session: The SQLAlchemy session to add this instance to.
		:param password: The password to hash.
		:param hasher_kwargs: Additional keyword arguments to give to the hasher.
		"""

		self.set_hashed_password(password, **hasher_kwargs)

		return super().write(session)

	def get_hashed_password_match(
		self: LocalAccount,
		password: bytes,
		**hasher_kwargs
	) -> bool:
		"""Gets whether or not the given ``password``'s Argon2id hash matches that
		of this instance's :attr:`hashed_password <.LocalAccount.hashed_password>`.

		If the password needs to be rehashed, it's done automatically. This can
		happen if configuration values have been changed and the current password
		is from before the change happened.

		:param password: The plaintext password, in bytes.
		:param hasher_kwargs: Keyword arguments to give the hasher.

		.. warning::
			``hasher_kwargs`` should not alter the salt or hash length. There is
			a limit of 128 characters for the column.

		:returns: The result of the check.
		"""

		hasher = argon2.PasswordHasher(**hasher_kwargs)

		if hasher.check_needs_rehash(self._hashed_password):
			self._hashed_password = hasher.hash(password)

		try:
			# Always returns :data:`True`

			return hasher.verify(
				self._hashed_password,
				password
			)
		except argon2.exceptions.VerifyMismatchError:
			# Don't catch exceptions that don't stem from the password being
			# incorrect! Other ones can be raised too, but not due to user
			# input.

			return False

	def set_hashed_password(
		self: LocalAccount,
		password: bytes,
		**hasher_kwargs
	) -> None:
		"""Sets this account's
		:attr:`hashed_password <.LocalAccount.hashed_password>` to the Argon2id
		hash of ``password``.

		:param password: A byte version of the plaintext password to hash.
		:param hasher_kwargs: Keyword arguments to give the hasher.

		.. warning::
			``hasher_kwargs`` should not alter the salt or hash length. There is
			a limit of 128 characters for the column.
		"""

		hasher = argon2.PasswordHasher(**hasher_kwargs)

		self._hashed_password = hasher.hash(password)

	def enable_totp(
		self: LocalAccount,
		backup_code_count: int,
		secret: typing.Union[None, str] = None
	) -> LocalAccountTOTP:
		"""Enables TOTP for this account.

		This sets the :attr:`has_totp <.LocalAccount.has_totp>` attribute to
		:data:`True` and creates an instance of :class:`.LocalAccountTOTP`.

		:param secret: The secret to use for the TOTP. If :data:`None`, this
			is generated automatically.

		:returns: The new :class:`.LocalAccountTOTP` instance.
		"""

		self.has_totp = True

		return LocalAccountTOTP.create(
			sqlalchemy.orm.object_session(self),
			local_account_id=self.id,
			secret=secret,
			write_kwargs={
				"backup_code_count": backup_code_count
			}
		)

	def disable_totp(self: LocalAccount) -> None:
		"""Disables TOTP for this account.

		This deletes the associated :class:`.LocalAccountTOTP` instance. It's
		assumed it exists and unexpected behavior will occur if it does not.
		"""

		self.has_totp = False

		self.totp.delete()
