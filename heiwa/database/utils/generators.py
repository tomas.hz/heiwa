"""Generators for SQLAlchemy columns and models."""

import uuid

import sqlalchemy
import sqlalchemy.engine

__all__ = ["get_uuid"]


def get_uuid(context: sqlalchemy.engine.ExecutionContext) -> uuid.UUID:
	"""Keeps generating an UUID4, until it is not present in the context's
	current column.

	:param context: The current execution context.

	:returns: The generated UUID.
	"""

	while True:
		uuid4 = uuid.uuid4()

		if not context.connection.execute(
			sqlalchemy.select(context.current_column).
			where(context.current_column == uuid4).
			exists().
			select()
		).scalars().one():
			break

	return uuid4
