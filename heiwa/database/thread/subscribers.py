"""Tables related to :class:`.Thread` subscriptions."""

import sqlalchemy

from .. import Base, utils

__all__ = ["thread_subscribers"]


thread_subscribers = sqlalchemy.Table(
	"thread_subscribers",
	Base.metadata,
	sqlalchemy.Column(
		"thread_id",
		utils.UUID,
		sqlalchemy.ForeignKey(
			"threads.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	),
	sqlalchemy.Column(
		"user_id",
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
)
r"""A table defining which :class:`.Thread`\ s which :class:`.User`\ s have
subscribed to.
"""
