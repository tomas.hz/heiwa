"""Models and tables for groups."""

from __future__ import annotations

import flask
import sqlalchemy
import sqlalchemy.orm

from .. import Base, utils
from .permissions import GroupPermissions

__all__ = [
	"Group",
	"GroupPermissions"
]
__version__ = "1.1.6"


class Group(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Group model."""

	__tablename__ = "groups"

	default_for = sqlalchemy.Column(
		sqlalchemy.ARRAY(sqlalchemy.String(128)),
		nullable=False
	)
	r"""The registration services for :class:`.User`\ s who will automatically
	be assigned a group. For example, when a user registers using OIDC and
	``oidc`` is one of the values in this column, the group will be assigned
	to them.

	If ``*`` is one of the values, all users will be assigned the group.

	.. seealso::
		:attr:`.User.registered_by`

		:meth:`.User.write`
	"""

	level = sqlalchemy.Column(
		sqlalchemy.Integer,
		nullable=False
	)
	"""How important a group's permissions will be when permissions are calculated
	for users or forums. The higher this value is, the more important it will be.

	.. seealso::
		:meth:`.User.parse_permissions`

		:meth:`.Forum.parse_permissions`
	"""

	name = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)
	"""A group's name."""

	description = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=True
	)
	"""A group's description."""

	permissions = sqlalchemy.orm.relationship(
		GroupPermissions,
		uselist=False,
		backref=sqlalchemy.orm.backref(
			"group",
			uselist=False
		),
		passive_deletes="all",
		lazy=True
	)
	"""A group's permissions. Not required to be set, except for the last group
	whose :attr:`default_for <.Group.default_for>` column contains `*` - meaning
	it's default for all users.

	.. seealso::
		:attr:`.Group.static_actions`

		:attr:`.Group.action_queries`
	"""

	def get_id_url(self: Group) -> str:
		"""Gets the JSON-LD ``@id`` route to this group.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"group.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: Group) -> None:
		r"""Adds the :attr:`instance_actions <.Group.instance_actions>` attribute,
		containing possible actions :class:`.User`\ s are allowed to perform on
		specific groups:

			``delete``:
				Whether or not a user can delete this group.

			``edit``:
				Whether or not a user can edit this group, excluding its permissions.

			``edit_permissions``:
				Whether or not a user can edit this group's permissions.

			``view``:
				Whether or not a user can view this group.

			``view_permissions``:
				Whether or not a user can view this group's permissions.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"edit_permissions": self.get_instance_action_edit_permissions,
			"view": self.get_instance_action_view,
			"view_permissions": self.get_instance_action_view
		}

	def get_instance_action_delete(self: Group, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this group.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["group_delete"] and
			self.level < user.highest_group.level
		)

	def get_instance_action_edit(self: Group, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this group.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["group_edit"] and
			self.level < user.highest_group.level
		)

	def get_instance_action_edit_permissions(self: Group, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this group's
		permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.

		.. seealso::
			:class:`.GroupPermissions`
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["group_edit_permissions"] and
			self.level < user.highest_group.level
		)

	def get_instance_action_view(self: Group, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this group.
		Always :data:`True` by default.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return True

	@staticmethod
	def get_static_action_create(user) -> bool:
		"""Checks whether or not ``user`` is allowed to create new groups.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["group_create"]

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete groups.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["group_delete"]

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit groups.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["group_edit"]

	@staticmethod
	def get_static_action_edit_permissions(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit groups' permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.

		.. seealso::
			:class:`.GroupPermissions`
		"""

		return user.parsed_permissions["group_edit"]

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view groups.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return True

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which groups ``user``
		is allowed to delete.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Group.action_queries["view"](user),
			user.parsed_permissions["group_delete"],
			Group.level < user.highest_group.level
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which groups ``user``
		is allowed to edit.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Group.action_queries["view"](user),
			user.parsed_permissions["group_edit"],
			Group.level < user.highest_group.level
		)

	@staticmethod
	def get_action_query_edit_permissions(
		user
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which groups ``user``
		is allowed to edit the permissions of.

		:param user: The user, a :class:`.User`.

		:returns: The query.

		.. seealso::
			:class:`.GroupPermissions`
		"""

		return sqlalchemy.and_(
			Group.action_queries["view"](user),
			user.parsed_permissions["group_edit_permissions"],
			Group.level < user.highest_group.level
		)

	@staticmethod
	def get_action_query_view(user) -> bool:
		"""Generates a selectable condition representing which groups ``user``
		is allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		return True

	static_actions = {
		"create": get_static_action_create,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"edit_permissions": get_static_action_edit_permissions,
		"view": get_static_action_view,
		"view_permissions": get_static_action_view
	}
	r"""Actions :class:`User`\ s are allowed to perform on all groups, without
	any indication of which group it is.

	``create``:
		Whether or not a user can create groups.

	``delete``:
		Whether or not a user can delete groups.

	``edit``:
		Whether or not a user can edit groups, excluding their permissions.

	``edit_permissions``:
		Whether or not a user can edit a groups' permissions.

	``view``:
		Whether or not a user can view groups.

	``view_permissions``:
		Whether or not a user can view groups' permissions.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"edit_permissions": get_action_query_edit_permissions,
		"view": get_action_query_view,
		"view_permissions": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.Group.instance_actions>`.
	"""
