"""Message directory models."""

from __future__ import annotations

import flask
import sqlalchemy

from .. import Base, utils

__all__ = ["MessageDirectory"]


class MessageDirectory(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Message directory model.

	When a directory is deleted, its messages will still exist. However, their
	:attr:`directory_id <.Message.directory_id>` will be set to :data:`None` /
	``NULL`` on the database engine's level.

	warning::
		No \"real\" directory should exist for basic sent / received messages,
		their directory ID should remain :data:`None`.
	"""

	__tablename__ = "message_directories"

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		)
	)
	"""The :class:`.User` who owns this directory. Only they should be allowed
	to view it by default.
	"""

	name = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)
	"""A message directory's name."""

	order = sqlalchemy.Column(
		sqlalchemy.Integer,
		default=0,
		nullable=False
	)
	"""The order a message directory will be displayed in by default. The higher
	it is, the higher the message directory will be. It should also display this
	way on frontend clients, in a similar fashion to email folders.
	"""

	message_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	"""The amount of messages in a directory."""

	messages = sqlalchemy.orm.relationship(
		"Message",
		uselist=False,
		passive_deletes="all",
		primaryjoin=(
			"or_("
			"MessageDirectory.id == Message.receiver_directory_id,"
			"MessageDirectory.id == Message.sender_directory_id"
			")"
		),
		lazy=True
	)

	def get_id_url(self: MessageDirectory) -> str:
		"""Gets the JSON-LD ``@id`` route to this directory.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"message.directory.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: MessageDirectory) -> None:
		r"""Adds the :attr:`instance_actions <.MessageDirectory.instance_actions>`
		attribute, containing possible actions :class:`.User`\ s are allowed to
		perform on specific directories:

			``delete``:
				Whether or not a user can delete this directory.

			``edit``:
				Whether or not a user can edit this directory.

			``view``:
				Whether or not a user can view this directory.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete_edit_view,
			"edit": self.get_instance_action_delete_edit_view,
			"view": self.get_instance_action_delete_edit_view
		}

	def get_instance_action_delete_edit_view(
		self: MessageDirectory,
		user
	) -> bool:
		"""Checks whether or not ``user`` is allowed to delete, edit or view this
		directory -- all possible actions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Message

		return (
			Message.static_actions["view"](user) and
			self.user_id == user.id
		)

	@staticmethod
	def get_static_action_delete_edit_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to create, delete or edit
		directories -- all the possible actions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Message

		return Message.static_actions["view"](user)

	@staticmethod
	def get_action_query_delete_edit_view(
		user
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which directories
		``user`` is allowed to delete, edit or view -- all the possible actions.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Message

		return sqlalchemy.and_(
			Message.action_queries["view"](user),
			MessageDirectory.user_id == user.id
		)

	static_actions = {
		"create": get_static_action_delete_edit_view,
		"delete": get_static_action_delete_edit_view,
		"edit": get_static_action_delete_edit_view,
		"view": get_static_action_delete_edit_view
	}
	r"""Actions :class:`User`\ s are allowed to perform on all directories,
	without any indication of which thread it is.

	``create``:
		Whether or not a user can create directories.

	``delete``:
		Whether or not a user can delete directories.

	``edit``:
		Whether or not a user can edit directories.

	``view``:
		Whether or not a user can view directories.
	"""

	action_queries = {
		"delete": get_action_query_delete_edit_view,
		"edit": get_action_query_delete_edit_view,
		"view": get_action_query_delete_edit_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.MessageDirectory.instance_actions>`.
	"""
