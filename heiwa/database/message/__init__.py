"""Message models."""

from __future__ import annotations

import typing

import flask
import sqlalchemy

from heiwa import constants

from .. import Base, utils
from .directory import MessageDirectory

__all__ = ["Message", "MessageDirectory"]
__version__ = "1.2.0"


class Message(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Message model."""

	__tablename__ = "messages"

	sender_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		nullable=False,
		index=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who sent a message."""

	receiver_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		nullable=False,
		index=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who received a message.

	.. note::
		Since allowing for multiple receivers of a single message would mean
		encrypting the message separately for each user and complicate things
		a lot, it has not been implemented. However, sending the same message
		to multiple people at once is still possible.
	"""

	sender_directory_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"message_directories.id",
			ondelete="SET NULL",
			onupdate="CASCADE"
		),
		nullable=False,
		index=True
	)
	"""The :attr:`id <.MessageDirectory.id>` of the directory this message is
	in, where the directory belongs to the sender. If unset, it's a general
	sent/received one and should be displayed as such.
	"""

	receiver_directory_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"message_directories.id",
			ondelete="SET NULL",
			onupdate="CASCADE"
		),
		nullable=False,
		index=True
	)
	"""The :attr:`id <.MessageDirectory.id>` of the directory this message is
	in, where the directory belongs to the receiver. If unset, it's a general
	sent/received one and should be displayed as such.
	"""

	is_read = sqlalchemy.Column(
		sqlalchemy.Boolean,
		default=False,
		nullable=False
	)
	"""Whether or not a message has been read."""

	encrypted_session_key = sqlalchemy.Column(
		sqlalchemy.LargeBinary,
		nullable=False
	)
	"""The AES key used to encrypt a message, encrypted using the receiver's
	:attr:`public_key <.User.public_key>`.
	"""

	iv = sqlalchemy.Column(
		sqlalchemy.LargeBinary,
		nullable=False
	)
	"""The AES IV used during the message's encryption."""

	tag = sqlalchemy.Column(
		sqlalchemy.String(64),
		nullable=True
	)
	"""A string of characters defined from the decrypted version of a message,
	used to check whether or not the encrypted content has been modified. This
	will usually be some form of hash, like SHA256 or SCrypt for extra (possibly
	unnecessary) security. The IV should also be accounted for.
	"""

	encrypted_content = sqlalchemy.Column(
		sqlalchemy.LargeBinary,
		nullable=False
	)
	"""A message's content, encrypted using AES with its session key. By default,
	the maximum size of this value will be 262160 bytes - up to 65536 4-byte
	unicode characters, or about 262144 1-byte characters - those generally
	compatible with ASCII.
	"""

	sender_directory = sqlalchemy.orm.relationship(
		MessageDirectory,
		uselist=False,
		passive_deletes="all",
		primaryjoin=lambda: Message.sender_directory_id == MessageDirectory.id,
		overlaps="messages",
		lazy=True
	)
	"""The :class:`.MessageDirectory` this message is in, for its sender. Can be
	:data:`None`.
	"""

	receiver_directory = sqlalchemy.orm.relationship(
		MessageDirectory,
		uselist=False,
		passive_deletes="all",
		primaryjoin=lambda: Message.receiver_directory_id == MessageDirectory.id,
		overlaps="messages",
		lazy=True
	)
	"""The :class:`.MessageDirectory` this message is in, for its receiver. Can
	be :data:`None`.
	"""

	def get_id_url(self: Message) -> str:
		"""Gets the JSON-LD ``@id`` route to this message.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"message.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: Message) -> None:
		r"""Adds the :attr:`instance_actions <.Message.instance_actions>` attribute,
		containing possible actions :class:`.User`\ s are allowed to perform on
		specific messages:

			``delete``:
				Whether or not a user can delete this message.

			``edit``:
				Whether or not a user can edit this message.

			``view``:
				Whether or not a user can view this message.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"view": self.get_instance_action_view
		}

	def get_instance_action_delete(self: Message, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this message.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["message_use"]
		)

	def get_instance_action_edit(
		self: Message,
		user,
		attrs: typing.Union[None, typing.Collection[str]] = None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this message.

		:param user: The user, a :class:`.User`.
		:param attrs: The attributes of this message the user is editing.
			:data:`None` by default, meaning they are not considered.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			user.parsed_permissions["message_use"] and (
				attrs is None or (
					self.sender_id == user.id
					if "is_read" not in attrs
					else (
						self.receiver_id == user.id
						if len(attrs) == 1
						else False
					)
				)
			)
		)

	def get_instance_action_view(self: Message, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this message.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return user.id in (self.sender_id, self.receiver_id)

	@staticmethod
	def get_static_action_create_delete_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to create, delete or edit
		messages.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			Message.static_actions["view"](user) and
			user.parsed_permissions["message_use"]
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view messages.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return True

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which messages ``user``
		is allowed to delete.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Message.action_queries["view"](user),
			user.parsed_permissions["message_use"]
		)

	@staticmethod
	def get_action_query_edit(
		user,
		attrs: typing.Union[None, typing.Collection[str]] = None
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which messages ``user``
		is allowed to edit.

		:param user: The user, a :class:`.User`.
		:param attrs: The attributes of this message the user is editing.
			:data:`None` by default, meaning they are not considered.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Message.action_queries["view"](user),
			user.parsed_permissions["message_use"],
			(
				True
				if attrs is None
				else (
					Message.sender_id == user.id
					if "is_read" not in attrs
					else (
						Message.receiver_id == user.id
						if len(attrs) == 1
						else False
					)
				)
			)
		)

	@staticmethod
	def get_action_query_view(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which messages ``user``
		is allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		return sqlalchemy.or_(
			Message.sender_id == user.id,
			Message.receiver_id == user.id
		)

	static_actions = {
		"create": get_static_action_create_delete_edit,
		"delete": get_static_action_create_delete_edit,
		"edit": get_static_action_create_delete_edit,
		"view": get_static_action_view
	}
	r"""Actions :class:`User`\ s are allowed to perform on all messages, without
	any indication of which thread it is.

	``create``:
		Whether or not a user can create messages.

	``delete``:
		Whether or not a user can delete messages.

	``edit``:
		Whether or not a user can edit messages.

	``view``:
		Whether or not a user can view messages.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.Message.instance_actions>`.
	"""


sqlalchemy.event.listen(
	Message.__table__,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION messages_create_delete_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT') THEN
					IF (NEW.receiver_directory_id IS NOT NULL) THEN
						UPDATE message_directories
						SET message_count = message_count + 1
						WHERE
							id = NEW.receiver_directory_id AND
							message_count < {constants.BIG_INTEGER_LIMIT};
					END IF;

					IF (NEW.sender_directory_id IS NOT NULL) THEN
						UPDATE message_directories
						SET message_count = message_count + 1
						WHERE
							id = NEW.sender_directory_id AND
							message_count < {constants.BIG_INTEGER_LIMIT};
					END IF;

					UPDATE users
					SET message_received_count = message_received_count + 1
					WHERE
						id = NEW.receiver_id AND
						message_received_count < {constants.BIG_INTEGER_LIMIT};

					UPDATE users
					SET message_sent_count = message_sent_count + 1
					WHERE
						id = NEW.sender_id AND
						message_sent_count < {constants.BIG_INTEGER_LIMIT};

					IF (NEW.is_read IS FALSE) THEN
						UPDATE users
						SET message_received_unread_count = message_received_unread_count + 1
						WHERE
							id = NEW.receiver_id AND
							message_received_unread_count < {constants.BIG_INTEGER_LIMIT};
					END IF;
				ELSIF (TG_OP = 'DELETE') THEN
					IF (OLD.receiver_directory_id IS NOT NULL) THEN
						UPDATE message_directories
						SET message_count = message_count - 1
						WHERE
							id = OLD.receiver_directory_id AND
							message_count < {constants.BIG_INTEGER_LIMIT};
					END IF;

					IF (OLD.sender_directory_id IS NOT NULL) THEN
						UPDATE message_directories
						SET message_count = message_count - 1
						WHERE
							id = OLD.sender_directory_id AND
							message_count < {constants.BIG_INTEGER_LIMIT};
					END IF;

					UPDATE users
					SET message_received_count = message_received_count - 1
					WHERE id = OLD.receiver_id;

					UPDATE users
					SET message_sent_count = message_sent_count - 1
					WHERE id = OLD.sender_id;

					IF (OLD.is_read IS TRUE) THEN
						UPDATE users
						SET message_received_unread_count = message_received_unread_count - 1
						WHERE id = OLD.receiver_id;
					END IF;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE OR REPLACE FUNCTION messages_edit_update_counters()
		RETURNS TRIGGER
		AS $trigger$
			BEGIN
				IF NEW.is_read != OLD.is_read THEN
					IF (NEW.is_read IS TRUE) THEN
						UPDATE users
						SET message_received_unread_count = message_received_unread_count - 1
						WHERE id = NEW.receiver_id;
					ELSE
						UPDATE users
						SET message_received_unread_count = message_received_unread_count + 1
						WHERE
							id = NEW.receiver_id AND
							message_received_unread_count < {constants.BIG_INTEGER_LIMIT};
					END IF;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_counters
		AFTER INSERT OR DELETE
		ON messages
		FOR EACH ROW EXECUTE FUNCTION messages_create_delete_update_counters();

		CREATE TRIGGER edit_update_counters
		AFTER UPDATE OF is_read
		ON messages
		FOR EACH ROW EXECUTE FUNCTION messages_edit_update_counters();
		"""
	).execute_if(dialect="postgresql")
)
