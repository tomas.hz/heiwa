r"""Models related to :class:`.User`\ s' reputation."""

from __future__ import annotations

import flask
import sqlalchemy
import sqlalchemy.ext.hybrid
import sqlalchemy.orm

from .. import Base, utils

__all__ = ["UserReputation"]


class UserReputation(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	""":class:`.User` reputation model. The reputation can be either positive
	(+1 in :attr:`reputation_value <.User.reputation_value>`), neutral (0) or
	negative (-1).

	Users can give other users reputation points multiple times. However, there
	should be a set cooldown. The recommended time for this is 14 days.
	"""

	__tablename__ = "user_reputation"

	giver_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who a reputation point
	was given by.

	Once given, this value should never be changed.
	"""

	receiver_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who a reputation point
	was received by.

	Once given, this value should never be changed.
	"""

	is_positive = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	"""Whether or not a reputation point is positive. If :data:`None` (``NULL``),
	it's neutral and will not affect its receiver's final reputation value.
	"""

	message = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=False
	)
	"""A reputation point's message. Unlike it being positive or negative, a
	message is required.
	"""

	def get_id_url(self: UserReputation) -> str:
		"""Gets the JSON-LD ``@id`` route to this reputation point.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"user.reputation.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: UserReputation) -> None:
		r"""Adds the :attr:`instance_actions <.UserReputation.instance_actions>`
		attribute, containing possible actions users are allowed to perform
		on reputation points:

			``delete``:
				Whether or not a user can delete this reputation point.

			``edit``:
				Whether or not a user can edit this reputation point.

			``view``:
				Whether or not a user can view this reputation point.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"view": self.get_instance_action_view
		}

	def get_instance_action_delete(self: UserReputation, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this reputation point.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"] and
			self.user.instance_actions["delete_reputation"](user)
		)

	def get_instance_action_edit(self: UserReputation, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this reputation point.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"] and
			self.user.instance_actions["edit_reputation"](user)
		)

	def get_instance_action_view(self: UserReputation, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this reputation point.

		:param user: The user.

		:returns: The result of the check.
		"""

		return self.user.instance_actions["view_reputation"](user)

	@staticmethod
	def get_static_action_create(
		user,
		receiving_user=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create reputation points.

		:param user: The user.
		:param receiving_user: The receiving user. :data:`None` by default,
			meaning they are not considered.

		:returns: The result of the check.

		.. note::
			To allow for universal keyword argument usage, ``user`` has not been
			changed to ``performing_user``.
		"""

		from . import User

		return (
			UserReputation.static_actions["view"](user) and
			User.static_actions["create_reputation"](user) and (
				receiving_user is None or
				receiving_user.instance_actions["create_reputation"](user)
			)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete reputation points.

		:param user: The user.

		:returns: The result of the check.
		"""

		from . import User

		return (
			UserReputation.static_actions["view"](user) and
			User.static_actions["delete_reputation"](user)
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit reputation points.

		:param user: The user.

		:returns: The result of the check.
		"""

		from . import User

		return (
			UserReputation.static_actions["view"](user) and
			User.static_actions["edit_reputation"](user)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view reputation points.

		:param user: The user.

		:returns: The result of the check.
		"""

		from . import User

		return User.static_actions["view_reputation"](user)

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which reputation points
		``user`` is allowed to delete.

		:param user: The user.

		:returns: The query.
		"""

		from . import User

		return sqlalchemy.and_(
			UserReputation.action_queries["view"](user),
			User.action_queries["delete_reputation"](user)
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which reputation points
		``user`` is allowed to edit.

		:param user: The user.

		:returns: The query.
		"""

		from . import User

		return sqlalchemy.and_(
			UserReputation.action_queries["view"](user),
			User.action_queries["edit_reputation"](user)
		)

	@staticmethod
	def get_action_query_view(user) -> bool:
		"""Generates a selectable condition representing which reputation points
		``user`` is allowed to view.

		:param user: The user.

		:returns: The query.
		"""

		from . import User

		return User.action_queries["view_reputation"](user)

	static_actions = {
		"create": get_static_action_create,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"view": get_static_action_view
	}
	r"""Actions a given user is allowed to perform on any reputation point,
	without any indication of which one it is.

	``create``:
		Whether or not a user can create reputation points.

	``delete``:
		Whether or not a user can delete reputation points.

	``edit``:
		Whether or not a user can edit reputation points.

	``view``:
		Whether or not a user can view reputation points.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.UserReputation.instance_actions>`.
	"""
