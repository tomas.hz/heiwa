r"""Models related to :class:`.User`\ s' permissions."""

from __future__ import annotations

import flask
import sqlalchemy

from .. import Base, utils

__all__ = ["UserPermissions"]


class UserPermissions(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	utils.BasePermissionMixin,
	Base
):
	""":class:`.User` permission model. This can be used to set permissions for
	one specific user, instead of only a :class:`.Group`.

	Only one instance of this model can exist for a given user at a time.
	"""

	__tablename__ = "user_permissions"

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` these
	permissions belong to.
	"""

	def get_id_url(self: UserPermissions) -> str:
		"""Gets the JSON-LD ``@id`` route to these permissions.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"user.permissions.view",
			id_=self.user_id,
			_external=True
		)

	def permission_control_init(self: UserPermissions) -> None:
		r"""Adds the :attr:`instance_actions <.UserPermissions.instance_actions>`
		attribute, containing possible actions :class:`.User`\ s are allowed to
		perform on sets of permissions:

			``delete``:
				Whether or not a user can delete this set of permissions.

			``edit``:
				Whether or not a user can edit this set of permissions.

			``view``:
				Whether or not a user can view this set of permissions.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"view": self.get_instance_action_view
		}

	def get_instance_action_delete(self: UserPermissions, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this set of
		permissions.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.user.instance_actions["edit_permissions"](user)
		)

	def get_instance_action_edit(self: UserPermissions, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this set of
		permissions.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.user.instance_actions["edit_permissions"](user)
		)

	def get_instance_action_view(self: UserPermissions, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this set of
		permissions.

		:param user: The user.

		:returns: The result of the check.
		"""

		return self.user.instance_actions["view_permissions"](user)

	@staticmethod
	def get_static_action_create(
		user,
		receiving_user=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create sets of permissions.

		:param user: The user.
		:param receiving_user: The receiving user. :data:`None` by default,
			meaning they are not considered.

		:returns: The result of the check.

		.. note::
			To allow for universal keyword argument usage, ``user`` has not been
			changed to ``performing_user``.
		"""

		from . import User

		return (
			UserPermissions.static_actions["view"] and
			User.static_actions["edit_permissions"](user) and (
				receiving_user is None or
				receiving_user.instance_actions["edit_permissions"](user)
			)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete sets of permissions.

		:param user: The user.

		:returns: The result of the check.
		"""

		from . import User

		return (
			UserPermissions.static_actions["view"] and
			User.static_actions["edit_permissions"](user)
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit sets of permissions.

		:param user: The user.

		:returns: The result of the check.
		"""

		from . import User

		return (
			UserPermissions.static_actions["view"] and
			User.static_actions["edit_permissions"](user)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view sets of permissions.

		:param user: The user.

		:returns: The result of the check.
		"""

		from . import User

		return sqlalchemy.and_(
			UserPermissions.action_queries["view"](user),
			User.static_actions["view_permissions"](user)
		)

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which sets of permissions
		``user`` is allowed to delete.

		:param user: The user.

		:returns: The query.
		"""

		from . import User

		return sqlalchemy.and_(
			UserPermissions.action_queries["view"](user),
			User.action_queries["edit_permissions"](user)
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which sets of permissions
		``user`` is allowed to edit.

		:param user: The user.

		:returns: The query.
		"""

		from . import User

		return sqlalchemy.and_(
			UserPermissions.action_queries["view"](user),
			User.action_queries["edit_permissions"](user)
		)

	@staticmethod
	def get_action_query_view(user) -> bool:
		"""Generates a selectable condition representing which sets of permissions
		``user`` is allowed to view.

		:param user: The user.

		:returns: The query.
		"""

		from . import User

		return User.action_queries["view_permissions"](user)

	static_actions = {
		"create": get_static_action_create,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"view": get_static_action_view
	}
	r"""Actions a given user is allowed to perform on any set of permissions,
	without any indication of which one it is.

	``create``:
		Whether or not a user can create a set of permissions.

	``delete``:
		Whether or not a user can delete a set of permissions.

	``edit``:
		Whether or not a user can edit a set of permissions.

	``view``:
		Whether or not a user can view a set of permissions.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.UserPermissions.instance_actions>`.
	"""
