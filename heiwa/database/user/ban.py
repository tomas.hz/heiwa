r"""Models related to :class:`.User`\ s' bans."""

from __future__ import annotations

import datetime
import typing

import flask
import sqlalchemy
import sqlalchemy.ext.hybrid
import sqlalchemy.orm

from .. import Base, utils

__all__ = ["UserBan"]


class UserBan(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	""":class:`.User` ban model. Even when a ban expires, it should not be
	deleted and instead preserved for moderators and the concerned users to be
	able to refer to in the future.

	.. seealso::
		:attr:`.User.is_banned`

		:meth:`.User.create_ban()`

		:meth:`.User.delete_ban()`

		:meth:`.User.get_ban()`
	"""

	__tablename__ = "user_bans"

	giver_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="SET NULL",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who a ban was issued by.

	.. note::
		To preserve bans from users who have been deleted, this value will be set to
		``NULL`` when they delete their account, unlike all other foreign keys
		relating to users.
	"""

	receiver_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=False
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` who a ban was issued to.

	Once issued, this value should never be changed.
	"""

	expiration_timestamp = sqlalchemy.Column(
		sqlalchemy.DateTime(timezone=True),
		nullable=False
	)
	"""The time a ban expires.

	.. note::
		When a ban expires, it's not taken into account immediately, but when the
		:class:`.User` this ban concerns makes any request where they've
		successfully authorized. This is to avoid having to use cron jobs and the
		like.
	"""

	reason = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=True
	)
	"""The reason for a ban."""

	def get_id_url(self: UserBan) -> str:
		"""Gets the JSON-LD ``@id`` route to this ban.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"user.ban.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: UserBan) -> None:
		r"""Adds the :attr:`instance_actions <.UserBan.instance_actions>`
		attribute, containing possible actions users are allowed to perform
		on bans:

			``edit``:
				Whether or not a user can edit this ban, including its expiration
				timestamp.

			``view``:
				Whether or not a user can view this ban.

		.. note::
			Since deleting bans - only setting their expiration timestamp to the
			current date and time - isn't formally allowed, the permission to do
			so here doesn't exist.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"edit": self.get_instance_action_edit,
			"view": self.get_instance_action_view
		}

	def get_instance_action_edit(self: UserBan, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this ban.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.user.instance_actions["edit_ban"](user)
		)

	def get_instance_action_view(self: UserBan, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this ban.

		:param user: The user.

		:returns: The result of the check.
		"""

		return self.user.instance_actions["view_ban"](user)

	@staticmethod
	def get_static_action_create(
		user,
		receiving_user=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create bans.

		:param user: The performing user.
		:param receiving_user: The receiving user. :data:`None` by default,
			meaning they are not considered.

		:returns: The result of the check.

		.. note::
			To allow for universal keyword argument usage, ``user`` has not been
			changed to ``performing_user``.
		"""

		from . import User

		return (
			UserBan.static_actions["view"](user) and
			User.static_actions["create_ban"](user) and (
				receiving_user is None or
				receiving_user.instance_actions["create_ban"](user)
			)
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit bans.

		:param user: The user.

		:returns: The result of the check.
		"""

		from . import User

		return (
			UserBan.static_actions["view"](user) and
			User.static_actions["edit_ban"](user)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view bans.

		:param user: The user.

		:returns: The result of the check.
		"""

		from . import User

		return User.static_actions["view_ban"](user)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which bans ``user`` is
		allowed to edit.

		:param user: The user.

		:returns: The query.
		"""

		from . import User

		return sqlalchemy.and_(
			UserBan.action_queries["view"](user),
			User.action_queries["edit_ban"](user)
		)

	@staticmethod
	def get_action_query_view(user) -> bool:
		"""Generates a selectable condition representing which bans ``user`` is
		allowed to view.

		:param user: The user.

		:returns: The query.
		"""

		from . import User

		return User.action_queries["view_ban"](user)

	static_actions = {
		"create": get_static_action_create,
		"edit": get_static_action_edit,
		"view": get_static_action_view
	}
	r"""Actions a given user is allowed to perform on any ban, without any
	indication of which one it is.

	``create``:
		Whether or not a user can create bans.

	``edit``:
		Whether or not a user can edit bans.

	``view``:
		Whether or not a user can view bans.
	"""

	action_queries = {
		"edit": get_action_query_edit,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.UserBan.instance_actions>`.
	"""

	@sqlalchemy.ext.hybrid.hybrid_property
	def is_expired(self: UserBan) -> bool:
		"""Checks whether or not this ban's
		:attr:`expiration_timestamp <.UserBan.expiration_timestamp>` is after the
		current UTC time, meaning it's expired.

		:returns: The result of the check.
		"""

		return (
			datetime.datetime.now(tz=datetime.timezone.utc)
			>= self.expiration_timestamp
		)

	@is_expired.expression
	def is_expired(cls: UserBan) -> sqlalchemy.sql.elements.BinaryExpression:
		"""Generates a SQLAlchemy condition representing or not a ban's
		:attr:`expiration_timestamp <.UserBan.expiration_timestamp>` is after the
		current UTC time, meaning it's expired.

		:returns: The condition.
		"""

		return (
			datetime.datetime.now(tz=datetime.timezone.utc)
			>= cls.expiration_timestamp
		)

	def expire(
		self: UserBan,
		current_utc_time: typing.Union[None, datetime.datetime] = None
	) -> None:
		"""Sets this ban's :attr:`.UserBan.expiration_timestamp` to the current
		date and time, expiring it.

		:param current_utc_time: The current time, in the UTC timezone. This is
			used as the new expiration timestamp value, but can remain
			:data:`None`. The time will be recorded at runtime then.

		.. warning::
			The parent user's :attr:`.is_banned <.User.is_banned>` attribute
			will remain unchanged, as child objects shouldn't modify their parents.
		"""

		self.expiration_timestamp = (
			current_utc_time
			if current_utc_time is not None
			else datetime.datetime.now(tz=datetime.timezone.utc)
		)

	@classmethod
	def get(
		cls,
		session: sqlalchemy.orm.Session,
		user,
		action_conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		order_by: typing.Union[
			None,
			sqlalchemy.sql.elements.UnaryExpression
		] = None,
		limit: typing.Union[
			None,
			int
		] = None,
		offset: typing.Union[
			None,
			int
		] = None,
		pkeys_only: bool = False,
		check_expired: bool = True
	):
		r"""Generates a selection query with permissions already handled. This
		may execute additional queries with some models.

		:param session: The SQLAlchemy session to execute additional queries with.
		:param user: The user whose permissions should be evaluated.
		:param action_conditions: Additional conditions that bans should
			fulfill, including the context of data used within actions.
			:data:`True` by default, meaning there are no additional conditions.
		:param conditions: Any additional conditions. :data:`True` by default,
			meaning there are no conditions.
		:param order_by: An expression to order by.
		:param limit: A limit.
		:param offset: An offset.
		:param pkeys_only: Whether or not to only return a query for the primary
			key.
		:param check_expired: Whether or not to check the retrieved bans'
			expiration and handle it if necessary. :data:`True` by default.

		:returns: The query.

		.. note::
			In this implementation, ``conditions`` are in the same position as
			``action_conditions`` and will be treated the same, but this is not
			the case with most other ones, and could cause trouble if not included
			here. This is also the case in the mixin this method is inherited
			from - :class:`PermissionControlMixin <heiwa.database.utils.Permis\
			sionControlMixin>`.
		"""

		if not check_expired:
			return (
				sqlalchemy.select(
					cls if not pkeys_only else cls.id
				).
				where(
					sqlalchemy.and_(
						cls.action_queries["view"](user),
						conditions,
						action_conditions
					)
				).
				order_by(order_by).
				limit(limit).
				offset(offset)
			)

		from . import User

		bans = session.execute(
			sqlalchemy.select(cls).
			where(
				sqlalchemy.and_(
					cls.action_queries["view"](user),
					conditions,
					action_conditions
				)
			).
			order_by(order_by).
			limit(limit).
			offset(offset)
		).scalars().all()

		ban_ids = []
		expired_ban_user_ids = []

		for ban in bans:
			ban_ids.append(ban.id)

			if ban.is_expired:
				expired_ban_user_ids.append(ban.receiver_id)

		if len(expired_ban_user_ids) != 0:
			session.execute(
				sqlalchemy.update(User).
				where(
					sqlalchemy.and_(
						User.id.in_(expired_ban_user_ids),
						User.is_banned.is_(True)
					)
				).
				values(is_banned=False)
			)

			session.commit()

		return (
			sqlalchemy.select(cls if not pkeys_only else cls.id).
			where(cls.id.in_(ban_ids)).
			order_by(order_by)
		)
