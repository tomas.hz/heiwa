"""Models relating to :class:`.Forum` permissions."""

from __future__ import annotations

import typing

import flask
import sqlalchemy
import sqlalchemy.orm

from .. import Base, utils

__all__ = [
	"ForumParsedPermissions",
	"ForumPermissionMixin",
	"ForumPermissionsGroup",
	"ForumPermissionsUser"
]


@sqlalchemy.orm.declarative_mixin
class ForumPermissionMixin:
	"""A utility mixin with columns corresponding to all permissions recognized
	only in forums.
	"""

	category_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	category_delete = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	category_edit = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	category_move = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	category_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_delete = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_edit = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_merge = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_move = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	forum_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_delete_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_delete_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_edit_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_edit_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_edit_vote = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_move_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_move_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	post_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_create_approved = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_create_unapproved = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_delete_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_delete_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_approval = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_lock_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_lock_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_pin = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_edit_vote = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_merge_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_merge_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_move_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_move_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)
	thread_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=True
	)

	DEFAULT_PERMISSIONS = {
		"category_create": None,
		"category_delete": None,
		"category_edit": None,
		"category_move": None,
		"category_view": None,
		"forum_create": None,
		"forum_delete": None,
		"forum_edit": None,
		"forum_merge": None,
		"forum_move": None,
		"forum_view": None,
		"post_create": None,
		"post_delete_own": None,
		"post_delete_any": None,
		"post_edit_own": None,
		"post_edit_any": None,
		"post_edit_vote": None,
		"post_move_own": None,
		"post_move_any": None,
		"post_view": None,
		"thread_create_approved": None,
		"thread_create_unapproved": None,
		"thread_delete_own": None,
		"thread_delete_any": None,
		"thread_edit_own": None,
		"thread_edit_any": None,
		"thread_edit_approval": None,
		"thread_edit_lock_own": None,
		"thread_edit_lock_any": None,
		"thread_edit_pin": None,
		"thread_edit_vote": None,
		"thread_merge_own": None,
		"thread_merge_any": None,
		"thread_move_own": None,
		"thread_move_any": None,
		"thread_view": None
	}
	"""The default values of all permissions. In this case, :data:`None`."""

	def to_permissions(self: ForumPermissionMixin) -> typing.Dict[
		str,
		typing.Union[
			None,
			bool
		]
	]:
		"""Transforms the values in this instance to the standard format for
		permissions - a dictionary, where string keys represent permissions,
		and their boolean value represents whether or not they're granted.

		:returns: The transformed permissions.
		"""

		return {
			permission_name: getattr(self, permission_name)
			for permission_name in self.DEFAULT_PERMISSIONS
		}


class ForumParsedPermissions(
	utils.CDWMixin,
	utils.LoggerMixin,
	ForumPermissionMixin,
	utils.ReprMixin,
	Base
):
	r"""A utility model used to store cached forum permissions based on the
	:class:`.ForumPermissionMixin` for :class:`.User`\ s.

	.. warning::
		This model should not be directly editable for any reason. It's
		automatically calculated from other models using the
		:meth:`parse_permissions <.Forum.parse_permissions>` method.
	"""

	__tablename__ = "forum_parsed_permissions"

	forum_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"forums.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.Forum.id>` of the :class:`.Forum` a set of cached
	permissions relates to.
	"""

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` a set of cached
	permissions relates to.
	"""

	category_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	category_delete = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	category_edit = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	category_move = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	category_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	forum_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	forum_delete = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	forum_edit = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	forum_merge = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	forum_move = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	forum_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_create = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_delete_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_delete_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_edit_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_edit_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_edit_vote = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_move_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_move_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	post_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_create_approved = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_create_unapproved = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_delete_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_delete_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_edit_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_edit_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_edit_approval = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_edit_lock_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_edit_lock_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_edit_pin = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_edit_vote = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_merge_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_merge_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_move_own = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_move_any = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)
	thread_view = sqlalchemy.Column(
		sqlalchemy.Boolean,
		nullable=False
	)

	def get_id_url(self: ForumParsedPermissions) -> str:
		"""Gets the JSON-LD ``@id`` route to these parsed permissions.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"forum.parsed_permissions.view",
			id_=self.id,
			_external=True
		)

	DEFAULT_PERMISSIONS = {
		"category_create": False,
		"category_delete": False,
		"category_edit": False,
		"category_move": False,
		"category_view": False,
		"forum_create": False,
		"forum_delete": False,
		"forum_edit": False,
		"forum_merge": False,
		"forum_move": False,
		"forum_view": False,
		"post_create": False,
		"post_delete_own": False,
		"post_delete_any": False,
		"post_edit_own": False,
		"post_edit_any": False,
		"post_edit_vote": False,
		"post_move_own": False,
		"post_move_any": False,
		"post_view": False,
		"thread_create_approved": False,
		"thread_create_unapproved": False,
		"thread_delete_own": False,
		"thread_delete_any": False,
		"thread_edit_own": False,
		"thread_edit_any": False,
		"thread_edit_approval": False,
		"thread_edit_lock_own": False,
		"thread_edit_lock_any": False,
		"thread_edit_pin": False,
		"thread_edit_vote": False,
		"thread_merge_own": False,
		"thread_merge_any": False,
		"thread_move_own": False,
		"thread_move_any": False,
		"thread_view": False
	}
	"""The default values of all permissions. In this case, :data:`False`."""


class ForumPermissionsGroup(
	ForumPermissionMixin,
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	r"""A utility model used to store :class:`.Forum` permissions for
	:class:`.Group`\ s, based on the :class:`.ForumPermissionMixin`.

	At a given time, only one instance of this model can exist for a specific
	forum and group combination.
	"""

	__tablename__ = "forum_permissions_group"

	forum_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"forums.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.Forum.id>` of the :class:`.Forum` a set of group permissions
	relates to.
	"""

	group_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"groups.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.Group.id>` of the :class:`.Group` a set of group permissions
	relates to.
	"""

	def get_id_url(self: ForumPermissionsGroup) -> str:
		"""Gets the JSON-LD ``@id`` route to these group permissions.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"forum.permissions_group.view",
			forum_id=self.forum_id,
			group_id=self.group_id,
			_external=True
		)

	def permission_control_init(self: ForumPermissionsGroup) -> None:
		r"""Adds the :attr:`instance_actions
		<.ForumPermissionsGroup.instance_actions>` attribute, containing possible
		actions :class:`.User`\ s are allowed to perform on sets of group
		permissions:

			``delete``:
				Whether or not a user can delete this set of group permissions.

			``edit``:
				Whether or not a user can edit this set of group permissions.

			``view``:
				Whether or not a user can view this set of group permissions.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"view": self.get_instance_action_view
		}

	def get_instance_action_delete(self: ForumPermissionsGroup, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this set of
		group permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.forum.instance_actions["edit_permissions_group"](user)
		)

	def get_instance_action_edit(self: ForumPermissionsGroup, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this set of
		group permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.forum.instance_actions["edit_permissions_group"](user)
		)

	def get_instance_action_view(self: ForumPermissionsGroup, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this set of
		group permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return self.forum.instance_actions["view_permissions_group"](user)

	@staticmethod
	def get_static_action_create(
		user,
		forum=None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create sets of group
		permissions.

		:param user: The user, a :class:`.User`.
		:param forum: The forum to assign the permissions to. :data:`None` by
			default, meaning this is not considered.

		:returns: The result of the check.
		"""

		from . import Forum

		return (
			ForumPermissionsGroup.static_actions["view"](user) and
			Forum.static_actions["edit_permissions_group"](user) and (
				forum is None or
				forum.instance_actions["edit_permissions_group"](user)
			)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete sets of group
		permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Forum

		return (
			ForumPermissionsGroup.static_actions["view"](user) and
			Forum.static_actions["edit_permissions_group"](user)
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit sets of group
		permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Forum

		return (
			ForumPermissionsGroup.static_actions["view"](user) and
			Forum.static_actions["edit_permissions_group"](user)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view sets of group
		permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Forum

		return Forum.static_actions["view_permissions_group"](user)

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which sets of group
		permissions ``user`` is allowed to delete.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Forum

		return sqlalchemy.and_(
			ForumPermissionsGroup.action_queries["view"](user),
			Forum.action_queries["edit_permissions_group"](user)
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which sets of group
		permissions ``user`` is allowed to edit.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Forum

		return sqlalchemy.and_(
			ForumPermissionsGroup.action_queries["view"](user),
			Forum.action_queries["edit_permissions_group"](user)
		)

	@staticmethod
	def get_action_query_view(user) -> sqlalchemy.sql.elements.BinaryExpression:
		"""Generates a selectable condition representing which sets of group
		permissions ``user`` is allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Forum

		return Forum.action_queries["view_permissions_group"](user)

	static_actions = {
		"create": get_static_action_create,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"view": get_static_action_view
	}
	r"""Actions a given user is allowed to perform on any set of group permissions,
	without any indication of which one it is.

	``create``:
		Whether or not a user can create sets of group permissions.

	``delete``:
		Whether or not a user can delete sets of group permissions.

	``edit``:
		Whether or not a user can edit sets of group permissions.

	``view``:
		Whether or not a user can view sets of group permissions.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.ForumPermissionsGroup.instance_actions>`.
	"""

	def write(
		self: ForumPermissionsGroup,
		session: sqlalchemy.orm.Session
	) -> None:
		"""Deletes the related :class:`.Forum`'s permissions for all users who
		are part of the related :class:`.Group`.

		:param session: The session to execute the deletion query with and add
			the new instance to.
		"""

		from ..user import user_groups

		session.execute(
			sqlalchemy.delete(ForumParsedPermissions).
			where(
				sqlalchemy.and_(
					ForumParsedPermissions.forum_id == self.forum_id,
					ForumParsedPermissions.user_id.in_(
						sqlalchemy.select(user_groups.c.user_id).
						where(user_groups.c.group_id == self.group_id)
					)
				)
			)
		)

		return super().write(session)


class ForumPermissionsUser(
	ForumPermissionMixin,
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	r"""A utility model used to store :class:`.Forum` permissions for
	:class:`.User`\ s, based on the :class:`.ForumPermissionMixin`.

	At a given time, only one instance of this model can exist for a specific
	forum and user combination.
	"""

	__tablename__ = "forum_permissions_user"

	forum_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"forums.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.Forum.id>` of the :class:`.Forum` a set of permissions
	relates to.
	"""

	user_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"users.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		primary_key=True
	)
	"""The :attr:`id <.User.id>` of the :class:`.User` a set of permissions
	relates to.
	"""

	def get_id_url(self: ForumPermissionsUser) -> str:
		"""Gets the JSON-LD ``@id`` route to these user permissions.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"forum.permissions_user.view",
			forum_id=self.forum_id,
			user_id=self.user_id,
			_external=True
		)

	def permission_control_init(self: ForumPermissionsUser) -> None:
		r"""Adds the :attr:`instance_actions
		<.ForumPermissionsUser.instance_actions>` attribute, containing possible
		actions :class:`.User`\ s are allowed to perform on sets of user
		permissions:

			``delete``:
				Whether or not a user can delete this set of user permissions.

			``edit``:
				Whether or not a user can edit this set of user permissions.

			``view``:
				Whether or not a user can view this set of user permissions.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"view": self.get_instance_action_view
		}

	def get_instance_action_delete(self: ForumPermissionsUser, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this set of
		user permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.forum.instance_actions["edit_permissions_user"](user)
		)

	def get_instance_action_edit(self: ForumPermissionsUser, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this set of
		user permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.forum.instance_actions["edit_permissions_user"](user)
		)

	def get_instance_action_view(self: ForumPermissionsUser, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this set of
		user permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		return self.forum.instance_actions["view_permissions_user"](user)

	@staticmethod
	def get_static_action_create(
		user,
		forum=None,
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create sets of user
		permissions.

		:param user: The user, a :class:`.User`.
		:param forum: The forum to assign the permissions to. :data:`None` by
			default, meaning this is not considered.

		:returns: The result of the check.
		"""

		from . import Forum

		return (
			ForumPermissionsUser.static_actions["view"](user) and
			Forum.static_actions["edit_permissions_user"](user) and (
				forum is None or
				forum.instance_actions["edit_permissions_user"](user)
			)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete sets of user
		permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Forum

		return (
			ForumPermissionsUser.static_actions["view"](user) and
			Forum.static_actions["edit_permissions_user"](user)
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit sets of user
		permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Forum

		return (
			ForumPermissionsUser.static_actions["view"](user) and
			Forum.static_actions["edit_permissions_user"](user)
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view sets of user
		permissions.

		:param user: The user, a :class:`.User`.

		:returns: The result of the check.
		"""

		from . import Forum

		return Forum.static_actions["view_permissions_user"](user)

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which sets of user
		permissions ``user`` is allowed to delete.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Forum

		return sqlalchemy.and_(
			ForumPermissionsUser.action_queries["view"](user),
			Forum.action_queries["edit_permissions_user"](user)
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which sets of user
		permissions ``user`` is allowed to edit.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Forum

		return sqlalchemy.and_(
			ForumPermissionsUser.action_queries["view"](user),
			Forum.action_queries["edit_permissions_user"](user)
		)

	@staticmethod
	def get_action_query_view(user) -> sqlalchemy.sql.elements.BinaryExpression:
		"""Generates a selectable condition representing which sets of user
		permissions ``user`` is allowed to view.

		:param user: The user, a :class:`.User`.

		:returns: The query.
		"""

		from . import Forum

		return Forum.action_queries["view_permissions_user"](user)

	static_actions = {
		"create": get_static_action_create,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"view": get_static_action_view
	}
	r"""Actions a given user is allowed to perform on any set of user permissions,
	without any indication of which one it is.

	``create``:
		Whether or not a user can create sets of user permissions.

	``delete``:
		Whether or not a user can delete sets of user permissions.

	``edit``:
		Whether or not a user can edit sets of user permissions.

	``view``:
		Whether or not a user can view sets of user permissions.
	"""

	action_queries = {
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"view": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.ForumPermissionsUser.instance_actions>`.
	"""

	def write(
		self: ForumPermissionsUser,
		session: sqlalchemy.orm.Session
	) -> None:
		"""Deletes the related :class:`.Forum`'s permissions for the
		:class:`.User` who this set of permissions relates to.

		:param session: The session to execute the deletion query with and add
			the new instance to.
		"""

		session.execute(
			sqlalchemy.delete(ForumParsedPermissions).
			where(
				sqlalchemy.and_(
					ForumParsedPermissions.forum_id == self.forum_id,
					ForumParsedPermissions.user_id == self.user_id
				)
			)
		)

		return super().write(session)
