"""Forum models and tables."""

from __future__ import annotations

import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from heiwa import constants

from .. import Base, utils
from .permissions import (
	ForumParsedPermissions,
	ForumPermissionMixin,
	ForumPermissionsGroup,
	ForumPermissionsUser
)
from .subscribers import forum_subscribers

__all__ = [
	"Forum",
	"ForumParsedPermissions",
	"ForumPermissionMixin",
	"ForumPermissionsGroup",
	"ForumPermissionsUser",
	"forum_subscribers"
]
__version__ = "1.1.13"


class Forum(
	utils.CDWMixin,
	utils.LoggerMixin,
	utils.PermissionControlMixin,
	utils.ReprMixin,
	utils.IdMixin,
	utils.CreationTimestampMixin,
	utils.EditInfoMixin,
	Base
):
	"""Forum model.

	.. note::
		Forums used to have their specific owners in the past, but since they're
		mainly going to be defined by a group of administrators who host the
		service and dealing with owners would cause unnecessary complications,
		they have been removed.
	"""

	__tablename__ = "forums"

	category_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"categories.id",
			ondelete="SET NULL",
			onupdate="CASCADE"
		),
		index=True,
		nullable=True
	)
	"""The :attr:`id <.Category.id>` of the :class:`.Category` a forum belongs
	in. If :data:`None`, the forum doesn't belong in any category and will
	generally be shown at the bottom in frontend programs.

	When / if the category is deleted, the forum will not be deleted, but
	instead, this column will become :data:`None`.
	"""

	parent_forum_id = sqlalchemy.Column(
		utils.UUID,
		sqlalchemy.ForeignKey(
			"forums.id",
			ondelete="CASCADE",
			onupdate="CASCADE"
		),
		index=True,
		nullable=True
	)
	"""The :attr:`id <.Forum.id>` of the forum another forum is a child of.

	.. seealso::
		:attr:`.Forum.child_forums`

		:meth:`.Forum.get_child_level`
	"""

	name = sqlalchemy.Column(
		sqlalchemy.String(128),
		nullable=False
	)
	"""A forum's name."""

	description = sqlalchemy.Column(
		sqlalchemy.String(65536),
		nullable=True
	)
	"""A forum's description."""

	order = sqlalchemy.Column(
		sqlalchemy.Integer,
		default=0,
		nullable=False
	)
	"""The order a forum will be displayed in by default. The higher it is, the
	higher the forum will be.
	"""

	subscriber_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The amount of :class:`.User`\ s subscribed to a forum.

	.. seealso::
		:obj:`.forum_subscribers`
	"""

	thread_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The amount of :class:`.Thread`\ s in a forum."""

	last_thread_timestamp = sqlalchemy.Column(
		sqlalchemy.DateTime(timezone=True),
		nullable=True
	)
	"""The time the latest :class:`.Thread` in a forum was made. If there haven't
	been any threads so far, this will be :data:`None`.
	"""

	post_count = sqlalchemy.Column(
		sqlalchemy.BigInteger,
		default=0,
		nullable=False
	)
	r"""The amount of :class:`:Post`\ s in all of a forum's
	:class:`.Thread`\ s.
	"""

	category = sqlalchemy.orm.relationship(
		"Category",
		passive_deletes="all",
		foreign_keys=[category_id],
		lazy=True
	)

	parsed_permissions = sqlalchemy.orm.relationship(
		ForumParsedPermissions,
		backref=sqlalchemy.orm.backref(
			"forum",
			uselist=False
		),
		passive_deletes="all",
		lazy=True
	)
	"""A forum's parsed permission cache.

	.. seealso::
		:meth:`.Forum.parse_permissions`

		:class:`.ForumParsedPermissions`
	"""

	permissions_groups = sqlalchemy.orm.relationship(
		ForumPermissionsGroup,
		backref=sqlalchemy.orm.backref(
			"forum",
			uselist=False
		),
		order_by=sqlalchemy.desc(ForumPermissionsGroup.creation_timestamp),
		passive_deletes="all",
		lazy=True
	)
	"""Forum permissions for groups.

	.. seealso::
		:class:`.ForumPermissionsGroup`
	"""

	permissions_users = sqlalchemy.orm.relationship(
		ForumPermissionsUser,
		backref=sqlalchemy.orm.backref(
			"forum",
			uselist=False
		),
		order_by=sqlalchemy.desc(ForumPermissionsUser.creation_timestamp),
		passive_deletes="all",
		lazy=True
	)
	"""Forum permissions for users.

	.. seealso::
		:class:`.ForumPermissionsUser`
	"""

	child_forums = sqlalchemy.orm.relationship(
		lambda: Forum,
		backref=sqlalchemy.orm.backref(
			"parent_forum",
			uselist=False,
			remote_side=lambda: Forum.id
		),
		order_by=lambda: sqlalchemy.desc(Forum.creation_timestamp),
		passive_deletes="all",
		lazy=True
	)
	"""A forum's children. Unless those child forums have their own specific
	permissions set, they inherit their parent's.

	.. seealso::
		:attr:`.Forum.parent_forum_id`

		:meth:`.Forum.get_child_level`
	"""

	def get_id_url(self: Forum) -> str:
		"""Gets the JSON-LD ``@id`` route to this forum.

		:returns: The route.

		.. warning::
			This route must be run in a Flask request context. It will break if it's
			not.
		"""

		return flask.url_for(
			"forum.view",
			id_=self.id,
			_external=True
		)

	def permission_control_init(self: Forum) -> None:
		r"""Adds the :attr:`instance_actions <.Forum.instance_actions>` attribute,
		containing possible actions :class:`.User`\ s are allowed to perform on
		specific forums:

			``create_child``:
				Whether or not a user can create child forums within this forum.

			``create_thread``:
				Whether or not a user can create threads within this forum.

			``delete``:
				Whether or not a user is allowed to delete this forum.

			``edit``:
				Whether or not a user is allowed to edit this forum.

			``edit_permissions_group``:
				Whether or not a user is allowed to edit groups' permissions for this
				forum.

			``edit_permissions_user``:
				Whether or not a user is allowed to edit users' permissions for this
				forum.

			``edit_subscription``:
				Whether or not a user is allowed to subscribe to / unsubscribe from this
				forum.

			``merge``:
				Whether or not a user is allowed to merge this forum with other forums.

			``move``:
				Whether or not a user is allowed to move this forum to another forum,
				making it its child forum.

			``move_category``:
				Whether or not a user is allowed to move :class:`.Category` objects to
				this forum.

			``move_thread``:
				Whether or not a user is allowed to move :class:`.Thread`\ s to this
				forum.

			``view``:
				Whether or not a user is allowed to view this forum.

			``view_permissions_group``:
				Whether or not a user is allowed to view groups' permissions for this
				forum.

			``view_permissions_user``:
				Whether or not a user is allowed to view users' permissions for this
				forum.

			``view_subscription``:
				Whether or not a user is allowed to view their subscription to this forum.

		.. warning::
			Input validation is not handled here. When permissions are being evaluated,
			it should have been performed already. If data entered is invalid (e.g.
			adding another entry to a one-on-one relasionship), the user's permission
			to perform said action can still be evaluated as :data:`True`.
		"""

		super().permission_control_init()

		self.instance_actions = {
			"create_category": self.get_instance_action_create_category,
			"create_child": self.get_instance_action_create_child,
			"create_thread": self.get_instance_action_create_thread,
			"delete": self.get_instance_action_delete,
			"edit": self.get_instance_action_edit,
			"edit_permissions_group": self.get_instance_action_edit,
			"edit_permissions_user": self.get_instance_action_edit,
			"edit_subscription": self.get_instance_action_view,
			"merge": self.get_instance_action_merge,
			"move": self.get_instance_action_move,
			"move_category": self.get_instance_action_move_category,
			"move_thread": self.get_instance_action_create_thread,
			"view": self.get_instance_action_view,
			"view_permissions_group": self.get_instance_action_view,
			"view_permissions_user": self.get_instance_action_view,
			"view_subscription": self.get_instance_action_view
		}

	def get_instance_action_create_category(self: Forum, user) -> bool:
		"""Checks whether or not ``user`` is allowed to create a :class:`.Category`
		within this forum.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.get_parsed_permissions(user).category_create
		)

	def get_instance_action_create_child(self: Forum, user) -> bool:
		"""Checks whether or not ``user`` is allowed to create a child forum
		within this one.

		:param user: The user.

		:returns: The result of the check.

		.. seealso::
			:attr:`.Forum.parent_forum_id`

			:attr:`.Forum.parent_forum`
		"""

		return (
			self.instance_actions["view"](user) and
			self.get_parsed_permissions(user).forum_create
		)

	def get_instance_action_create_thread(
		self: Forum,
		user,
		is_approved: typing.Union[None, bool] = None,
		is_locked: bool = False,
		is_pinned: bool = False
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create a :class:`.Thread`
		in this forum.

		:param user: The user.
		:param is_approved: Whether or not the thread is supposed to be approved.
			:data:`None` by default, meaning it can be either.
		:param is_locked: Whether or not the thread should be locked. :data:`False`
			by default.
		:param is_pinned: Whether or not the thread should be pinned. :data:`False`
			by default.

		:returns: The result of the check.
		"""

		parsed_permissions = self.get_parsed_permissions(user)

		return (
			self.instance_actions["view"](user) and
			parsed_permissions.thread_view and (
				(
					is_approved is None and (
						parsed_permissions.thread_create_approved or
						parsed_permissions.thread_create_unapproved
					)
				) or (
					is_approved and
					parsed_permissions.thread_create_approved
				) or (
					not is_approved and
					parsed_permissions.thread_create_unapproved
				)
			) and (
				not is_locked or
				parsed_permissions.thread_edit_lock_own or
				parsed_permissions.thread_edit_lock_any
			) and (
				not is_pinned or
				parsed_permissions.thread_edit_pin
			)
		)

	def get_instance_action_delete(self: Forum, user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete this forum.

		:param user: The user.

		:returns: The result of the check.

		.. seealso::
			:attr:`.Thread.is_pinned`
		"""

		return (
			self.instance_actions["view"](user) and
			self.get_parsed_permissions(user).forum_delete
		)

	def get_instance_action_edit(self: Forum, user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit this forum.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.get_parsed_permissions(user).forum_edit
		)

	def get_instance_action_view(self: Forum, user) -> bool:
		"""Checks whether or not ``user`` is allowed to view this forum.

		:param user: The user.

		:returns: The result of the check.
		"""

		return self.get_parsed_permissions(user).forum_view

	def get_instance_action_merge(
		self: Forum,
		user,
		future_forum: typing.Union[None, Forum] = None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to merge this forum with
		other forums.

		:param user: The user.
		:param future_forum: The forum this forum is about to be merged with. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The result of the check.
		"""

		return (
			self.instance_actions["view"](user) and
			self.get_parsed_permissions(user).forum_merge and (
				future_forum is None or
				future_forum.instance_actions["merge"](user)
			)
		)

	def get_instance_action_move(
		self: Forum,
		user,
		future_parent: typing.Union[None, Forum] = None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to move this forum to
		other forums.

		:param user: The user.
		:param future_parent: The forum this forum is about to be moved to. By
			default, this is :data:`None`, meaning it's ignored.

		:returns: The result of the check.

		.. seealso::
			:attr:`.Forum.parent_forum_id`

			:attr:`.Forum.parent_forum`
		"""

		return (
			self.instance_actions["view"](user) and
			self.get_parsed_permissions(user).forum_move and (
				future_parent is None or
				future_parent.instance_actions["move"](user)
			)
		)

	def get_instance_action_move_category(self: Forum, user) -> bool:
		"""Checks whether or not ``user`` is allowed to move a :class:`.Category`
		to this forum.

		:param user: The user.

		:returns: The result of the check.

		.. seealso::
			:attr:`.Category.forum_id`
		"""

		return (
			self.instance_actions["view"](user) and
			self.get_parsed_permissions(user).category_move
		)

	@staticmethod
	def get_static_action_create(
		user,
		parent: typing.Union[None, Forum] = None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create forums.

		:param user: The user.
		:param parent: The parent forum. :data:`None` by default, meaning there
			isn't one, or it shouldn't be considered.

		:returns: The result of the check.
		"""

		return (
			Forum.static_actions["view"](user) and
			user.parsed_permissions["forum_create"] and (
				parent is None or
				parent.instance_actions["create"](user)
			)
		)

	@staticmethod
	def get_static_action_create_category(user) -> bool:
		"""Checks whether or not the ``user`` is allowed to create
		:class:`.Category` objects without knowledge of which forum it'll be
		done in.

		:param user: The user.

		:returns: The result.
		"""

		from ..category import Category

		return (
			Forum.static_actions["view"](user) and
			Category.static_actions["create"](user)
		)

	@staticmethod
	def get_static_action_create_thread(
		user,
		is_approved: typing.Union[None, bool] = None,
		is_locked: bool = False,
		is_pinned: bool = False
	) -> bool:
		"""Checks whether or not ``user`` is allowed to create a :class:`.Thread`,
		without knowledge of which forum it's going to be done in.

		:param user: The user.
		:param is_approved: Whether or not the thread is supposed to be approved.
			:data:`None` by default, meaning it can be either.
		:param is_locked: Whether or not the thread should be locked. :data:`False`
			by default.
		:param is_pinned: Whether or not the thread should be pinned. :data:`False`
			by default.

		:returns: The result of the check.
		"""

		from ..thread import Thread

		return (
			Forum.static_actions["view"](user) and
			Thread.static_actions["create"](
				user,
				is_approved=is_approved,
				is_locked=is_locked,
				is_pinned=is_pinned
			)
		)

	@staticmethod
	def get_static_action_delete(user) -> bool:
		"""Checks whether or not ``user`` is allowed to delete forums.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			Forum.static_actions["view"](user) and
			user.parsed_permissions["forum_delete"]
		)

	@staticmethod
	def get_static_action_edit(user) -> bool:
		"""Checks whether or not ``user`` is allowed to edit forums.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			Forum.static_actions["view"](user) and
			user.parsed_permissions["forum_edit"]
		)

	@staticmethod
	def get_static_action_merge(
		user,
		future_forum: typing.Union[None, Forum] = None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to merge forums with another
		forum.

		:param user: The user.
		:param future_forum: The forum that is going to be merged with. :data:`None`
			by deafult, meaning it's not considered.

		:returns: The result of the check.
		"""

		return (
			Forum.static_actions["view"](user) and
			user.parsed_permissions["forum_merge"] and (
				future_forum is None or
				future_forum.instance_actions["merge"](user)
			)
		)

	@staticmethod
	def get_static_action_move(
		user,
		future_parent: typing.Union[None, Forum] = None
	) -> bool:
		"""Checks whether or not ``user`` is allowed to move forums to another
		forum.

		:param user: The user.
		:param future_parent: The forum to move other forums to. :data:`None` by
			default, meaning it's not considered.

		:returns: The result of the check.
		"""

		return (
			Forum.static_actions["view"](user) and
			user.parsed_permissions["forum_move"] and (
				future_parent is None or
				future_parent.instance_actions["move"](user)
			)
		)

	@staticmethod
	def get_static_action_move_category(user) -> bool:
		"""Checks whether or not ``user`` is allowed to move :class:`.Category`
		objects.

		:param user: The user.

		:returns: The result of the check.
		"""

		return (
			Forum.static_actions["view"](user) and
			user.parsed_permissions["category_move"]
		)

	@staticmethod
	def get_static_action_view(user) -> bool:
		"""Checks whether or not ``user`` is allowed to view forums.

		:param user: The user.

		:returns: The result of the check.
		"""

		return user.parsed_permissions["forum_view"]

	@staticmethod
	def get_action_query_create_category(
		user
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which forums ``user``
		can create :class:`.Category` objects in.

		:param user: The user.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Forum.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.category_create.is_(True)).
				exists()
			)
		)

	@staticmethod
	def get_action_query_create_child(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which forums ``user`` is
		allowed to create child forums in.

		:param user: The user.

		:returns: The query.

		.. note::
			Finding forums where the ``create`` action is allowed is impossible, but
			that's not the case for finding ones where only the creation of child forums
			is allowed. Thus, this action exists.
		"""

		return sqlalchemy.and_(
			Forum.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.forum_create.is_(True)).
				exists()
			)
		)

	@staticmethod
	def get_action_query_create_thread(
		user,
		is_approved: typing.Union[None, bool] = None,
		is_locked: bool = False,
		is_pinned: bool = False
	) -> sqlalchemy.sql.elements.ClauseList:
		r"""Generates a selectable condition representing which forums ``user`` is
		allowed to create :class:`.Thread`\ s in.

		:param user: The user.
		:param is_approved: Whether or not the thread should be approved.
			:data:`None` by default, meaning it's not considered.
		:param is_locked: Whether or not the thread should be locked. :data:`False`
			by default.
		:param is_pinned: Whether or not the thread should be pinned. :data:`False`
			by default.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Forum.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(
					sqlalchemy.and_(
						ForumParsedPermissions.thread_view.is_(True),
						(
							sqlalchemy.or_(
								ForumParsedPermissions.thread_create_approved.is_(True),
								ForumParsedPermissions.thread_create_unapproved.is_(True)
							)
							if is_approved is None
							else (
								ForumParsedPermissions.thread_create_approved.is_(True)
								if is_approved
								else ForumParsedPermissions.thread_create_unapproved.is_(True)
							)
						),
						(
							sqlalchemy.or_(
								ForumParsedPermissions.thread_edit_lock_own.is_(True),
								ForumParsedPermissions.thread_edit_lock_any.is_(True)
							)
							if is_locked
							else True
						),
						(
							ForumParsedPermissions.thread_edit_pin.is_(True)
							if is_pinned
							else True
						)
					)
				).
				exists()
			)
		)

	@staticmethod
	def get_action_query_delete(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which forums ``user`` is
		allowed to delete.

		:param user: The user.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Forum.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.forum_delete.is_(True)).
				exists()
			)
		)

	@staticmethod
	def get_action_query_edit(user) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which forums ``user`` is
		allowed to edit.

		:param user: The user.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Forum.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.forum_edit.is_(True)).
				exists()
			)
		)

	@staticmethod
	def get_action_query_merge(
		user,
		future_forum: typing.Union[None, Forum] = None
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which forums ``user`` is
		allowed to merge with another forum.

		:param user: The user.
		:param future_forum: The forum other forums should be merged with.
			:data:`None` by default, meaning this is not considered.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Forum.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.forum_merge.is_(True)).
				exists()
			),
			(
				True
				if future_forum is None
				else future_forum.action_queries["merge"](user)
			)
		)

	@staticmethod
	def get_action_query_move(
		user,
		future_parent: typing.Union[None, Forum] = None
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which forums ``user`` is
		allowed to move to another forum.

		:param user: The user.
		:param future_parent: The forum other forums should be moved to. :data:`None`
			by default, meaning this is not considered.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Forum.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.forum_move.is_(True)).
				exists()
			),
			(
				True
				if future_parent is None
				else future_parent.action_queries["move"](user)
			)
		)

	@staticmethod
	def get_action_query_move_category(
		user
	) -> sqlalchemy.sql.elements.ClauseList:
		"""Generates a selectable condition representing which forums ``user`` is
		allowed to move :class:`.Category` objects to.

		:param user: The user.

		:returns: The query.
		"""

		return sqlalchemy.and_(
			Forum.action_queries["view"](user),
			(
				sqlalchemy.select(ForumParsedPermissions.forum_id).
				where(ForumParsedPermissions.category_move.is_(True)).
				exists()
			)
		)

	@staticmethod
	def get_action_query_view(user) -> sqlalchemy.sql.elements.BinaryExpression:
		"""Generates a selectable condition representing which forums ``user`` is
		allowed to view.

		:param user: The user.

		:returns: The query.
		"""

		return (
			sqlalchemy.select(ForumParsedPermissions.forum_id).
			where(ForumParsedPermissions.forum_view.is_(True)).
			exists()
		)

	static_actions = {
		"create": get_static_action_create,
		"create_category": get_static_action_create_category,
		"create_thread": get_static_action_create_thread,
		"delete": get_static_action_delete,
		"edit": get_static_action_edit,
		"edit_permissions_group": get_static_action_edit,
		"edit_permissions_user": get_static_action_edit,
		"edit_subscription": get_static_action_view,
		"merge": get_static_action_merge,
		"move": get_static_action_move,
		"move_category": get_static_action_move_category,
		"move_thread": get_static_action_create_thread,
		"view": get_static_action_view,
		"view_permissions_group": get_static_action_view,
		"view_permissions_user": get_static_action_view,
		"view_subscription": get_static_action_view
	}
	r"""Actions :class:`User`\ s are allowed to perform on all threads, without
	any indication of which thread it is.

	``create``:
		Whether or not a user can create forums.

	``create_category``
		Whether or not a user is allowed to create :class:`.Category` objects.

	``create_child``:
		Whether or not a user can create child forums within other forums.

	``create_thread``:
		Whether or not a user can create threads within forums.

	``delete``:
		Whether or not a user can delete forums.

	``edit``:
		Whether or not a user can edit forums.

	``edit_permissions_group``:
		Whether or not a user can edit forums' permissions for groups.

	``edit_permissions_user``:
		Whether or not a user can edit forums' permissions for users.

	``edit_subscription``:
		Whether or not a user is allowed to subscribe to / unsubscribe from forums.

	``merge``:
		Whether or not a user is allowed to merge forums with another forum.

	``move``:
		Whether or not a user is allowed to move forums to another forum, making
		them its child forums.

	``move_category``:
		Whether or not a user is allowed to move :class:`.Category` objects.

	``move_thread``:
		Whether or not a user is allowed to move :class:`.Thread`\ s.

	``view``:
		Whether or not a user is allowed to view forums.

	``view_permissions_group``:
		Whether or not a user is allowed to view groups' permissions for forums.

	``view_permissions_user``:
		Whether or not a user is allowed to view users' permissions for forums.

	``view_permissions_user``:
		Whether or not a user is allowed to view their subscriptions to forums.
	"""

	action_queries = {
		"create_category": get_action_query_create_category,
		"create_child": get_action_query_create_child,
		"create_thread": get_action_query_create_thread,
		"delete": get_action_query_delete,
		"edit": get_action_query_edit,
		"edit_permissions_group": get_action_query_edit,
		"edit_permissions_user": get_action_query_edit,
		"edit_subscription": get_action_query_view,
		"merge": get_action_query_merge,
		"move": get_action_query_move,
		"move_category": get_action_query_move_category,
		"move_thread": get_action_query_create_thread,
		"view": get_action_query_view,
		"view_permissions_group": get_action_query_view,
		"view_permissions_user": get_action_query_view,
		"view_subscription": get_action_query_view
	}
	"""Actions and their required permissions translated to be evaluable within
	SQL queries. These permissions will generally be the same as
	:attr:`instance_actions <.Group.instance_actions>`.
	"""

	def delete(self: Forum) -> None:
		r"""Deletes all :class:`.Notification`\ s associated with this forum, as
		well as the forum itself.
		"""

		from ..notification import Notification
		from ..post import Post
		from ..thread import Thread

		session = sqlalchemy.orm.object_session(self)

		thread_ids = session.execute(
			sqlalchemy.select(Thread.id).
			where(Thread.forum_id == self.id)
		).scalars().all()

		session.execute(
			sqlalchemy.delete(Notification).
			where(
				sqlalchemy.or_(
					sqlalchemy.and_(
						Notification.type.in_(Thread.NOTIFICATION_TYPES),
						Notification.identifier.in_(thread_ids)
					),
					sqlalchemy.and_(
						Notification.type.in_(Post.NOTIFICATION_TYPES),
						Notification.identifier.in_(
							sqlalchemy.select(Post.id).
							where(Post.thread_id.in_(thread_ids))
						)
					)
				)
			).
			execution_options(synchronize_session="fetch")
		)

		return super().delete()

	def get_is_subscribed(self: Forum, user_id: uuid.UUID) -> bool:
		"""Checks whether or not the :class:`.User` with the given ``user_id``
		is subscribed to this forum.

		:param user_id: The :attr:`id <.User.id>` of the user.

		:returns: The result of the check.
		"""

		return sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(forum_subscribers.c.forum_id).
			where(
				sqlalchemy.and_(
					forum_subscribers.c.forum_id == self.id,
					forum_subscribers.c.user_id == user_id
				)
			).
			exists().
			select()
		).scalars().one()

	@classmethod
	def get(
		cls,
		session: sqlalchemy.orm.Session,
		user,
		action_conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		conditions: typing.Union[
			bool,
			sqlalchemy.sql.elements.BinaryExpression,
			sqlalchemy.sql.elements.ClauseList
		] = True,
		order_by: typing.Union[
			None,
			sqlalchemy.sql.elements.UnaryExpression
		] = None,
		limit: typing.Union[
			None,
			int
		] = None,
		offset: typing.Union[
			None,
			int
		] = None,
		pkeys_only: bool = False
	) -> sqlalchemy.sql.Select:
		"""Generates a selection query with permissions already handled.

		Since forums' permissions may not be parsed, this will always emit
		additional queries to check.

		:param session: The SQLAlchemy session to execute additional queries with.
		:param user: The user whose permissions should be evaluated.
		:param action_conditions: Additional conditions that forums should
			fulfill, including the context of parsed permissions, unlike
			``conditions``. Generally, this will be generated using
			:attr:`action_queries <.Forum.action_queries>`. :data:`True` by
			default, meaning there are no additional conditions.
		:param conditions: Any additional conditions, outside a parsed permission
			context. :data:`True` by default, meaning there are no conditions.
		:param order_by: An expression to order by.
		:param limit: A limit.
		:param offset: An offset.
		:param pkeys_only: Whether or not to only return a query for the primary
			key.

		:returns: The query.
		"""

		inner_conditions = sqlalchemy.and_(
			ForumParsedPermissions.forum_id == cls.id,
			ForumParsedPermissions.user_id == user.id
		)

		while True:
			rows = session.execute(
				sqlalchemy.select(
					cls.id,
					(
						sqlalchemy.select(ForumParsedPermissions.forum_id).
						where(inner_conditions).
						exists()
					)
				).
				where(
					sqlalchemy.and_(
						conditions,
						sqlalchemy.or_(
							~(
								sqlalchemy.select(ForumParsedPermissions.forum_id).
								where(inner_conditions).
								exists()
							),
							(
								sqlalchemy.select(ForumParsedPermissions.forum_id).
								where(
									sqlalchemy.and_(
										inner_conditions,
										cls.action_queries["view"](user),
										action_conditions
									)
								).
								exists()
							)
						)
					)
				).
				order_by(order_by).
				limit(limit).
				offset(offset)
			).all()

			if len(rows) == 0:
				# No need to hit the database with a complicated query twice
				return (
					# Just in case
					sqlalchemy.select(cls if not pkeys_only else cls.id).
					where(False)
				)

			forum_ids = []
			unparsed_permission_forum_ids = []

			for row in rows:
				forum_id, parsed_permissions_exist = row

				if not parsed_permissions_exist:
					unparsed_permission_forum_ids.append(forum_id)

					continue

				forum_ids.append(forum_id)

			if len(unparsed_permission_forum_ids) != 0:
				for forum in (
					session.execute(
						sqlalchemy.select(cls).
						where(cls.id.in_(unparsed_permission_forum_ids))
					).scalars()
				):
					forum.parse_permissions(user)

				session.commit()
			else:
				return (
					sqlalchemy.select(cls if not pkeys_only else cls.id).
					where(cls.id.in_(forum_ids)).
					order_by(order_by)
				)

	def _get_child_forum_and_own_ids(
		self: Forum,
		current_id: typing.Union[
			None,
			uuid.UUID
		] = None
	) -> typing.List[uuid.UUID]:
		"""Gets a list of this forum's :attr:`id <.Forum.id>`, combined with its
		child forums'.

		:param current_id: The :attr:`id <.Forum.id>` of the forum this function
			is currently working with.

		:returns: A list of the IDs.

		.. seealso::
			:attr:`.Forum.child_forums`
		"""

		if current_id is None:
			current_id = self.id

		ids = [current_id]

		for child_forum_id in sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(Forum.id).
			where(Forum.parent_forum_id == current_id)
		).scalars().all():
			ids += self._get_child_forum_and_own_ids(child_forum_id)

		return ids

	def delete_all_parsed_permissions(self: Forum) -> None:
		"""Deletes all instances of :class:`.ForumParsedPermissions` associated
		with this forum, as well as its children.
		"""

		sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.delete(ForumParsedPermissions).
			where(
				ForumParsedPermissions.forum_id.in_(
					self._get_child_forum_and_own_ids()
				)
			)
		)

	def _parse_child_level(
		self: Forum,
		child_level: int = 0,
		current_id: typing.Union[
			None,
			uuid.UUID
		] = None
	) -> int:
		"""Gets how many levels 'deep' this forum is. For example, if there is
		no parent forum set, it's ``0``. If it's the child of a forum with no
		parent of it sown, it's ``1``.

		:param child_level: The current child level.
		:param current_id: The :attr:`id <.Forum.id>` of the current forum.

		:returns: The child level.

		.. seealso::
			:meth:`.Forum.get_child_level`
		"""

		if current_id is None:
			current_id = self.id

		parent_forum_id = sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(Forum.parent_forum_id).
			where(Forum.id == current_id)
		).scalars().one_or_none()

		if parent_forum_id is not None:
			child_level += 1

			return self._parse_child_level(
				child_level,
				parent_forum_id
			)

		return child_level

	def get_child_level(self: Forum) -> int:
		"""Returns how many levels 'deep' this forum is. For example, if there is
		no parent forum set, it's ``0``. If it's the child of a forum with no
		parent of it sown, it's ``1``.

		This value is primarily used to limit forum creation through the API. If
		the child level limit is, for example, a fairly liberal ``25``, forums
		more than 25 levels deep will not be created.

		.. note::
			Ideally, there would be no limit at all, but calculating inherited
			permissions has to be done through a recursive function to some
			extent. Recursion, at least in CPython, currently has inherent limits.

		.. seealso::
			:meth:`.Forum._parse_child_level`
		"""

		if self.parent_forum_id is None:
			return 0

		return self._parse_child_level(
			1,
			self.parent_forum_id
		)

	def get_parsed_permissions(
		self: Forum,
		user,
		auto_parse: bool = True
	) -> typing.Union[
		None,
		ForumParsedPermissions
	]:
		"""Gets the cached parsed permissions this forum has for the given
		``user``. If ``auto_parse`` is :data:`True` and the permissions have not
		yet been parsed, they are parsed automatically.

		:param user: The user whose parsed permissions should be obtained.
		:param auto_parse: Whether or not to parse the permissions, if it hasn't
			been done yet.

		:returns: The :class:`.ForumParsedPermissions`.
		"""

		parsed_permissions = sqlalchemy.orm.object_session(self).get(
			ForumParsedPermissions,
			(
				self.id,
				user.id
			)
		)  # Don't query twice for the same thing

		if parsed_permissions is None and auto_parse:
			self.parse_permissions(user)

		return parsed_permissions

	def get_subscriber_ids(self: Forum) -> typing.List[uuid.UUID]:
		r"""Gets this forum's subscribers' :attr:`id <.User.id>`\ s.

		:returns: The subscriber IDs.

		.. seealso::
			:obj:`.forum_subscribers`
		"""

		return sqlalchemy.orm.object_session(self).execute(
			sqlalchemy.select(forum_subscribers.c.user_id).
			where(forum_subscribers.c.forum_id == self.id)
		).scalars().all()

	def _get_permissions_group(
		self: Forum,
		group_id: uuid.UUID,
		forum_id: typing.Union[
			None,
			uuid.UUID
		] = None
	) -> typing.Dict[str, bool]:
		"""Returns this forum's, as well as the parent forums' permissions for
		the :class:`.Group` with the given ``group_id``. If the ``forum_id``
		argument is :data:`None`, the current forum is considered to be ``self``.
		Otherwise, the :attr:`id <.Forum.id>` supplied within the argument is
		considered to be that of the current forum's.

		This forum's permissions take precedence. For unset permissions, the
		parent closest to this forum takes precedence.

		:param group_id: The :attr:`id <.Group.id>` of the :class:`.Group` whose
			permissions should be found.
		:param forum_id: The current forum's ID.

		:returns: A transformed version of the group's permissions.

		.. note::
			In the past, this function used to retrieve the parent forum object,
			and called this function on it. This does improve readability, but
			introduces a considerable amount of unnecessary data being used.

		.. seealso::
			:class:`.ForumPermissionsGroup`
		"""

		session = sqlalchemy.orm.object_session(self)

		if forum_id is None:
			forum_id = self.id
			parent_forum_id = self.parent_forum_id
		else:
			parent_forum_id = session.execute(
				sqlalchemy.select(Forum.parent_forum_id).
				where(Forum.id == forum_id)
			).scalars().one_or_none()

		parsed_group_permissions = {}

		own_group_permissions = session.execute(
			sqlalchemy.select(ForumPermissionsGroup).
			where(
				sqlalchemy.and_(
					ForumPermissionsGroup.group_id == group_id,
					ForumPermissionsGroup.forum_id == forum_id
				)
			)
		).scalars().one_or_none()

		if own_group_permissions is not None:
			parsed_group_permissions = own_group_permissions.to_permissions()

		if parent_forum_id is not None:
			for permission_name, permission_value in self._get_permissions_group(
				group_id,
				parent_forum_id
			).items():
				if (
					permission_value is None or
					parsed_group_permissions.get(permission_name) is not None
				):
					continue

				parsed_group_permissions[permission_name] = permission_value

		return parsed_group_permissions

	def _get_permissions_user(
		self: Forum,
		user_id: uuid.UUID,
		forum_id: typing.Union[
			None,
			uuid.UUID
		] = None
	) -> typing.Dict[str, bool]:
		"""Returns this forum's, as well as the parent forums' permissions for
		the :class:`.User` with the given ``user_id``. If the ``forum_id``
		argument is :data:`None`, the current forum is considered to be ``self``.
		Otherwise, the :attr:`id <.Forum.id>` supplied within the argument is
		considered to be that of the current forum's.

		This forum's permissions take precedence. For unset permissions, the
		parent closest to this forum takes precedence.

		:param user_id: The :attr:`id <.User.id>` of the :class:`.User` whose
			permissions should be found.
		:param forum_id: The current forum's ID.

		:returns: A transformed version of the user's permissions.

		.. note::
			In the past, this function used to retrieve the parent forum object,
			and called this function on it. This does improve readability, but
			introduces a considerable amount of unnecessary data being used.

		.. seealso::
			:class:`.ForumPermissionsUser`
		"""

		session = sqlalchemy.orm.object_session(self)

		if forum_id is None:
			forum_id = self.id
			parent_forum_id = self.parent_forum_id
		else:
			parent_forum_id = session.execute(
				sqlalchemy.select(Forum.parent_forum_id).
				where(Forum.id == forum_id)
			).scalars().one_or_none()

		parsed_user_permissions = {}

		own_user_permissions = session.execute(
			sqlalchemy.select(ForumPermissionsUser).
			where(
				sqlalchemy.and_(
					ForumPermissionsUser.user_id == user_id,
					ForumPermissionsUser.forum_id == forum_id
				)
			)
		).scalars().one_or_none()

		if own_user_permissions is not None:
			parsed_user_permissions = own_user_permissions.to_permissions()

		if parent_forum_id is not None:
			for permission_name, permission_value in self._get_permissions_user(
				user_id,
				parent_forum_id
			).items():
				if (
					permission_value is None or
					parsed_user_permissions.get(permission_name) is not None
				):
					continue

				parsed_user_permissions[permission_name] = permission_value

		return parsed_user_permissions

	def parse_permissions(
		self: Forum,
		user
	) -> ForumParsedPermissions:
		"""Sets the given ``user``'s :class:`.ForumParsedPermissions` for the
		current forum to:

			#. The user's :attr:`parsed_permissions <.User.parsed_permissions>`.
			#. Any permissions associated with this forum defined for the groups the
			   user is a part of.
			#. Any permissions associated with this forum defined for the user.

		In that order. The lower on the list a set of permissions is, the more
		important it is and overrides values that existed in the previous set.

		:param user: The :class:`.User` whose permissions should be parsed.

		.. seealso::
			:meth:`.Forum._get_permissions_group`

			:class:`.ForumPermissionsGroup`

			:meth:`.Forum._get_permissions_user`

			:class:`.ForumPermissionsUser`
		"""

		from ..group import Group
		from ..user import user_groups

		session = sqlalchemy.orm.object_session(self)

		parsed_permissions = {}

		for group_id in session.execute(
			sqlalchemy.select(user_groups.c.group_id).
			where(user_groups.c.user_id == user.id).
			order_by(
				sqlalchemy.desc(
					sqlalchemy.select(Group.level).
					where(Group.id == user_groups.c.group_id).
					scalar_subquery()
				)
			)
		).scalars().all():
			for permission_name, permission_value in self._get_permissions_group(
				group_id
			):
				if (
					permission_value is None or
					parsed_permissions.get(permission_name) is not None
				):
					continue

				parsed_permissions[permission_name] = permission_value

		for permission_name, permission_value in self._get_permissions_user(
			user.id
		):
			if permission_value is None:
				continue

			parsed_permissions[permission_name] = permission_value

		for permission_name in ForumPermissionMixin.DEFAULT_PERMISSIONS:
			if parsed_permissions.get(permission_name) is None:
				parsed_permissions[
					permission_name
				] = user.parsed_permissions[
					permission_name
				]

		parsed_permissions_row = self.get_parsed_permissions(
			user,
			auto_parse=False
		)

		self.logger.debug(
			"Reparsed forum permissions for user ID %s: %s",
			user.id,
			parsed_permissions
		)

		if parsed_permissions_row is None:
			parsed_permissions_row = ForumParsedPermissions.create(
				session,
				forum_id=self.id,
				user_id=user.id,
				**parsed_permissions
			)
		else:
			for permission_name, permission_value in parsed_permissions.items():
				setattr(
					parsed_permissions_row,
					permission_name,
					permission_value
				)

		return parsed_permissions_row


sqlalchemy.event.listen(
	forum_subscribers,
	"after_create",
	sqlalchemy.DDL(
		f"""
		CREATE OR REPLACE FUNCTION forum_subscribers_create_delete_update_counters()
		RETURNS trigger
		AS $trigger$
			BEGIN
				IF (TG_OP = 'INSERT') THEN
					UPDATE forums
					SET subscriber_count = subscriber_count + 1
					WHERE
						id = NEW.forum_id AND
						subscriber_count < {constants.BIG_INTEGER_LIMIT};
				ELSIF (TG_OP = 'DELETE') THEN
					UPDATE forums
					SET subscriber_count = subscriber_count - 1
					WHERE id = OLD.forum_id;
				END IF;

				RETURN NULL;
			END;
		$trigger$ LANGUAGE plpgsql;

		CREATE TRIGGER create_delete_update_counters
		AFTER INSERT OR DELETE
		ON forum_subscribers
		FOR EACH ROW
		EXECUTE FUNCTION forum_subscribers_create_delete_update_counters();
		"""
	).execute_if(dialect="postgresql")
)
