"""Authentication."""

import functools
import typing

import authlib.jose
import flask

from .. import database, exceptions

__all__ = ["authenticate_via_jwt"]
__version__ = "3.3.0"


def authenticate_via_jwt(
	func: typing.Callable
) -> typing.Callable:
	"""If the HTTP ``Authorization`` header is present, derives the current
	:class:`User <heiwa.database.User>` from it (if possible) using JWT and
	assigns them to :attr:`flask.g.user`.

	If it has been more time than the ``USER_LAST_ACTIVE_TIMESTAMP_COOLDOWN``
	config key states since the user has last been active (or it's :data:`None`),
	it's checked if they have the storage of their latest activity time enabled.
	If so, the timestamp is then updated.

	If the user is banned, it's checked if their ban has expired. If not, access
	is denied. Otherwise, they're unbanned, the ban is stored for future
	reference and the request continues normally.

	:raises heiwa.exceptions.APIAuthorizationHeaderMissing: Raised when there is
		no HTTP ``Authorization`` header present at all.
	:raises heiwa.exceptions.APIAuthorizationHeaderInvalid: Raised when the HTTP
		``Authorization`` header's content is invalid in the context of this
		function - for example, doesn't start with ``Bearer``.
	:raises heiwa.exceptions.APIAuthorizationJWTInvalid: Raised when the JWT given
		inside the ``Authorization`` header is in any way invalid. For example, it
		cannot be decoded.
	:raises heiwa.exceptions.APIAuthorizationJWTInvalidClaims: Raised when the
		given JWT is semantically valid, but its claims are not. For example, it's
		using an old ``USER_JWT_SECRET_KEY`` signature, or the signature is forged.
	:raises heiwa.exceptions.APIAuthorizationJWTSubjectNotFound: Raised when the
		given JWT is valid, but the user ID in its claims' ``sub`` value does not
		correspond to any user. This can happen when the user has deleted their
		account.
	:raises heiwa.exceptions.APIUserBanned: Raised when the user attempting to
		use the wrapped endpoint has been banned. Their ban is returned in the
		exception's :attr:`details <heiwa.exceptions.APIUserBanned.details>`.
	:raises heiwa.exceptions.APIAuthorizationJWTInvalidSubjectResourceType: Raised
		when the resource type identifier (what is before the first ``:`` in the
		``sub`` claim) is not the :class:`User <heiwa.database.User>` class name.
	"""

	@functools.wraps(func)
	def decorator(*args, **kwargs) -> typing.Any:
		if "Authorization" not in flask.request.headers:
			raise exceptions.APIAuthorizationHeaderMissing

		if not flask.request.headers["Authorization"][:7] == "Bearer ":
			raise exceptions.APIAuthorizationHeaderInvalid

		try:
			claims = authlib.jose.jwt.decode(
				flask.request.headers["Authorization"][7:],
				flask.current_app.config["USER_JWT_SECRET_KEY"]
			)
		except authlib.jose.JoseError as exception:
			raise exceptions.APIAuthorizationJWTInvalid from exception

		try:
			claims.validate()
		except authlib.jose.JoseError as exception:
			raise exceptions.APIAuthorizationJWTInvalidClaims from exception

		resource_type, id_ = claims["sub"].split(":")

		if resource_type != database.User.__class__.__name__:
			raise exceptions.APIAuthorizationJWTInvalidSubjectResourceType

		user = flask.g.sa_session.get(database.User, id_)

		if user is None:
			raise exceptions.APIAuthorizationJWTSubjectNotFound(id_)

		if (
			(
				user.last_active_timestamp is None or
				user.last_active_timestamp < (
					flask.g.current_utc_time
					- flask.current_app.config["USER_LAST_ACTIVE_TIMESTAMP_COOLDOWN"]
				)
			) and user.preferences.should_record_activity
		):
			user.last_active_timestamp = flask.g.current_utc_time

		if user.is_banned:
			ban = user.get_ban(check_expired=True)

			if ban is not None:
				raise exceptions.APIUserBanned(ban)

		flask.g.identifier = user.id
		flask.g.user = user

		return func(*args, **kwargs)
	return decorator
