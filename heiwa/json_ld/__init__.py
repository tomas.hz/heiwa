"""JSON-LD encoding.

.. seealso::
	`JSON for linking data - json-ld.org <https://json-ld.org/>`_
"""

from __future__ import annotations

import base64
import datetime
import enum
import json
import typing
import uuid

import flask
import sqlalchemy
import sqlalchemy.orm

from .. import database, get_config

__all__ = ["JSONLDEncoder"]
__version__ = "2.0.0"


class JSONLDEncoder(json.JSONEncoder):
	r"""A JSON encoder based on the default
	:class:`JSONEncoder <json.JSONEncoder>`, with support for JSON-LD SQLAlchemy
	ORM objects.

	SQLAlchemy ORM model instances are converted to dictionaries containing all
	columns allowed for the current :attr:`flask.g.user` (if there is one), or
	don't start with ``_`` if there are no permissions set up. A ``@context``
	key is added based on the ``JSON_LD_CONTEXT_URL``, as well as ``@type``, which
	is based on their class' names. If the instance has a ``get_id_url``
	method and there is a Flask request context, an ``@id`` is added with the
	result of the method being run.

	:class:`bytes` are converted to their base64-encoded string equivalents.

	:class:`date <datetime.date>`\ s, :class:`time <datetime.time>`\ s and
	:class:`datetime <datetime.datetime>`\ s are converted to ISO-8601 strings.

	:class:`Enum <enum.Enum>`\ s are converted to their values.

	:class:`UUID <uuid.UUID>`\ s are converted to their string equivalents.

	.. note::
		Flask's default JSON encoder converts dates and times using the format
		defined in `RFC 822 <https://tools.ietf.org/html/rfc822>`_. Since it's
		more common and universal, ISO-8601 has been chosen instead.

	.. seealso::
		`ISO-8601 Specification <https://wikiless.org/wiki/ISO_8601>`_
	"""

	def default(
		self: JSONLDEncoder,
		o: object
	) -> typing.Union[
		typing.Any,
		str,
		typing.Dict[
			str,
			typing.Any
		]
	]:
		"""Converts various objects to JSONable values, as described in the
		class' documentation.

		:param o: The object to convert.

		:returns: A converted version of the object.
		"""

		if isinstance(o.__class__, sqlalchemy.orm.DeclarativeMeta):
			converted_object = {
				"@context": (
					get_config()["JSON_LD_CONTEXT_URL"]
					+ f"{o.__class__.__name__}.jsonld"
				),
				"@type": o.__class__.__name__
			}

			if (
				hasattr(o, "get_id_url") and
				flask.has_request_context()
			):
				converted_object["@id"] = o.get_id_url()

			converted_object.update(
				{
					column: getattr(o, column)
					for column in o.get_allowed_columns(flask.g.get("user"))
				}
				if isinstance(o, database.utils.PermissionControlMixin)
				else {
					column.key: getattr(o, column.key)
					for column in sqlalchemy.inspect(o).mapper.column_attrs
					if not column.key.startswith("_")
				}
			)

			return converted_object

		if isinstance(o, bytes):
			return base64.b64encode(o).decode("utf-8")

		if isinstance(
			o,
			datetime.date | datetime.time | datetime.datetime
		):
			return o.isoformat()

		if isinstance(o, enum.Enum):
			return o.value

		if isinstance(o, uuid.UUID):
			return str(o)

		return json.JSONEncoder.default(self, o)
