"""Constant shared by multiple subpackages."""

__all__ = ["BIG_INTEGER_LIMIT"]
__version__ = "3.0.0"

BIG_INTEGER_LIMIT = 9223372036854775807
"""2^63, the limit for a signed ``BIGINT`` SQL type."""
