"""Sheduled tasks."""

from __future__ import annotations

import typing

import datetime
import logging

import apscheduler.triggers.base
import apscheduler.schedulers.background
import apscheduler.schedulers.base
import pytz
import rpyc
import sqlalchemy

from .. import database, constants

__all__ = [
	"SchedulerService",
	"delete_expired_guest_users",
	"delete_expired_thread_view_records",
	"delete_expired_user_bans",
	"delete_unverified_expired_local_accounts",
	"get_scheduler",
	"log_statistics"
]
__version__ = "2.2.0"


class SchedulerService(rpyc.Service):
	r"""An RPyC service used to communicate with and handle an APScheduler from
	another program.

	.. note::
		Adapted from an `example <https://github.com/agronholm/apscheduler/com\
		mit/19fa2fa27a7e168fbcec883817408a21a8b7339e>`_ by
		`Alex Grönholm <https://github.com/agronholm>`_.
	"""

	def __init__(
		self,
		scheduler: apscheduler.schedulers.base.BaseScheduler,
		*args,
		**kwargs
	) -> None:
		"""Assigns the ``scheduler`` to ``self``, then runs the superclass'
		``__init__`` method.

		:param scheduler: The scheduler to operate on.
		"""

		self.scheduler = scheduler

		self.logger = logging.getLogger(__name__)

		super().__init__(*args, **kwargs)

	def add_job(
		self: SchedulerService,
		func: typing.Union[
			str,
			typing.Callable
		],
		*args,
		**kwargs
	):
		"""Adds a job to the :attr:`scheduler <.SchedulerService.scheduler>`.

		:param func: The function to execute.

		:returns: The result of the function call to do so.
		"""

		self.logger.info("Adding job with func: %s", func)

		return self.scheduler.add_job(
			func,
			*args,
			**kwargs
		)

	def modify_job(
		self: SchedulerService,
		job_id: str,
		jobstore: typing.Union[None, str] = None,
		**changes
	):
		"""Modifies a job in the :attr:`scheduler <.SchedulerService.scheduler>`.

		:param job_id: The ID of the job.
		:param jobstore: Where and how the job is stored. Optional.

		:returns: The result of the function call to do so.
		"""

		self.logger.info("Modifying job ID %s", job_id)

		return self.scheduler.modify_job(
			job_id,
			jobstore,
			**changes
		)

	def reschedule_job(
		self: SchedulerService,
		job_id: str,
		jobstore: typing.Union[None, str] = None,
		trigger: typing.Union[
			None,
			str,
			apscheduler.triggers.base.BaseTrigger
		] = None,
		**trigger_args
	):
		"""Reschedules a job in the :attr:`scheduler <.SchedulerService.scheduler>`.

		:param job_id: The ID of the job.
		:param jobstore: Where and how the job is stored. Optional.
		:param trigger: Which trigger determines when the function is run. Optional.

		:returns: The result of the function call to do so.
		"""

		self.logger.info("Rescheduling job ID %s", job_id)

		return self.scheduler.reschedule_job(
			job_id,
			jobstore,
			trigger,
			**trigger_args
		)

	def pause_job(
		self: SchedulerService,
		job_id: str,
		jobstore: typing.Union[None, str] = None
	):
		"""Pauses a job in the :attr:`scheduler <.SchedulerService.scheduler>`.

		:param job_id: The ID of the job.
		:param jobstore: Where and how the job is stored. Optional.

		:returns: The result of the function call to do so.
		"""

		self.logger.info("Pausing job ID %s", job_id)

		return self.scheduler.pause_job(job_id, jobstore)

	def resume_job(
		self: SchedulerService,
		job_id: str,
		jobstore: typing.Union[None, str] = None
	):
		"""Resunes a job in the :attr:`scheduler <.SchedulerService.scheduler>`.

		:param job_id: The ID of the job.
		:param jobstore: Where and how the job is stored. Optional.

		:returns: The result of the function call to do so.
		"""

		self.logger.info("Resuming job ID %s", job_id)

		return self.scheduler.resume_job(job_id, jobstore)

	def remove_job(
		self: SchedulerService,
		job_id: str,
		jobstore: typing.Union[None, str] = None
	):
		"""Removes a job from the :attr:`scheduler <.SchedulerService.scheduler>`.

		:param job_id: The ID of the job.
		:param jobstore: Where and how the job is stored. Optional.

		:returns: The result of the function call to do so.
		"""

		self.logger.info("Removing job ID %s", job_id)

		self.scheduler.remove_job(job_id, jobstore)

	def get_job(
		self: SchedulerService,
		job_id: str
	):
		"""Finds a job in the :attr:`scheduler <.SchedulerService.scheduler>` by
		its ID.

		:param job_id: The ID of the job.

		:returns: The job.
		"""

		return self.scheduler.get_job(job_id)

	def get_jobs(
		self: SchedulerService,
		jobstore: typing.Union[None, str] = None
	):
		"""Gets all jobs in the :attr:`scheduler <.SchedulerService.scheduler>`.

		:param jobstore: Where and how the jobs are stored. Optional.

		:returns: The jobs.
		"""

		return self.scheduler.get_jobs(jobstore)


def get_scheduler(
	config: typing.Dict,
	scheduler_class: apscheduler.schedulers.base.BaseScheduler = (
		apscheduler.schedulers.background.BackgroundScheduler
	)
) -> typing.Tuple[
	apscheduler.schedulers.base.BaseScheduler,
	bool
]:
	r"""Generates a scheduler based on the given Flask app config containing
	informabion about when to perform certain tasks.

	:param config: The Flask app's config.
	:param scheduler_class: The scheduler class to use. :class:`BackgroundSche\
		duler <apscheduler.schedulers.background.BackgroundScheduler>` by default.

	:returns: The scheduler.

	.. warning::
		The scheduler's timezone is set to UTC. Real world timezones can be
		messy and break things in unexpected ways, avoiding the possible confusion
		with setup isn't of extreme importance either.
	"""

	logger = logging.getLogger(__name__)

	scheduler = scheduler_class(timezone=pytz.utc)

	jobs_exist = False

	for config_key, func in CONFIG_KEY_JOBS.items():
		if config[config_key] == {}:
			logger.debug("%s config key doesn't exist, skipping job", config_key)

			continue

		logger.info(
			"Adding job %s with config key %s",
			func,
			config_key
		)

		scheduler.add_job(
			func=func,
			args=(config,),
			trigger="cron",
			**config[config_key]
		)

		jobs_exist = True

	if not jobs_exist:
		logger.warning("Scheduler %s contains no tasks", scheduler)

	return scheduler


def delete_expired_guest_users(config: typing.Dict) -> None:
	r"""Deletes all :class:`User <heiwa.database.User>`\ s who have been registered
	by the :mod:`guest <heiwa.views.guest>` API and have not made any content
	visible to external parties - the conditions for which are defined in
	:attr:`User.has_content <heiwa.database.User.has_content>`. For those who do
	have visible content, the
	:attr:`external_identifier <heiwa.database.User.external_identifier>`
	containing a hashed IP address is set to :data:`None`.

	:param config: The concerned Flask app's config.

	.. note::
		To allow for easy hashing, the actual :class:`Flask <flask.Flask>`
		instance is not provided.

	.. note::
		Since all guest users without content have been deleted after the first
		query, the second does not include needing to have content as a
		condition.
	"""

	logger = logging.getLogger(__name__)

	with sqlalchemy.orm.Session(
		sqlalchemy.create_engine(config["DATABASE_URI"])
	) as session:
		max_creation_timestamp = (
			datetime.datetime.now(tz=datetime.timezone.utc)
			- datetime.timedelta(seconds=config["GUEST_SESSION_EXPIRES_AFTER"])
		)

		logger.info(
			"Removing guest users registered before or on %s",
			max_creation_timestamp
		)

		session.execute(
			sqlalchemy.delete(database.User).
			where(
				sqlalchemy.and_(
					database.User.creation_timestamp <= max_creation_timestamp,
					database.User.registered_by == "guest",
					~database.User.has_content
				)
			).
			execution_options(synchronize_session="fetch")
		)

		session.execute(
			sqlalchemy.update(database.User).
			where(
				sqlalchemy.and_(
					database.User.creation_timestamp <= max_creation_timestamp,
					database.User.registered_by == "guest"
				)
			).
			values(external_id=None).
			execution_options(synchronize_session="fetch")
		)

		logger.debug("Finished removing guest users, committing session")

		session.commit()


def delete_expired_user_bans(config: typing.Dict) -> None:
	r"""Sets the :attr:`is_banned <heiwa.database.User.is_banned>` attribute of
	all :class:`User <heiwa.database.User>`s whose ban has expired to
	:data:`False`, if theirs is still :data:`True`. This will also happen
	automatically, should they use any API endpoint where they've authenticated.

	:param config: The concerned Flask app's config.

	.. note::
		To allow for easy hashing, the actual :class:`Flask <flask.Flask>`
		instance is not provided.

	.. seealso::
		:class:`heiwa.database.UserBan`
	"""

	logger = logging.getLogger(__name__)

	with sqlalchemy.orm.Session(
		sqlalchemy.create_engine(config["DATABASE_URI"])
	) as session:
		logger.info("Updating users with expired bans")

		session.execute(
			sqlalchemy.update(database.User).
			where(
				sqlalchemy.and_(
					database.User.is_banned,
					(
						sqlalchemy.select(database.UserBan.receiver_id).
						where(
							sqlalchemy.and_(
								database.UserBan.receiver_id == database.User.id,
								database.UserBan.is_expired
							)
						).
						exists()
					)
				)
			).
			values(is_banned=False).
			execution_options(synchronize_session="fetch")
		)

		logger.debug("Finished pdating users with expired bans, committing session")

		session.commit()


def delete_expired_thread_view_records(config: typing.Dict) -> None:
	"""Deletes all :class:`ThreadViewRecord <heiwa.database.ThreadViewRecord>`
	objects whose age exceeds the amount of seconds defined in the
	``THREAD_VIEW_COOLDOWN`` config key.

	:param config: The concerned Flask app's config.

	.. note::
		To allow for easy hashing, the actual :class:`Flask <flask.Flask>`
		instance is not provided.
	"""

	logger = logging.getLogger(__name__)

	with sqlalchemy.orm.Session(
		sqlalchemy.create_engine(config["DATABASE_URI"])
	) as session:
		logger.info("Deleting expired thread views")

		session.execute(
			sqlalchemy.delete(database.ThreadViewRecord).
			where(
				database.ThreadViewRecord.creation_timestamp <= (
					datetime.datetime.now(tz=datetime.timezone.utc)
					- datetime.timedelta(seconds=config["THREAD_VIEW_COOLDOWN"])
				)
			)
		)

		logger.debug("Finished deleting expired thread views, committing session")

		session.commit()


def delete_unverified_expired_local_accounts(config: typing.Dict) -> None:
	r"""Deletes all :class:`LocalAccount <heiwa.database.LocalAccount>`\ s that
	have existed for longer than
	``config->LOCAL_ACCOUNT_VERIFICATION_JWT_EXPIRES_AFTER`` seconds and whose
	:attr:`email_is_verified <heiwa.database.LocalAccount.email_is_verified>`
	attribute is :data:`False`.

	:param config: The concerned Flask app's config.

	.. note::
		To allow for easy hashing, the actual :class:`Flask <flask.Flask>`
		instance is not provided.
	"""

	logger = logging.getLogger(__name__)

	with sqlalchemy.orm.Session(
		sqlalchemy.create_engine(config["DATABASE_URI"])
	) as session:
		logger.info("Deleting unverified expired local accounts")

		session.execute(
			sqlalchemy.delete(database.LocalAccount).
			where(
				sqlalchemy.and_(
					database.LocalAccount.email_is_verified.is_(False),
					(
						database.LocalAccount.creation_timestamp
						<= (
							datetime.datetime.now(tz=datetime.timezone.utc)
							- datetime.timedelta(
								seconds=config["LOCAL_ACCOUNT_VERIFICATION_JWT_EXPIRES_AFTER"]
							)
						)
					)
				)
			)
		)

		logger.debug(
			"Finished deleting unverified expired local accounts, committing session"
		)

		session.commit()


def log_statistics(config: typing.Dict) -> None:
	"""As long as it's not the exact same as the previous record, creates a
	:class:`Statistic <heiwa.database.Statistic>` object with the current
	thread, post and user count.

	:param config: The concerned Flask app's config.

	.. note::
		To allow for easy hashing, the actual :class:`Flask <flask.Flask>`
		instance is not provided.
	"""

	logger = logging.getLogger(__name__)

	with sqlalchemy.orm.Session(
		sqlalchemy.create_engine(config["DATABASE_URI"])
	) as session:
		logger.info("Logging statistics")

		thread_count, post_count, user_count = session.execute(
			sqlalchemy.select(
				(
					sqlalchemy.select(
						sqlalchemy.func.count(database.Thread.id)
					).
					limit(constants.BIG_INTEGER_LIMIT).
					scalar_subquery()
				),
				(
					sqlalchemy.select(
						sqlalchemy.func.count(database.Post.id)
					).
					limit(constants.BIG_INTEGER_LIMIT).
					scalar_subquery()
				),
				(
					sqlalchemy.select(
						sqlalchemy.func.count(database.User.id)
					).
					limit(constants.BIG_INTEGER_LIMIT).
					scalar_subquery()
				)
			)
		).one()

		logger.debug(
			"Logging %s threads, %s posts, %s users",
			thread_count,
			post_count,
			user_count
		)

		last_statistic = session.execute(
			sqlalchemy.select(
				database.Statistic.thread_count,
				database.Statistic.post_count,
				database.Statistic.user_count
			).
			order_by(
				sqlalchemy.desc(database.Statistic.creation_timestamp)
			).
			limit(1)
		).one_or_none()

		if (
			last_statistic is not None and
			(thread_count, post_count, user_count) == last_statistic
		):
			logger.debug("Statistics unchanged, not adding to the database this time")

			return

		session.execute(
			sqlalchemy.insert(database.Statistic).
			values(
				thread_count=thread_count,
				post_count=post_count,
				user_count=user_count
			)
		)

		logger.debug("Finished logging statistics, comitting session")

		session.commit()


CONFIG_KEY_JOBS = {
	"TASK_DELETE_EXPIRED_GUESTS_CRON": delete_expired_guest_users,
	"TASK_DELETE_EXPIRED_THREAD_VIEW_RECORDS_CRON": (
		delete_expired_thread_view_records
	),
	"TASK_DELETE_EXPIRED_USER_BANS_CRON": delete_expired_user_bans,
	"TASK_DELETE_UNVERIFIED_EXPIRED_LOCAL_ACCOUNTS_CRON": (
		delete_unverified_expired_local_accounts
	),
	"TASK_LOG_STATISTICS_CRON": log_statistics
}
"""A dictionary defining which Flask config keys correspond to which task."""
