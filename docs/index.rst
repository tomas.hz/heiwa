Heiwa documentation
===================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   performance
   heiwa

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

Notes
=====

HTTP QUERY (SEARCH) method in searching APIs
--------------------------------------------

``list_`` APIs use the ``QUERY`` HTTP method, one yet to be standardized. This
was a hard choice to make, but with (nearly) infinitely scalable ``filter``
usage, I believe it to be the best option -- or more precisely, the one I like
and will use, even if it means forsaking HTTP until it is a part of its
standard.

**Why?**

Semantically, ``GET`` requests' bodies, while allowed, should have no meaning.
URLs should be the only identifier for caching them, nothing else should
matter.

This leaves us with 2 ``GET`` options;

* Ignoring semantics and using bodies anyway - the implementation of choice for
  a long time. I don't like this. Semantics are important.

* Putting *everything* in the URL. I don't like this one either. It's going to
  make the creation of manual requests significantly harder and require an
  overhaul of the current input validation system - which, while messily
  generated, works fairly well at its core. And to top it off, there are limits
  to URL lengths which could make those large ``filter``\ s completely
  impossible.

``POST`` requests are also a common choice for developers facing this issue.
They remove the issue of incorrect body semantics, but introduce a new issue - 
the lack of semantic idempotence. Using them instead would be trading an old
problem for a different one, especially with caching.
