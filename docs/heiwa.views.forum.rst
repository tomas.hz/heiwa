Forums
======

.. automodule:: heiwa.views.forum
   :members:
   :special-members:

Parsed permissions
------------------

.. automodule:: heiwa.views.forum.parsed_permissions
   :members:
   :special-members:

Group permissions
-----------------

.. automodule:: heiwa.views.forum.permissions_group
   :members:
   :special-members:

User permissions
----------------

.. automodule:: heiwa.views.forum.permissions_user
   :members:
   :special-members:

Subscriptions
-------------

.. automodule:: heiwa.views.forum.subscription
   :members:
   :special-members:

Utilities
---------

.. automodule:: heiwa.views.forum.utils
   :members:
   :special-members:
