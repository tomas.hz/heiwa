Performance
===========

As it's the only fully supported SQL dialect right now, PostgreSQL was used as
the database backend. When testing occurs, the server has already been running
and received idle requests for a few minutes.

All tests were performed on a ThinkPad X260:
   * CPU: Intel i5-6300U - 4 cores, 2.4 Ghz
   * RAM: Samsung M471A1K43CB1-CRC - 8GB DDR4 @ 2133 MHz
   * SSD: Intel SSDSC2KF48 - 480GB, 492.4MB/s read, TODO write, 0.12ms access time
   * Power management on performance mode, charger plugged in

Programs used for testing can be found in the
`useful utilities <https://gitlab.com/tomas.hz/useful-utilities>`_ repository.
Generally, measurements are done in a single thread so as to avoid unrealistic
load all at once.

Forum APIs
----------

.. seealso::
   :doc:`Original API views <heiwa.views.forum>`

Listing with permissions parsed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Last measured on version 0.17.38.*

The average for a realistic 50 results was **45.5ms**. At 512 results - the
default limit, but one rarely seen in the real world outside data scraping and
cloning - the average was **72.97ms**. All forums' permissions were already
parsed at the time they were listed, unlike the metric following this one.
There were always 512 or more forums available.

.. image:: /images/forum-list-parsed-chart.png
    :alt: A chart showing the results

:download:`CSV <files/forum-list-parsed-results.csv>`

Listing with permissions not yet parsed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Last measured on version 0.17.38.*

The average for a realistic 50 results was **395.12ms**. While this is a lot,
the respective forums' permissions will remain parsed forever until an
administrator changes them. With 512 results, the default limit, the average
was **3403.85ms**. There were always 512 or more forums available. The cached
parsed permissions were deleted with each new measurement.

.. image:: /images/forum-list-unparsed-chart.png
    :alt: A chart showing the results

:download:`CSV <files/forum-list-unparsed-results.csv>`

Thread APIs
-----------

.. seealso::
   :doc:`Original API views <heiwa.views.thread>`

Listing in a single forum with permissions parsed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Last measured on version 0.17.38.*

The average for a realistic 50 results was **48.3ms**. At 512 results, the
average was **112.55ms**. All threads belonged to a single forum, whose
permissions were already known in advance.

.. image:: /images/thread-list-single-parsed-chart.png
    :alt: A chart showing the results

:download:`CSV <files/thread-list-single-parsed-results.csv>`

Listing in multiple forums with permissions parsed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Last measured on version 0.17.41.*

The average for a realistic 50 results was **48.85ms**. At 512 results, the
average was **113.74ms**. In total, every thread belonged to one of 50 forums,
whose permissions were already parsed.

Load like this is most often seen when users search for specific content. For
example, threads containing links in any forum, created in the last few days.

That said, there seemed to be no meaningful difference, with the extra ~1ms
likely resulting from a statistical inaccuracy.

.. image:: /images/thread-list-multiple-parsed-chart.png
    :alt: A chart showing the results

:download:`CSV <files/thread-list-multiple-parsed-results.csv>`

Listing in multiple forums with permissions not parsed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Last measured on version 0.17.41.*

The average for a realistic 50 results was **388.41ms**. At 512 results, the
average was **466.3ms**, not significantly higher than the average for 50. In
total, every thread belonged to one of 50 forums, whose permissions were not
parsed and deleted with each new measurement.

Load like this is most often seen when new users search for threads containing
specific content and the related forums' permissions have not yet had a chance
to be parsed. For example, child forums which are usually not visible on the
index page of frontend programs.

There is a visible trend of the response time rapidly increasing with the first
few requests, then nearly leveling out. This likely resulted from the first 50
threads' forums being unique, each increasing the average response time by how
long it takes to parse its permissions.

.. image:: /images/thread-list-multiple-unparsed-chart.png
    :alt: A chart showing the results

:download:`CSV <files/thread-list-multiple-unparsed-results.csv>`

Creating
~~~~~~~~

*Last measured on version 0.17.38.*

The average amount of time it took to create a thread was **45.33ms**. The
forum was the same each time and its permissions were already calculated.

.. image:: /images/thread-create-chart.png
    :alt: A chart showing the results

:download:`CSV <files/thread-create-results.csv>`

Post APIs
---------

.. seealso::
   :doc:`Original API views <heiwa.views.post>`

Listing in a single forum with permissions parsed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Last measured on version 0.17.41.*

The average for a realistic 50 results was **42.95ms**. At 512, the average
was **98.12ms**. All posts belonged to a single thread's forum, whose
permissions were already known in advance.

.. image:: /images/post-list-single-parsed-chart.png
    :alt: A chart showing the results

:download:`CSV <files/post-list-single-parsed-results.csv>`

Listing in multiple forums with permissions parsed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Last measured on version 0.17.41.*

The average for a realistic 50 results was **42.05ms**. At 512 results, the
average was **78.92ms**. In total, every post's thread belonged to one of 50
forums, whose permissions were already parsed.

.. image:: /images/post-list-multiple-parsed-chart.png
    :alt: A chart showing the results

:download:`CSV <files/post-list-multiple-parsed-results.csv>`

Listing in multiple forums with permissions not parsed
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Last measured on version 0.17.41.*

The average for a realistic 50 results was **384.41ms**. At 512 results, the
average was **447.63ms**. In total, every post belonged to one of 50 threads'
forums, whose permissions were not parsed and deleted with each new
measurement. There were always 512 or more posts available.

.. image:: /images/post-list-multiple-unparsed-chart.png
    :alt: A chart showing the results

:download:`CSV <files/post-list-multiple-unparsed-results.csv>`
