API views
=========

.. automodule:: heiwa.views
   :members:
   :special-members:

.. toctree::
   :maxdepth: 4

   heiwa.views.category
   heiwa.views.file
   heiwa.views.forum
   heiwa.views.group
   heiwa.views.guest
   heiwa.views.message
   heiwa.views.meta
   heiwa.views.notification
   heiwa.views.oidc
   heiwa.views.post
   heiwa.views.statistic
   heiwa.views.thread
   heiwa.views.user
   heiwa.views.utils
